

#import "NewCenterViewController.h"
//
//  PFViewController.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "PFViewController.h"
#import "PackPickerController.h"
#import "ContentViewController.h"
#import "UIColor-Extras.h"

#import "LanguageSource.h"
#import "LanguageWordPack.h"
#import "LanguageWord.h"
#import "LanguageFiling.h"

#import <QuartzCore/QuartzCore.h>
//#import <MessageUI/MessageUI.h>

#import "BoxWorld.h"
#import "BoxShape.h"
#import "BoxBody.h"

#import "FluffyRiverView.h"
#import "SnippetViewController.h"
#import "EmitterDebugView.h"
#import "SimpleVerb.h"
#import "JSONKit.h"
#import "NSData+HMAC.h"
#import "GTMNSDictionary+URLArguments.h"

#import "AttractorView.h"
#import "FilingView.h"

#import "AboutViewController.h"
#import "SettingsViewController.h"
#import "WordpackPickerViewController.h"

#import "PackPickerTableController.h"
#import "MenuController.h"

#import "ContextCardViewController.h"
#import "CardView.h"

#import "TestFlight.h"
#import "RightViewController.h"

#import "PFAppDelegate.h"

// Attractor states: closed, waiting on network, open

@implementation NewCenterViewController

@synthesize wordPack;
@synthesize displayLink;
@synthesize world;
@synthesize documentOptionsPanel;
@synthesize wallBodies;
@synthesize mouseAnchorBody;
@synthesize filingPopoverController;
@synthesize fluffyView;
@synthesize menuView;
@synthesize attractorViews;
@synthesize addAttractorView;
@synthesize selectedAttractor;
@synthesize embeddedViewControllerBackdropView;
@synthesize workQueue;
@synthesize snippetController;
@synthesize wordPackStr;

@synthesize wordpacks, settings, info, plusButton, mailScreenshot;
@synthesize shouldShowPlusButton;
@synthesize settingsDict;

@synthesize background;

@synthesize dictionarymanager;

// For iPhone
@synthesize tableviewController;
@synthesize wordPackSelect;
@synthesize tableArray;

PFAppDelegate *appDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor redColor]];
    NSLog(@"heyheyheywordpack %@", wordPackSelect);
    wordpackLoader= [[WordpackLoader alloc]initWithDictManager:dictionarymanager];
    wordpackDB= [wordpackLoader getWordpackDB];
     [self loadWordPack:wordPackSelect];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadWordPack:(NSString*)wordpack{
    //change bg color
    NSMutableArray* colors=[[NSMutableArray alloc] initWithObjects:
                            @"Castle.png",
                            @"Balcony.png",
                            @"Moonlight.png",
                            @"River.png",
                            @"Window.png",
                            @"Regina.png",
                            @"Dock.png",
                            @"Waterfront.png",
                            nil];
    
    NSString* path= [colors objectAtIndex:(arc4random()%[colors count])];

    OPLog(@"Loading wordpack: %@", wordpack);
    //    self.wordPackStr= [wordpack stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    
    //state variables
    self.wordPackStr= wordpack;
    
    
    NSArray* words= [[wordpackDB getWordPackForWord:wordpack] words];
    [self removeAttractors];
    
    OPLog(@"Creating attractors:");
    for (WordDescription* word in words){
        NSLog(@"%@ ", word.fullword);
        
        for (NSString* item in word.affixes){
            LanguageWord* l= [LanguageWord alloc];
            l.term=item;
            l.wordpackStr= wordpack;
            AttractorView* aView=[self insertAttractorForWord:l];
            
            if ([[morphemeDB getTypeForMorpheme:item] isEqualToString:@"suffix"] || [[morphemeDB getTypeForMorpheme:item] isEqualToString:@"prefix"]){
                [aView makeMorpheme];
                
            } else {
                [self pinBody:aView];
            }
            
        }
    }
}

#pragma mark - C Functions

static inline CGPoint cartesianPointFromPolar(CGFloat radius, CGFloat angle) {
    return CGPointMake(cosf(angle)*radius, sinf(angle)*radius);
}

static inline CGAffineTransform transformForPositionRotation(CGPoint pos, CGFloat rotation) {
    CGAffineTransform t = CGAffineTransformMakeTranslation(pos.x, pos.y);
    CGAffineTransform r = CGAffineTransformMakeRotation(rotation);
    return CGAffineTransformConcat(r, t);
}

static inline CGFloat CGPointDistanceToPoint(CGPoint p1, CGPoint p2) {
    return sqrtf( (p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y) );
}

#pragma mark - Snippet

- (NSArray*) getWordpackList{
    return [wordpackDB wordpackTitles];
}

//OUTDATED
- (bool)isSuffix:(NSString*)string{
    if ([[morphemeDB getTypeForMorpheme:string] isEqualToString:@"suffix"])
        return true;
    return false;
    
    //OUTDATED
    if ([string characterAtIndex:0]=='-')
        return true;
    return false;
}

//OUTDATED
- (bool)isPrefix:(NSString*)string{
    if ([[morphemeDB getTypeForMorpheme:string] isEqualToString:@"prefix"])
        return true;
    return false;
    
    //
    if ([string characterAtIndex:[string length]-1]=='-')
        return true;
    return false;
}

- (void)removeAttractors {
    // get rid of existing attractors
    for (AttractorView *av in self.attractorViews) {
        [self.world removeBody:av.body];
        [av removeFromSuperview];
        
        // remove any filings that the attractor has
        for (FilingView *fv in av.filingViews) {
            [self.world removeBody:fv.body];
            [fv removeFromSuperview];
        }
    }
    [self.attractorViews removeAllObjects];
    
    [self removePlusAttractor];
    
}


- (void)showNewAttractor {
    
    [self stopBoxWorld];
    
    [self createPlusAttractor];
    AttractorView *view = self.addAttractorView;
    
    NSValue *Tv = [NSValue valueWithCGPoint:view.center];
    
    UITextField *tf = [view.layer valueForKey:@"textField"];
    UIImageView *iv = [view.layer valueForKey:@"imageView"];
    
    [view.layer setValue:Tv forKey:@"originalCenter"];
    
    tf.placeholder = @"new word";
    
    [self.view addSubview:self.embeddedViewControllerBackdropView];
    self.embeddedViewControllerBackdropView.backgroundColor = [UIColor colorWithWhite:1. alpha:0.8];
    self.embeddedViewControllerBackdropView.alpha = 0.;
    
    [self.view bringSubviewToFront:view];
    
    [view removeGestureRecognizer:view.tapRecognizer];
    
    [UIView animateWithDuration:0.4
                          delay:0.
                        options:0
                     animations:^{
                         
                         view.center = CGPointMake(self.view.bounds.size.width/2.,
                                                   self.view.bounds.size.height/2.);
                         
                         iv.alpha = 0.;
                         tf.alpha = 1.;
                         self.fluffyView.alpha = 0.1;
                         for (AttractorView *av in self.attractorViews) {
                             av.userInteractionEnabled = NO;
                             av.alpha = 0.2;
                         }
                         self.embeddedViewControllerBackdropView.alpha = 1.;
                         
                     }
                     completion:^(BOOL finished) {
                         
                         [tf becomeFirstResponder];
                         
                         UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                               action:@selector(handleCanvasTapFrom:)];
                         tap.delegate = self;
                         [self.embeddedViewControllerBackdropView addGestureRecognizer:tap];
                     }];
}


- (void)showViewController:(UIViewController*)controller {
    
    OPLogMethod;
    
    // if self.embeddedViewControllerBackdropView has a superview, we are alreay presenting a view controller
    if (nil != self.embeddedViewControllerBackdropView.superview) {
        return;
    }
    
    
    controller.view.alpha = 0.;
    CGSize S = controller.contentSizeForViewInPopover;
    
    controller.view.frame = CGRectMake(0., 0., S.width, S.height);
    controller.view.center = CGPointMake(self.view.bounds.size.width/2., self.view.bounds.size.height/2.);
    
    controller.view.clipsToBounds = YES;
    controller.view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    controller.view.layer.borderWidth = 2.;
    controller.view.layer.cornerRadius = 10.;
    controller.view.backgroundColor = [UIColor whiteColor];
    
    self.embeddedViewControllerBackdropView.alpha = 0.;
    self.embeddedViewControllerBackdropView.frame = self.view.bounds;
    self.embeddedViewControllerBackdropView.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.8];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(hideViewController)];
    tap.delegate = self;
    [self.embeddedViewControllerBackdropView addGestureRecognizer:tap];
    [self.view addSubview:self.embeddedViewControllerBackdropView];
    
    [self.embeddedViewControllerBackdropView addSubview:controller.view];
    
    controller.view.layer.shouldRasterize = YES;
    
    // this is to move between two sibling view controllers.
    [self addChildViewController:controller];
    
    [UIView animateWithDuration:.1
                     animations:^{
                         
                         self.embeddedViewControllerBackdropView.alpha = 1.;
                         
                     } completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.2
                                             options:0
                                          animations:^{
                                              
                                              controller.view.alpha = 1.;
                                              
                                          } completion:^(BOOL finished) {
                                              
                                              [controller didMoveToParentViewController:self];
                                          }];
                         
                         
                     }];
}


- (void) createPlusAttractor {
    AttractorView *aView = [self insertAttractorForWord:nil];
    [self.view addSubview:aView];
    self.addAttractorView = aView;
    
    // toss the long press recognizer, since the add attractor doesn't need it
    [aView removeGestureRecognizer:aView.longPressRecognizer];
    // also the pinch
    [aView removeGestureRecognizer:aView.pinchRecognizer];
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"plus_attractor"]];
    [aView.rotatingView addSubview:iv];
    iv.center = CGPointMake(aView.bounds.size.width/2., aView.bounds.size.height/2.);
    iv.userInteractionEnabled = YES;
    [aView.layer setValue:iv forKey:@"imageView"];
    
    UITextField *field = [[UITextField alloc] initWithFrame:CGRectMake(0., 0., aView.bounds.size.width-10., 42.)];
    field.center = CGPointMake(aView.bounds.size.width/2., aView.bounds.size.height/2.);
    field.font = [UIFont boldSystemFontOfSize:28.];
    field.adjustsFontSizeToFitWidth = YES;
    field.borderStyle = UITextBorderStyleBezel;
    field.alpha = 0.;
    [aView addSubview:field];
    [aView.layer setValue:field forKey:@"textField"];
    field.backgroundColor = [UIColor whiteColor];
    field.delegate = self;
}

- (void) removePlusAttractor{
    // remove + attractor
    [self.world removeBody:self.addAttractorView.body];
    [self.addAttractorView removeFromSuperview];
    self.addAttractorView = nil;
}

// Called after reading Attractor Gestures
- (void)handleAttractorTapFrom:(UITapGestureRecognizer*)recognizer {
    AttractorView *tappedAttractor = (AttractorView*)recognizer.view;
    
    // To open attractors after tap, without glomming
    tappedAttractor.isCompleted=true;
    
    OPLog(@"tap attractor");
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        // ...
        
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        OPLog(@"gotinfirststep");
        self.selectedAttractor = tappedAttractor;
        
        //close all attractors but the tapped one
        for (AttractorView *av in self.attractorViews) {
            if (av != tappedAttractor) {
                if (kAttractorViewStateClosed != av.state) {
                    [self closeAttractor:av];
                }
            }
        }
        
        if (tappedAttractor == self.addAttractorView) {
            [self showNewAttractor];
            [TestFlight passCheckpoint:@"NewAttractorTap"];
        } else if ([tappedAttractor.word.term isEqualToString:@"Welcome"]){
    
            [self showPopover:self.wordpacks];
            
            // Don't show filings if attractor isn't completed
        } else if (![tappedAttractor isCompleted]){
            //  OPLog(@"in this");
            return ;
        } else{
            // normal attractor
            if (kAttractorViewStateOpen == tappedAttractor.state) {
                [TestFlight passCheckpoint:@"AttractorTapClose"];
                [self closeAttractor:tappedAttractor];
                
                [self highlightAttractor:nil];
                
            } else if (kAttractorViewStateClosed == tappedAttractor.state) {
                OPLog(@"Got into new attract");
                [TestFlight passCheckpoint:@"AttractorTapOpen"];
                [self loadAttractor:tappedAttractor];
                [self highlightAttractor:tappedAttractor];
                
            }
        }
    }
}


- (void)highlightAttractor:(AttractorView*)attractor {
    
    if (attractor) {
        [UIView animateWithDuration:0.4
                         animations:^{
                             for (AttractorView *av in self.attractorViews) {
                                 if (av != attractor) {
                                     av.alpha = 0.7;
                                 } else {
                                     av.alpha = 1.;
                                 }
                             }
                         }];
    } else {
        [UIView animateWithDuration:0.4
                         animations:^{
                             for (AttractorView *av in self.attractorViews) {
                                 av.alpha = 1.;
                             }
                         }];
    }
    
}

#pragma mark - UI Actions

- (CGFloat)randomFloatSeededWithString:(NSString*)string {
    NSData *wd = [[string dataUsingEncoding:NSUTF8StringEncoding] SHA1];
    
    long *d = (long*)[wd bytes]; // pointer to a long
    
    long seedling = *(d);
    
    d+= 1;
    seedling += *(d);
    
    srandom(seedling);
    
    return RANDOM_FLOAT();
}


- (AttractorView*)insertAttractorForWord:(LanguageWord*)aWord {
    OPLog(@"New attractor for %@",aWord.term);
    NSLog(@"type: %@",[morphemeDB getTypeForMorpheme:aWord.term]);
    
    CGFloat rr = 0.0;
    if (aWord)
        rr = [self randomFloatSeededWithString:aWord.term];
    
    // View
    AttractorView *av = [[AttractorView alloc] initWithFrame:CGRectMake(0., 0., MAGNET_WIDTH, MAGNET_HEIGHT)];
    
    if ([self isSuffix:aWord.term] || [self isPrefix:aWord.term]){
        [av makeMorpheme];
        
        if ([[morphemeDB getTypeForMorpheme:aWord.term] isEqualToString:@"suffix"])
            // Set Defintion for NYU
            [av.defLabel setHidden:NO];
    }
    
    
    
    [av.layer setValue:[NSNumber numberWithFloat:rr]
                forKey:@"hue"];
    
    av.shapeLayer.fillColor = [UIColor grayColor].CGColor;
    
    av.radius = av.bounds.size.width/2.;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(handleAttractorTapFrom:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    tap.delegate = av;
    [av addGestureRecognizer:tap];
    av.tapRecognizer = tap;
    
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(handleAttractorPanFrom:)];
    pan.minimumNumberOfTouches = 1;
    pan.maximumNumberOfTouches = 1;
    [av addGestureRecognizer:pan];
    av.panRecognizer = pan;
    
    //reverse pinch action
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(handleAttractorPinchFrom:)];
    [av addGestureRecognizer:pinch];
    av.pinchRecognizer=pinch;
    
    
    // Delete action
    [av.deleteButton addTarget:self
                        action:@selector(deleteAttractor:)
              forControlEvents:UIControlEventTouchUpInside];
    av.deleteButton.alpha = 0.;
    
    
    // Box2D
    /*
     BoxFixture* fixture;
     if (av.isMorpheme){
     int width=SMALL_MAGNET_WIDTH;
     int height=SMALL_MAGNET_HEIGHT;
     
     PolygonShape* shape= [PolygonShape rectangleWithSize:CGSizeMake(width,height)];
     shape.centroid=CGPointMake(-1*width/2, height/2);
     fixture= [BoxFixture fixtureWithShape:shape];
     } else{
     int width=MAGNET_WIDTH;
     int height=MAGNET_HEIGHT;
     
     PolygonShape* shape=[PolygonShape rectangleWithSize:CGSizeMake(width,height)];
     shape.centroid=CGPointMake(-1*width/2, height/2);
     fixture= [BoxFixture fixtureWithShape:shape];
     }
     */
    
    BoxFixture *fixture;
    if (av.isMorpheme){
        fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:SMALL_MAGNET_HEIGHT/1.5 Position:CGPointZero]];
    } else {
        fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:MAGNET_HEIGHT/1.5 Position:CGPointZero]];
    }
    
    fixture.friction = 0.5;
    fixture.density = 10.0;
    fixture.restitution = 0.5;
    
    BoxBody *body = [BoxBody dynamicBody];
    body.linearDamping = 60;
    body.angularDamping = 60;
    [body addFixture:fixture];
    [self.world addBody:body];
    // Add body
    body.weakContext = av;
    
 
    
    //pin body if not morpheme
    if (!av.isMorpheme){
        
    }
    
    RANDOM_SEED();
    CGFloat rx = 0.1 + RANDOM_FLOAT()*(0.7*self.view.bounds.size.width);
    CGFloat ry = 0.1 + RANDOM_FLOAT()*(0.7*self.view.bounds.size.height);
    
    body.position = CGPointMake(rx,ry);
    // Set up position
    av.word = aWord;
    av.body = body;
    
    av.frame = CGRectMake(rx,ry, av.frame.size.width, av.frame.size.height);

    av.label.text = aWord.term;
    
    NSString* def=[morphemeDB getDefinitionForMorpheme:aWord.term];
    // NSString* def=@"THIS IS DEF";
    // NSLog(@"Def: %@", def);
    av.defLabel.text=def;
    
    //populate morpheme array with the word we initiated the attractor with
    [av.morphemeComponents addObject:aWord.term];
    
    
    if (aWord) {
        // pre-cache the card image and controller
        ContextCardViewController *ctxvc = [[ContextCardViewController alloc] initWithNibName:nil bundle:nil];
        ctxvc.word = av.word;
        [av.layer setValue:ctxvc
                    forKey:@"contextCardController"];
        
        [self renderImageOfController:ctxvc
                            withFrame:CGRectMake(0., 0., 600., 800.)
                           completion:^(UIImage *img) {
                               
                               UIImageView *iv = [[UIImageView alloc] initWithImage:img];
                               iv.frame = av.bounds;
                               iv.contentMode = UIViewContentModeScaleAspectFit;
                               iv.alpha = 0.;
                               
                               iv.center = CGPointMake(av.bounds.size.width/2.,
                                                       av.bounds.size.height/2.);
                               [av addSubview:iv];
                               [av.layer setValue:iv forKey:@"cardImageView"];
                           }];
        
    }
    
    //add the attractor view to the view
        [self.view addSubview:av];
    
    // keep track of all attractors being displayed
    [self.attractorViews addObject:av];
    
    return av;
}

// Load Words
- (void)loadAttractorsForWordPack:(LanguageWordPack*)chosenPack {
    OPLog(@"wordPickerDone");
    
    self.wordPack = chosenPack;
    
    // Build attractors for words in the chosen pack
    OPLog(@"Loading %i words.",[self.wordPack.words count]);
    for (LanguageWord *aWord in self.wordPack.words) {
        AttractorView *aView;
        
        aView = [self insertAttractorForWord:aWord];
    }
}


- (void)loadAttractor:(AttractorView *)attractor {
    
    CAShapeLayer *l = attractor.shapeLayer;
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.15];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [CATransaction setCompletionBlock:^{
        [CATransaction begin];
        [CATransaction setAnimationDuration:0.15];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        l.transform = CATransform3DIdentity;
        [CATransaction commit];
    }];
    if (0 == [attractor.word.filings count])
        l.fillColor = [UIColor blackColor].CGColor;
    l.transform = CATransform3DMakeScale(1.2, 1.2, 1.0);
    [CATransaction commit];
    
    attractor.state = kAttractorViewStateWaitingOnNetwork;
    
    if ([attractor.word.filings count] == 0) {
        [attractor startFluffies];
        CGFloat delay = 0; //kAttractorFluffyDelay + RANDOM_FLOAT()*kAttractorFluffyDelay;
        [self performSelector:@selector(tloadAttractor:) withObject:attractor afterDelay:delay];
    } else {
        if (self.selectedAttractor == attractor) {
            [self openAttractor:attractor];
        }
    }
}

- (void) pinBody:(AttractorView*)a{
    NSLog(@"Pinning: %@", a.word.term);
}
- (void)tloadAttractor:(AttractorView*)attractor {
    
    OPLog(@"loading: %@",attractor.word.term);
    
    
    attractor.word.wordpackStr= self.wordPackStr;
    //Call Blekko
    [attractor.word loadFilingsCompletion:^(BOOL success, LanguageWord *word) {
        [attractor stopFluffies];
        
        if (0 < [word.filings count]) {
            [UIView animateWithDuration:0.2
                             animations:^{
                                 CGFloat hue = [[attractor.layer valueForKey:@"hue"] floatValue];
                                 attractor.shapeLayer.fillColor = [UIColor colorWithHue:hue
                                                                             saturation:0.9
                                                                             brightness:0.6 alpha:1.].CGColor;
                             }];
        } else {
            
            [UIView animateWithDuration:0.2
                             animations:^{
                                 attractor.shapeLayer.fillColor = [UIColor grayColor].CGColor;
                             }];
        }
        RightViewController *filingTable = [[RightViewController alloc] init];
      
       filingTable.view.backgroundColor = [UIColor greenColor];
        NSLog(@"ITHAPPENS");

        
        if (self.selectedAttractor == attractor) {
            if (success) {
                OPLog(@"attractor loaded: %@ (%i filings)",attractor.word.term,[word.filings count]);
                // Open table- show filing
                filingTable.currectAttractorView = attractor;
                [filingTable openAttractor:attractor];
                //Do not open attractor
                //[self openAttractor:attractor];
                  [filingTable.tableView reloadData];
            } else {
               // [self openAttractor:attractor];
            }
            
        }
    }];
   
}

#pragma mark -

- (void)renderImageOfController:(UIViewController*)controller withFrame:(CGRect)frame completion:(void(^)(UIImage* img))completion {
    
    NSAssert(controller, @"missing controller");
    
    // render the image in a secondary thread to avoid locking up the main thread.
    [self.workQueue addOperationWithBlock:^{
        //OPLogMethod;
        UIView *view = controller.view;
        view.frame = frame;
        [view layoutIfNeeded];
        UIGraphicsBeginImageContext(controller.view.bounds.size);
        [controller.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            completion(img);
        }];
        
    }];
}
#pragma mark - +Attractor

- (void)hideNewAttractor:(NSString*)term {
    
    AttractorView *view = self.addAttractorView;
    NSValue *Tv = [view.layer valueForKey:@"originalCenter"];
    UIImageView *iv = [view.layer valueForKey:@"imageView"];
    UITextField *tf = [view.layer valueForKey:@"textField"];
    
    AttractorView *newAttractor = nil;
    
    if (term) {
        LanguageWord *newWord = [[LanguageWord alloc] init];
        newWord.term = term;
        newWord.gloss = [[LanguageSource gloss] valueForKey:[term lowercaseString]];
        
        
        [self.wordPack.words addObject:newWord];
        
        newAttractor = [self insertAttractorForWord:newWord];
        [self.view addSubview:newAttractor];
        [self.attractorViews addObject:newAttractor];
        newAttractor.alpha = 0.;
        
        tf.text = nil;
    } else {
        [tf resignFirstResponder];
    }
    
    
    [self removePlusAttractor]; //we remove the + attractor
    NSLog(@"Removed the + attractor");
    
    [UIView animateWithDuration:.6
                          delay:0.1
                        options:0
                     animations:^{
                         //view.transform = T;
                         view.center = [Tv CGPointValue];
                         tf.alpha = 0.;
                         self.embeddedViewControllerBackdropView.alpha = 0.;
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3
                                          animations:^{
                                              for (AttractorView *av in self.attractorViews) {
                                                  av.userInteractionEnabled = YES;
                                                  av.alpha = 1.;
                                              }
                                              
                                              iv.alpha = 1.;
                                              self.fluffyView.alpha = 1.;
                                          }];
                         
                         [self startBoxWorld];
                         
                         UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                               action:@selector(handleAttractorTapFrom:)];
                         [view addGestureRecognizer:tap];
                         view.tapRecognizer = tap;
                         
                         self.embeddedViewControllerBackdropView.gestureRecognizers = nil;
                         [self.embeddedViewControllerBackdropView removeFromSuperview];
                         
                         if (term) {
                             [self performSelector:@selector(flashAttractor:) withObject:newAttractor afterDelay:.5];
                         }
                         
                         
                     }];
}


#pragma mark - Gesture Recognizer Callbacks

- (void)handleCanvasTapFrom:(UITapGestureRecognizer*)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [self hideNewAttractor:nil];
    }
}
#pragma mark - Snippet

//Called when one attractor's filing is tapped.
//Opens the preview snippet
- (void)showSnippetForFiling:(FilingView*)filingView {
    // Tap filing show snippet
    [TestFlight passCheckpoint:@"ShowSnippetForFiling"];
    
    self.snippetController = [[SnippetViewController alloc] initWithNibName:nil bundle:nil];
    //We need a nav controller to show the top bar with "expand" button
    UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:self.snippetController];
    navc.modalPresentationStyle = UIModalPresentationPageSheet;
    UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:navc];
    pop.delegate = self;
    self.filingPopoverController = pop;
    
    self.snippetController.filing = filingView.filing;
    CGSize suggestedSize;
    
    if ([[filingView.filing.content objectForKey:@"type"] isEqualToString:@"image"]){
        suggestedSize= CGSizeMake(500., 400);
    } else {
        suggestedSize= CGSizeMake(350., 250.);
    }
    [pop setPopoverContentSize:suggestedSize
                      animated:NO];
    
    
    if (filingView.filing.isDef){
        //Use apple built in dict
        
        // UIReferenceLibraryViewController *reference = [[UIReferenceLibraryViewController alloc] initWithTerm:filingView.filing.word.term];
        //[self presentModalViewController:reference animated:YES];
        
        //[self stopBoxWorld];
   
        /*
         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
         NSString *def =[self.dictionarymanager getDefinitionForWord:filingView.filing.word.term];
         NSMutableDictionary* content = [[NSMutableDictionary alloc] init];
         [content setObject:def forKey: @"description"];
         [self.snippetController.filing setContent:content];
         NSLog(@"content, %@", content);
         return;
         } else if ([[filingView.filing.content objectForKey:@"type"] isEqualToString:@"image"]){
         */
        
        //Use apple dict in wildcard, internal one otherwise
        if ([self.wordPackStr isEqualToString:@"Wildcard"]){
            UIReferenceLibraryViewController *reference = [[UIReferenceLibraryViewController alloc] initWithTerm:filingView.filing.word.term];
            [self presentModalViewController:reference animated:YES];
            [self stopBoxWorld];
            return;
        } else {
            NSString* def= [self.dictionarymanager getDefinitionForWord:filingView.filing.word.term];
            NSMutableDictionary* content= [[NSMutableDictionary alloc] init];
            [content setObject:def forKey:@"description"];
            NSLog(@"Set DEFINITIONN: term: %@, def: %@",filingView.filing.word.term, def);
            [self.snippetController.filing setContent:content];
        }
        //end of adding in
    } else {
        [filingView markAsRead];
    }
    /*
    [self.filingPopoverController presentPopoverFromRect:filingView.frame
                                                  inView:self.view
                                permittedArrowDirections:UIPopoverArrowDirectionAny
                                                animated:YES];
    */
    
  //  [self stopBoxWorld];
}


// filing gestures
- (void)handleFilingTapFrom:(UITapGestureRecognizer*)recognizer {
    FilingView *view = (FilingView*)recognizer.view;
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        // ...
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        [self showSnippetForFiling:view];
	}
}

- (void)removeFiling:(FilingView*)filing fromAttractor:(AttractorView*)attractor {
    [self.world removeBody:filing.body];
    [attractor.word.filings removeObject:filing];
    [filing removeFromSuperview];
    [attractor.filingViews removeObject:filing];
}

- (void)handleFilingPanFrom:(UIPanGestureRecognizer*)recognizer {
    FilingView *view = (FilingView*)recognizer.view;
    BoxBody *theBody = view.body;
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        // should create joint to pin attractor down here.
        BoxJoint* attractorJoint=[[BoxJoint alloc] init];
        attractorJoint.bodyA=view.attractorView.body;
        attractorJoint.bodyB=[wallBodies lastObject];
        //[self.world addJoint:attractorJoint];
        [view.layer setValue:attractorJoint
                      forKey:@"pinJoint"];
        //
        
        BoxMouseJoint *mJoint = [[BoxMouseJoint alloc] init];
        mJoint.bodyA = self.mouseAnchorBody;//[self.wallBodies lastObject];
        mJoint.bodyB = theBody;
        mJoint.targetPoint = [recognizer locationInView:self.view];
        mJoint.maxForce = 1000.0*theBody.mass;
        mJoint.dampingRatio = 0.9;
        [self.world addJoint:mJoint];
        [view.layer setValue:mJoint
                      forKey:@"mouseJoint"];
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        mJoint.targetPoint = [recognizer locationInView:self.view];
        
        BoxDistanceJoint *dJoint = [view.layer valueForKey:@"distanceJoint"];
        
        if (dJoint) {
            CGPoint p1 = dJoint.localAnchorA;
            CGPoint p2 = dJoint.localAnchorB;
            CGFloat distance = CGPointDistanceToPoint(p1, p2);
            
            if (distance <= 100.) {
                
                [UIView animateWithDuration:.2
                                 animations:^{
                                     view.alpha = 0.5;
                                 }];
                
            } else {
                view.alpha = 1.;
            }
            
        }
        
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        //remove "pin"joint here
        BoxJoint* pinJoint = [view.layer valueForKey:@"attractorPinJoint"];
        [self.world removeJoint:pinJoint];
        [view.layer setValue:nil forKey:@"attractorPinJoint"];
        //
        
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        [self.world removeJoint:mJoint];
        [view.layer setValue:nil forKey:@"mouseJoint"];
        
        BoxDistanceJoint *dJoint = [view.layer valueForKey:@"distanceJoint"];
        
        if (dJoint) {
            CGPoint p1 = dJoint.localAnchorA;
            CGPoint p2 = dJoint.localAnchorB;
            CGFloat distance = CGPointDistanceToPoint(p1, p2);
            
            if (distance >= 200.0) { //remove filing
                //OPLog(@"removing distance joint: %f",distance);
                [view.layer setValue:nil forKey:@"distanceJoint"];
                [self.world removeJoint:dJoint];
                
                // remove gesture recognizers - so you can't interact with the filing anymore
                NSArray *gr = view.gestureRecognizers;
                for (UIGestureRecognizer *recog in gr) {
                    [view removeGestureRecognizer:recog];
                }
                
                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options:0
                                 animations:^{
                                     view.alpha = 0.0;
                                     
                                 } completion:^(BOOL finished) {
                                     [self removeFiling:view fromAttractor:view.attractorView];
                                 }];
            }
            
        }
        
	}
    
}


- (void)handleAttractorPanFrom:(UIPanGestureRecognizer*)recognizer {
    
    AttractorView *view = (AttractorView*)recognizer.view;
    
    BoxBody *theBody = view.body;
   // NSAssert(theBody,@"wtf");
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        BoxMouseJoint *mJoint = [[BoxMouseJoint alloc] init];
        mJoint.bodyA = self.mouseAnchorBody;//[self.wallBodies lastObject];
        mJoint.bodyB = theBody;
        mJoint.targetPoint = [recognizer locationInView:self.view];
        mJoint.maxForce = 1000.0*theBody.mass;
        mJoint.dampingRatio = 0.9;
        
        [self.world addJoint:mJoint];
        
        [view.layer setValue:mJoint
                      forKey:@"mouseJoint"];
        view.touched=YES;
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        mJoint.targetPoint = [recognizer locationInView:self.view];
        
        //check if we need to merge with an other attractor
        AttractorView* attractorToMerge=nil;
        
        for (AttractorView* a in [self attractorViews]){
            float dist=CGPointDistanceToPoint(a.frame.origin, view.frame.origin);
            //Took out to not read merging/glomming
            /*if (a!=view && a.touched && dist<ATTRACTOR_MERGE_THRESHOLD){
             BOOL shouldMerge=[self checkMergeMatch:a.word.term with:view.word.term];
             if (shouldMerge)
             attractorToMerge=a;
             }*/
            
            if ([a.type isEqualToString:@"welcome_magnet"]){
                NSLog(@"%f", dist);
                NSLog(@"Ax: %f, Ay: %f", a.frame.origin.x, a.frame.origin.y);
                NSLog(@"Fx: %f, Fy: %f", view.frame.origin.x, view.frame.origin.y);
                
                /*   if (a!=view && a.touched && dist < 410){
                 [self mergeWelcomeAttractorPieces];
                 return;
                 }*/
            }
        }
        
        if (attractorToMerge!=nil){
            [self mergeAttractor:view and:attractorToMerge];
            
            //check if we have merged all words in current pack
            //if yes, poetry!
            NSLog(@"Currently, %i words are completed out of %i", words_completed_current_wordpack, words_in_current_wordpack);
            if (words_completed_current_wordpack==words_in_current_wordpack){
                [self loadPoemWords];
                [self notifyFullVersion];
            }
        }
        
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        
        [self.world removeJoint:mJoint];
        [view.layer setValue:nil
                      forKey:@"mouseJoint"];
        
        view.touched=NO;
        
	}
}


- (void)openAttractor:(AttractorView*)attractor {
    LanguageWord *word = attractor.word;
  BoxBody *attractorBody = attractor.body;
     /* 
    //this happens when attractor is unglommed while pulling data
    if (attractorBody==nil){
        [self highlightAttractor:nil];
        return;
    }
   // taking this out allowed the filings to be loaded in iphone 
      */
    // add filing views for the filings in the word
    
    for (LanguageFiling *filing in word.filings) {
        NSLog(@"HEREHERE");
        // Adds filings to screen. the definition filing is automatically put in. showfiling
        FilingView *fv = [self insertFilingViewForFiling:filing atPosition:attractorBody.position];
        //fv.shapeLayer.fillColor= [self getFilingColor:word.];
        [self.view addSubview:fv];
        fv.attractorView = attractor;
        [attractor.filingViews addObject:fv];
    }
    
    //TODO: CLEANUP
    //    if (word.gloss) {
    LanguageFiling *glossFiling = [[LanguageFiling alloc] init];
    glossFiling.isDef=YES;
    glossFiling.word = word;
    glossFiling.content = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           word.term,@"filing_id",
                           word.gloss,@"description", nil];
    
    // NSLog(@"HEREHEREHERE %@", word.gloss);
    //  NSLog(@"HEREHEREHEREhere %@", word.term);
    NSLog(@"HEREHEREHEREk %@",glossFiling.content);
    FilingView *glossFilingView = [self insertFilingViewForFiling:glossFiling
                                                       atPosition:attractorBody.position];
    [self.view addSubview:glossFilingView];
    glossFilingView.attractorView = attractor;
    [attractor.filingViews addObject:glossFilingView];
    
    [glossFilingView.layer setValue:glossFiling
                             forKey:@"glossFiling"];
    
    //    }
    
    // make sure the attractor is above the filing views
    [self.view bringSubviewToFront:attractor];
    
    [UIView animateWithDuration:0.0
                          delay:0.0
                        options:0
                     animations:^{
                         
                         // animate the filings from the center of the attractor out
                         for (FilingView *fv in attractor.filingViews) {
                             BoxBody *body = fv.body;
                             fv.alpha = 1.0;
                             CGPoint p = body.position;
                             fv.center = p;
                         }
                         
                     } completion:^(BOOL finished) {
                         
                         for (FilingView *fv in attractor.filingViews) {
                             
                             
                             BoxBody *body = fv.body;
                             BoxDistanceJoint *joint = [[BoxDistanceJoint alloc] init];
                             joint.bodyA = attractorBody;
                             joint.bodyB = body;
                             joint.length = 120.; //defines how far filings are from attractor
                             joint.frequency = 3.0;
                             joint.dampingRatio = 0.1;
                             [self.world addJoint:joint];
                             
                             [fv.layer setValue:joint
                                         forKey:@"distanceJoint"];
                         }
                         
                     }];
    //  NSLog(@"trying to open attractor end");
    attractor.state = kAttractorViewStateOpen;
}



- (FilingView*)insertFilingViewForFiling:(LanguageFiling*)aFiling atPosition:(CGPoint)position {
    
    FilingView *fv = [[FilingView alloc] initWithFrame:CGRectMake(0., 0., 60., 60.)];
    NSString *filingURL = nil;
    filingURL = [aFiling.content valueForKey:@"filing_id"];
    
    // View
    
    fv.shapeLayer.frame = CGRectMake(0., 0., fv.bounds.size.width, fv.bounds.size.height);
    fv.shapeLayer.path = [UIBezierPath bezierPathWithOvalInRect:fv.shapeLayer.bounds].CGPath;
    
    fv.shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    fv.shapeLayer.fillColor = [UIColor clearColor].CGColor;//[self colorFromPaletteWithIndex:fColor].CGColor;
    
    // add Gesture Recognizers
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleFilingTapFrom:)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    [fv addGestureRecognizer:recognizer];
    fv.tapRecogznier = recognizer;
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(handleFilingPanFrom:)];
    pan.minimumNumberOfTouches = 1;
    pan.maximumNumberOfTouches = 1;
    [fv addGestureRecognizer:pan];
    fv.panRecognizer = pan;
    
    // Configure
    
    CGFloat a = fv.bounds.size.width;
    fv.alpha = 0.;
    
    BoxFixture *fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:a/2.0 Position:CGPointZero]];
    fixture.friction = 0.5;
    fixture.density = 0.3;
    
    BoxBody *body = [BoxBody dynamicBody];
    body.linearDamping = 0.9;
    body.angularDamping = 0.5;
    [body addFixture:fixture];
    [self.world addBody:body];
    
    CGPoint pp = cartesianPointFromPolar(20.0*RANDOM_FLOAT(), 2.0*M_PI*RANDOM_FLOAT());
    body.position = CGPointMake(pp.x + position.x,
                                pp.y + position.y);
    
    fv.center = body.position;
    
    body.weakContext = fv;
    
    fv.body = body;
    fv.filing = aFiling;
    
    //if filing is dictionary access
    if (aFiling.isDef){
        [fv addDLabel];
    } else if ([[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
        [fv addImageLabel];
        
        /* } else if (aFiling.hasRead && ![[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
         [fv markAsRead];
         } else if (aFiling.hasRead && [[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
         NSLog(@"read and image");
         [fv addImageLabel];
         */
    } else {
        [fv markAsUnread];
    }
    
    return fv;
}
- (void)handleAttractorPinchFrom:(UIPinchGestureRecognizer*)recognizer {
    //only perform gesture recognizer once
    //THIS IS NOT IDEAL!
    //We want to do as follows:
    //NSUInteger index=[self.attractorViews indexOfObject:att];
    //NSLog(@"Position for attractor %@ is %d", att.word.term, index);
    //if index==NSNotFound
    //this doesn't work because of (i think) multithreading issues. Need to look into it.
    
    NSLog(@"unglom: %f", recognizer.scale);
    
    if (recognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    //can't unglom atomic words
    if ([((AttractorView*)recognizer.view).morphemeComponents count]==0)
        return;
    
    
    
    if (recognizer.scale>1.0){
        int i=-10;
        for (LanguageWord* l in ((AttractorView*)recognizer.view).morphemeComponents){
            NSLog(@"%@", l.term);
            AttractorView* newAttractor= [self insertAttractorForWord:l];
            
            //make it small if morpheme
            if ([self isSuffix:l.term] || [self isPrefix:l.term])
                [newAttractor makeMorpheme];
            
            //set position under fingers
            CGPoint pos= recognizer.view.center;
            pos.x+=-10*i;
            pos.y+=-10*i;
            newAttractor.center= pos;
            newAttractor.body.position= pos;
            
            i+=10;
        }
        
        //one less word completed if what we unglommed was full word
        if ([wordpackDB isFullWord:((AttractorView*)recognizer.view).word.term])
            words_completed_current_wordpack-=1;
        
        
        
        [self removeSingleAttractor:(AttractorView*)recognizer.view];
        
    }
}

/*
- (void)handleAttractorLongPressFrom:(UILongPressGestureRecognizer*)recognizer {
    AttractorView *v = (AttractorView*)recognizer.view;
    if (UIGestureRecognizerStateBegan == recognizer.state) {
        UIImageView *iv = [v.layer valueForKey:@"cardImageView"];
        
        [iv removeFromSuperview];
        
        CGRect targetRect = CGRectMake(0., 0., 0.8*768., 0.8*1004.);
        
        self.embeddedViewControllerBackdropView.alpha = 0.;
        self.embeddedViewControllerBackdropView.frame = self.view.bounds;
        [self.view addSubview:self.embeddedViewControllerBackdropView];
        [self.view addSubview:iv];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(hideViewController)];
        tap.delegate = self;
        [self.embeddedViewControllerBackdropView addGestureRecognizer:tap];
        
        iv.center = v.center;
        [UIView animateWithDuration:0.5
                              delay:0.
                            options:0
                         animations:^{
                             
                             self.embeddedViewControllerBackdropView.alpha = 1.;
                             iv.bounds = targetRect;
                             iv.center = CGPointMake(self.view.bounds.size.width/2., self.view.bounds.size.height/2.);
                             iv.alpha = 1.;
                             v.rotatingView.alpha = 1.;
                             
                         } completion:^(BOOL finished) {
                             
                             ContextCardViewController *vc = [v.layer valueForKey:@"contextCardController"];
                             vc.view.bounds = targetRect;
                             [self showCardController:vc completion:^{
                                 [iv removeFromSuperview];
                                 //[v.layer setValue:nil forKey:@"cardImageView"];
                             }];
                             
                         }];
        
    }
    
}*/
 

- (void)handleSwipeFrom:(UISwipeGestureRecognizer*)recognizer {
    
    CGPoint p = [recognizer locationInView:self.view];
    
    UIView *v = [self.view hitTest:p withEvent:nil];
    //OPLog(@"self.view %p v %p",self.view,v);
    if (v != self.fluffyView) {
        return; // if there is a view under the touch, and it's not the main view, ignore swipe
    }
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
    }
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        [TestFlight passCheckpoint:@"SwipeRightForWordPacks"];
        
    }
}

// Ignore touch events on UIControl classes. Thanks Apple. :/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if (touch.view == gestureRecognizer.view) {
        return YES;
    }
    return NO;
    
}

@end
