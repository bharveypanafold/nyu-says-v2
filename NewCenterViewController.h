

#import <UIKit/UIKit.h>




#import <UIKit/UIKit.h>

#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <GameKit/GameKit.h>
//#import <Twitter/Twitter.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "BoxWorld.h"
#import "LanguageWordPack.h"
#import "AttractorView.h"
#import "FluffyRiverView.h"
#import "SnippetViewController.h"

#import "PopOverViewController.h"

#import "MorphemeLoader.h"
#import "MorphemeDatabase.h"

#import "WordpackLoader.h"
#import "WordpackDatabase.h"

#import "PoemWordsLoader.h"
#import "PoemWordsDatabase.h"

#import "IntroMagnetView.h"

#import "DictionaryManager.h"



#define kAttractorFluffyDelay 1.5
#define kDefaultAttractorSize 142.0

#define DEFAULT_ATTRACTOR_SIZE  142.0
#define DEFAULT_AFFIX_SIZE 80

#define MAGNET_WIDTH 142.0
#define MAGNET_HEIGHT 142.0

#define SMALL_MAGNET_WIDTH 196.0
#define SMALL_MAGNET_HEIGHT 64.0

#define ATTRACTOR_MERGE_THRESHOLD 250
#define NUM_LAUNCHES_TO_RATE_PROMPT 24

#define NUM_RANDOM_POEMWORDS_HIGH 1
#define NUM_RANDOM_POEMWORDS_LOW 1

#define WORDPACK_BUTTON_MENUBAR 1
#define SETTINGS_BUTTON_MENUBAR 2
#define INFO_BUTTON_MENUBAR 3
#define ADD_NEW_ATTRACTOR 4
//#define TWEET_BUTTON_MENUBAR 5
#define MAIL_BUTTON_MENUBAR 5

#define UIALERT_RATE_US 1
#define UIALERT_FULL_VERSION 2

#define WELCOME_MAGNET_WIDTH 411
#define WELCOME_MAGNET_HEIGHT 300

#define WELCOME_LEFT 1
#define WELCOME_RIGHT 2

#define SETTINGS_FILE_NAME @"Settings"
#define NUM_LAUNCHES_SETTINGS_KEY @"numlaunches"
#define REMIND_TO_REVIEW_SETTINGS_KEY @"remindtoreview"

#define RATE_FIRST_REMINDER 24
#define RATE_SECOND_REMINDER 54

@interface NewCenterViewController : UIViewController
<UIPopoverControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate>
{
    MorphemeLoader* morphemeLoader;
    MorphemeDatabase* morphemeDB;
    
    WordpackLoader* wordpackLoader;
    WordpackDatabase* wordpackDB;
    
    PoemWordsLoader* poemwordsLoader;
    PoemWordsDatabase* poemwordsDB;
    
    AVAudioPlayer* player;
    
    int words_completed_current_wordpack;
    int words_in_current_wordpack;
}
@property (strong,nonatomic) BoxWorld *world;
@property (weak,nonatomic) CADisplayLink *displayLink;

@property (strong,nonatomic) LanguageWordPack *wordPack;

@property (strong,nonatomic) NSMutableArray *attractorViews;
@property (strong,nonatomic) AttractorView *addAttractorView;
@property (strong,nonatomic) AttractorView *selectedAttractor;
//@property (strong,nonatomic) AttractorView*)attractor;

// Box2D objects
@property (strong,nonatomic) NSMutableArray *wallBodies;
@property (strong,nonatomic) BoxBody *mouseAnchorBody;
@property (strong,nonatomic) UIView *documentOptionsPanel;
@property (strong,nonatomic) UIPopoverController *filingPopoverController;
@property (strong,nonatomic) FluffyRiverView *fluffyView;
@property (strong,nonatomic) UIView *menuView;
@property (strong,nonatomic) UIView *embeddedViewControllerBackdropView;
@property (strong,nonatomic) NSOperationQueue *workQueue;
@property (strong,nonatomic) SnippetViewController *snippetController;

//global stuff
@property (strong,nonatomic) NSMutableDictionary* settingsDict;

// Stuff relative to menu bar
@property (strong,nonatomic) UIButton* wordpacks;
@property (strong,nonatomic) UIButton* settings;
@property (strong,nonatomic) UIButton* info;
@property (strong,nonatomic) UIButton* plusButton;
//@property (strong,nonatomic) UIButton* tweetScreenshot;
@property (strong,nonatomic) UIButton* mailScreenshot;

@property (nonatomic) BOOL shouldShowPlusButton;

@property (strong, nonatomic) NSString* wordPackStr;

//@property (strong,nonatomic) UIPopoverController* popoverController;

@property (strong,nonatomic) UIImageView* background;

@property (strong,nonatomic) DictionaryManager* dictionarymanager;


@property (strong, nonatomic) NSString* wordPackSelect;
@property (strong,nonatomic) UITableViewController* tableviewController;
@property (strong,nonatomic) NSMutableArray* tableArray;

- (void) setBackgroundToImage:(NSString*)path;
- (void) loadWordPack:(NSString*)wordpack;

- (void)startBoxWorld;
- (void)stopBoxWorld;

- (void)buildBoxWalls;
- (void)positionViews;

- (void)openAttractor:(AttractorView*)attractor;
- (void)closeAttractor:(AttractorView*)attractor;
- (void)loadAttractor:(AttractorView*)attractor;

- (void)repositionWalls;

- (AttractorView*)insertAttractorForWord:(LanguageWord*)aWord;

- (void)recenterEmbeddedViewController;

- (void)loadAttractorsForWordPack:(LanguageWordPack*)chosenPack;
- (void) loadRandomWordPack;

- (void) setupMenuBar;
- (void) setupFluffyRiver;

//- (void) ratePromptCheck;

- (void) showPopover:(UIButton*)sender;
- (PopOverViewController*) settingsPopover;
- (PopOverViewController*) wordpacksPopover;
- (PopOverViewController*) infoPopover;

- (void)removeSingleAttractor:(AttractorView*)attractor;

- (void)mergeAttractor:(AttractorView*)a and:(AttractorView*)b;
- (void)pinBody:(AttractorView*)a;

- (bool)isSuffix:(NSString*)string;
- (bool)isPrefix:(NSString*)string;

- (void) removePlusAttractor;
- (void) showAddAttractorButton;

- (NSArray*) getWordpackList;
- (BOOL) checkMergeMatch:(NSString*)affix1 with:(NSString*)affix2;

- (void) playMergeSound;
- (UIImage*) captureScreen;

//- (void) tweetsheetScreenshot;
- (void) mailsheetScreenshot;
- (void) loadPoemWords;
- (void) notifyFullVersion;

- (void) mergeWelcomeAttractorPieces;
- (void) bloomAttractor:(AttractorView*)a;



@end
