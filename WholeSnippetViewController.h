//
//  WholeSnippetViewController.h
//  NYUSays
//
//  Created by Vivian Allum on 4/3/14.
//
//

#import <UIKit/UIKit.h>
#import "LanguageFiling.h"

@interface WholeSnippetViewController : UIViewController
@property (strong, nonatomic) NSString* snippet;
@property (strong,nonatomic) LanguageFiling *filing;
@property int  indexSelected;
//@property (strong, nonatomic) NSString* snippet;
- (void)passIndex:(NSNumber *)someNumber;
@end
