//
//  RightViewController.m
//  NYUSays
//
//  Created by Vivian Allum on 2/13/14.
//
//

#import "RightViewController.h"
#import "FilingView.h"
#import "SnippetViewController.h"
#import "LanguageWord.h"
#import "AttractorView.h"
#import "BoxBody.h"
#import "LanguageFiling.h"
#import "AttractorView.h"
#import "LeftViewController.h"
#import "NewCenterViewController.h"
#import "PFAppDelegate.h"
#import "WholeSnippetViewController.h"
@interface RightViewController ()

@end

@implementation RightViewController
@synthesize snippetController;
@synthesize filingPopoverController;
@synthesize dictionarymanager;
@synthesize currectAttractorView;
@synthesize filing;
@synthesize word;
@synthesize attractor;
@synthesize  filingTextforTable;
@synthesize filingViewsRVC;
@synthesize filingTexts;
@synthesize arrayFilings;

PFAppDelegate *appDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     filingTexts = [[NSMutableArray alloc] init];
    arrayFilings = [[NSMutableArray alloc]init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    UITableView *table_view = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:table_view];
    table_view.delegate = self;
    table_view.dataSource = self;
 
    appDelegate=(PFAppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
    
    self.dictionarymanager= [[DictionaryManager alloc] init];
  //  wordpackLoader= [[WordpackLoader alloc]initWithDictManager:dictionarymanager];
    //wordpackDB= [wordpackLoader getWordpackDB];
    //self.listOfWordpacks= [self getWordpackList];


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidAppear:(BOOL)animated;
{
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
   
   if([appDelegate.filingTable count])
   {
       NSLog(@"returned count %i",[appDelegate.filingTable count]);
    return [appDelegate.filingTable count];
   // return 3;
       //return ([filingTexts count]); // fix = attractor.filingviews count???
   }
   else
   {
         NSLog(@"number of rows in section: 0" );
       return 10;
   }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     // NSLog(@" Selected path row", indexPath.row);
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
       if( indexPath.row>0){
        if(([appDelegate.filingTable objectAtIndex:(indexPath.row)] != NULL) &&
           (indexPath.row<arrayFilings.count)){
            NSString * current = [appDelegate.filingTable objectAtIndex:(indexPath.row)];
            NSLog(@" Object at selected path row %@", [appDelegate.filingTable objectAtIndex:(indexPath.row)]);
            // [cell.textLabel setText:(NSString*)current];
            cell.textLabel.text = [NSString stringWithFormat:@"%@", current];
            
            // cell.textLabel.text = current;
            //cell.textLabel.textColor = [UIColor  blueColor];
            
                   }
    }
   

       //  NSLog(@"CellForRowAtIndexPath %@",cell.textLabel.text);
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        return cell;
        
    
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    WholeSnippetViewController *viewController2 = [[WholeSnippetViewController alloc]init];
    NSLog(@" transferring %@",[appDelegate.filingTable objectAtIndex:(indexPath.row)]);
    viewController2.snippet = [appDelegate.filingTable objectAtIndex:(indexPath.row)];
    viewController2.filing = [appDelegate.arrayFilings objectAtIndex:(indexPath.row)];
    viewController2.indexSelected=indexPath.row;
    //pass index value
   // NSNumber *num = [NSNumber numberWithInt:indexPath.row];
   // [viewController2 performSelectorInBackground:@selector(passIndex:) withObject:num];
    NSLog(@"this is hte website FOR RIGHTVIEWCONT %@", [[appDelegate.arrayFilings objectAtIndex:(indexPath.row)] valueForKey:@"page"]);
    //animate view
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    //
    [self presentModalViewController:viewController2 animated:NO];

    
}



- (void)showSnippetForFiling:(FilingView*)filingView {

    self.snippetController = [[SnippetViewController alloc] initWithNibName:nil bundle:nil];
    //We need a nav controller to show the top bar with "expand" button
    UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:self.snippetController];
    navc.modalPresentationStyle = UIModalPresentationPageSheet;

    self.snippetController.filing = filingView.filing;
  
    self.filing = filingView.filing;
    
 //  [self.snippetController getSnippet];
  //  self.filingShortText= snippetController.filingText1;
  //  NSLog(@"fling %@", snippetController.filingText1);
    CGSize suggestedSize;
    
    if ([[filingView.filing.content objectForKey:@"type"] isEqualToString:@"image"]){
        suggestedSize= CGSizeMake(500., 400);
    } else {
        suggestedSize= CGSizeMake(350., 250.);
    }

                  //Use apple dict in wildcard, internal one otherwise
        if (filingView.filing.isDef){
            NSString* def= [self.dictionarymanager getDefinitionForWord:filingView.filing.word.term];
            NSMutableDictionary* content= [[NSMutableDictionary alloc] init];
            [content setObject:def forKey:@"description"];
            NSLog(@"Set definition: term: %@, def: %@",filingView.filing.word.term, def);
            [self.snippetController.filing setContent:content];
            self.filingTextforTable = self.snippetController.filingText1;
        }
        //end of adding in

    [self.snippetController getSnippet];
    self.filingShortText= snippetController.filingText1;
   // NSLog(@"websiteee %@", [self.snippetController.filing.content valueForKey:@"page"]); works to get url!

    [filingTexts addObject:snippetController.filingText1]; //add to array to populate array take PAGE
    [arrayFilings addObject: snippetController.filing.content]; //took this out, attempt to make array with website
    
    // [self stopBoxWorld];
    //  [self.tableView reloadData];
    
}


 //moved back to center view controller

- (void)openAttractor:(AttractorView*)attractor1 {

    NSLog(@"trying to open attractor");
   
    self.attractor = attractor1;
    LanguageWord *word = attractor1.word;
    BoxBody *attractorBody = attractor1.body;
    /*
     //this happens when attractor is unglommed while pulling data
     if (attractorBody==nil){
     [self highlightAttractor:nil];
     return;
     }
     // taking this out allowed the filings to be loaded in iphone
     */

   // [self showSnippetForFiling:view];
    
    for (LanguageFiling *filing in word.filings) {
       // adds filings to screen. the definition filing is automatically put in. showfiling
        FilingView *fv = [self insertFilingViewForFiling:filing atPosition:attractorBody.position];

        //fv.shapeLayer.fillColor= [self getFilingColor:word.];
       // [self.view addSubview:fv];
         [self showSnippetForFiling:fv];
        fv.attractorView = attractor1;
        [self.filingViewsRVC addObject:fv];
        // leave above in so we can access filing views
    }
    
    //TODO: CLEANUP
    //    if (word.gloss) {
    LanguageFiling *glossFiling = [[LanguageFiling alloc] init];
    glossFiling.isDef=YES;
    glossFiling.word = word;
    glossFiling.content = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           word.term,@"filing_id",
                           word.gloss,@"description", nil];
    
    // NSLog(@"HEREHEREHERE word.gloss meow %@", word.gloss);
    //  NSLog(@"HEREHEREHEREhere %@", word.term);
  //  NSLog(@"HEREHEREHEREk content meow %@",glossFiling.content);
    FilingView *glossFilingView = [self insertFilingViewForFiling:glossFiling
                                                       atPosition:attractorBody.position];
    [self.view addSubview:glossFilingView];
    glossFilingView.attractorView = attractor;
    [attractor.filingViews addObject:glossFilingView];
    
  //  [glossFilingView.layer setValue:glossFiling
                        //     forKey:@"glossFiling"];
    
    //    }
    
    // make sure the attractor is above the filing views
  //  [self.view bringSubviewToFront:attractor];
    
    [UIView animateWithDuration:0.0
                          delay:0.0
                        options:0
                     animations:^{
                         
                         // animate the filings from the center of the attractor out
                         for (FilingView *fv in attractor.filingViews) {
                             BoxBody *body = fv.body;
                             fv.alpha = 1.0;
                             CGPoint p = body.position;
                             fv.center = p;
                         }
                         
                     } completion:^(BOOL finished) {
                         
                         for (FilingView *fv in attractor.filingViews) {
                             
                             
                        /*    BoxBody *body = fv.body;
                             BoxDistanceJoint *joint = [[BoxDistanceJoint alloc] init];
                             joint.bodyA = attractorBody;
                             joint.bodyB = body;
                             joint.length = 120.; //defines how far filings are from attractor
                             joint.frequency = 3.0;
                             joint.dampingRatio = 0.1;
                             [self.world addJoint:joint];
                             
                             [fv.layer setValue:joint
                                         forKey:@"distanceJoint"];*/ 
                         }
                         
                     }];
 // [appDelegate.filingTable addObjectsFromArray: filingTexts];
     [appDelegate.filingTable removeAllObjects];
      appDelegate.filingTable = [[NSMutableArray alloc] initWithArray:self.filingTexts copyItems:YES];;
    [appDelegate.arrayFilings removeAllObjects];
    appDelegate.arrayFilings = [[NSMutableArray alloc] initWithArray:self.arrayFilings copyItems:YES];;
   // NSLog(@"number in array %i",[appDelegate.filingTable count]);
    
   //  appDelegate.filingTable = [[NSMutableArray alloc] arrayByAddingObjectsFromArray:filingTexts];
//NSLog(@"trying to open attractor end last %@", [appDelegate.filingTable objectAtIndex: 1]);
    //repoopulate
    //[self.viewWillAppear];
     //   [self.tableView reloadData];
  // attractor.state = kAttractorViewStateOpen;
}




- (FilingView*)insertFilingViewForFiling:(LanguageFiling*)aFiling atPosition:(CGPoint)position {
   
    FilingView *fv = [[FilingView alloc] initWithFrame:CGRectMake(0., 0., 60., 60.)];
    NSString *filingURL = nil;
    filingURL = [aFiling.content valueForKey:@"filing_id"];
    
    // View
      
    fv.shapeLayer.frame = CGRectMake(0., 0., fv.bounds.size.width, fv.bounds.size.height);
    fv.shapeLayer.path = [UIBezierPath bezierPathWithOvalInRect:fv.shapeLayer.bounds].CGPath;
    
    fv.shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    fv.shapeLayer.fillColor = [UIColor clearColor].CGColor;//[self colorFromPaletteWithIndex:fColor].CGColor;
    
    // add Gesture Recognizers
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleFilingTapFrom:)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    [fv addGestureRecognizer:recognizer];
    fv.tapRecogznier = recognizer;
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(handleFilingPanFrom:)];
    pan.minimumNumberOfTouches = 1;
    pan.maximumNumberOfTouches = 1;
    [fv addGestureRecognizer:pan];
    fv.panRecognizer = pan;
    
    // Configure
    
    CGFloat a = fv.bounds.size.width;
    fv.alpha = 0.;
    
    BoxFixture *fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:a/2.0 Position:CGPointZero]];
    fixture.friction = 0.5;
    fixture.density = 0.3;
    
    BoxBody *body = [BoxBody dynamicBody];
    body.linearDamping = 0.9;
    body.angularDamping = 0.5;
    [body addFixture:fixture];
 //   [self.world addBody:body];
    
   /* CGPoint pp = cartesianPointFromPolar(20.0*RANDOM_FLOAT(), 2.0*M_PI*RANDOM_FLOAT());
    body.position = CGPointMake(pp.x + position.x,
                                pp.y + position.y);
    
    fv.center = body.position;
    
    body.weakContext = fv;
    
    fv.body = body;*/
    fv.filing = aFiling;
    
   /* //if filing is dictionary access
    if (aFiling.isDef){
        [fv addDLabel];
    } else if ([[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
        [fv addImageLabel];
       */ 
        /* } else if (aFiling.hasRead && ![[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
         [fv markAsRead];
         } else if (aFiling.hasRead && [[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
         NSLog(@"read and image");
         [fv addImageLabel];
         */
  /* } else {
        [fv markAsUnread];
    }
    */
    return fv;
}


- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"repopulate %i", [appDelegate.filingTable count]);
  //  [super viewWillAppear:animated];
    //  appDelegate.filingTable = [[NSMutableArray alloc] arrayByAddingObjectsFromArray:filingTexts];
      //repoopulate
  //  [self.tableView reloadData];
}



@end
