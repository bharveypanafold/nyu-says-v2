

#import "LeftViewController.h"
#import "NewCenterViewController.h"
#import "MMDrawerController.h"

#import "WordpackPickerViewController.h"
#import "PFViewController.h"

#import "DictionaryManager.h"

#import "AttractorView.h"
#import "FilingView.h"
@implementation LeftViewController

@synthesize listOfWordpacks;
@synthesize table;
@synthesize dictionarymanager;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UITableView *table_view = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:table_view];
    table_view.delegate = self;
    table_view.dataSource = self;
    
     self.dictionarymanager= [[DictionaryManager alloc] init];
    wordpackLoader= [[WordpackLoader alloc]initWithDictManager:dictionarymanager];
    wordpackDB= [wordpackLoader getWordpackDB];
     self.listOfWordpacks= [self getWordpackList];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    //  return [listOfWordpacks count];
    // NSLog(@"number of rows in section %d", [listOfWordpacks count]);
    return [listOfWordpacks count];
}

- (NSArray*) getWordpackList{
    return [wordpackDB wordpackTitles];
}


// Populate left drawer table with correct wordpack titles.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 
    
    [[cell.contentView viewWithTag:CELL_NAME_TAG] removeFromSuperview];
    UILabel* wordPackName= [[UILabel alloc] initWithFrame:CGRectMake(10, 0, cell.frame.size.width-80, cell.frame.size.height)];
    wordPackName.font= [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    [wordPackName setAdjustsFontSizeToFitWidth:YES];
    [wordPackName setBackgroundColor:[UIColor clearColor]];
    wordPackName.text= [NSString stringWithFormat:@"%@ %@", [listOfWordpacks objectAtIndex:indexPath.row],@""];
    wordPackName.tag= CELL_NAME_TAG;
    
   // [cell.contentView addSubview:wordPackName];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", wordPackName.text];

    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString* wordpack= [self.listOfWordpacks objectAtIndex:indexPath.row];
    
    //if (indexPath.row == 0) {
        NewCenterViewController *center = [[NewCenterViewController alloc] init];
        center.wordPackSelect =wordpack;
        [self.mm_drawerController
         setCenterViewController:center
         withCloseAnimation:YES
         completion:nil];
    /*}else{
        UIViewController *center = [[UIViewController alloc] init];
        
        [center.view setBackgroundColor:[UIColor blueColor]];
        [self.mm_drawerController
         setCenterViewController:center
         withCloseAnimation:YES
         completion:nil];
    }*/
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
  
 
}


-(MMDrawerController*)mm_drawerController{
    if([self.parentViewController isKindOfClass:[MMDrawerController class]]){
        return (MMDrawerController*)self.parentViewController;
    }
    else if([self.parentViewController isKindOfClass:[UINavigationController class]] &&
            [self.parentViewController.parentViewController isKindOfClass:[MMDrawerController class]]){
        return (MMDrawerController*)[self.parentViewController parentViewController];
    }
    else{
        return nil;
    }
}


@end
