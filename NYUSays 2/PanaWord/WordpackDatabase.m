//
//  WordpackDatabase.m
//  EnglishDiscovery
//
//  Created by Guillaume on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WordpackDatabase.h"

@implementation WordpackDatabase
@synthesize words;
@synthesize wordpacks;
@synthesize wordpackTitles;

- (id) init {
    self=[super init];
    if (self){
        self.words= [[NSMutableArray alloc] initWithCapacity:10];
        self.wordpacks= [[NSMutableArray alloc] initWithCapacity:10];
        self.wordpackTitles= [[NSMutableArray alloc] initWithCapacity:10];
    }
    return self;
}

- (BOOL) wordInDatabase:(NSString*)word{
    for (WordDescription* w in self.words){
        if ([w.fullword isEqualToString:word])
            return YES;
    }
    return NO;
}

- (void) addWord:(WordDescription*)l{
    if ([self wordInDatabase:l.fullword])
        return;
    [self.words addObject:l];
}

- (WordDescription*) getWord:(NSString*)word{
    for (WordDescription* w in self.words){
        if ([w.fullword isEqualToString:word])
            return w;
    }
    return nil;
}

- (NSArray*) getWordsWithAffix:(NSString*)affix{
    NSMutableArray* array=[[NSMutableArray alloc] initWithCapacity:5];
    
    for (WordDescription* w in words){
        if ([w hasAffix:affix])
            [array addObject:w];
    }
    
    NSArray* a=[[NSArray alloc] initWithArray:array];
    return a;
}

- (NSString*) getRewriteRuleResultFor:(NSString*)a and:(NSString*)b{
    for (WordDescription* w in words){
        if ([w hasAffix:a] && [w hasAffix:b])
            return [w rewriteRuleFor:a and:b].result;

        NSLog(@"Checking if %@ or %@ is rewrite result", a, b);
        if ([w stringIsRewriteResult:a] || [w stringIsRewriteResult:b])
            return w.fullword;
    }
    
    return nil;
}

- (void) addWordPack:(LanguageWordPack*)w{
    [self.wordpacks addObject:w];
}

- (LanguageWordPack*) getWordPackForWord:(NSString*)word{
    //TODO
    int i= [self.wordpackTitles indexOfObject:word];
    return [self.wordpacks objectAtIndex:i];
}

- (int) numberOfWordsInWordPack:(NSString*)wordpackName{
    int wordpackIndex= [wordpackTitles indexOfObject:wordpackName];
    LanguageWordPack* wordpack=[wordpacks objectAtIndex:wordpackIndex];

    return [wordpack.words count];
}

//Checks whether the given word is a fullword in one of the loaded wordpacks
//Input: None
//Returns: true or fale
- (bool) isFullWord:(NSString*)w{
    for (WordDescription* wd in self.words){
        if ([wd.fullword isEqualToString:w]){
            NSLog(@"%@ is a full word", w);
            return true;
        }
    }
    return false;
}

@end
