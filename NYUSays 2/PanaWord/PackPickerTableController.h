//
//  PackPickerTableController.h
//  PanaWord
//
//  Created by Panafold on 11/22/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageTopic.h"

@interface PackPickerTableController : UIViewController

@property (strong,nonatomic) LanguageTopic *topic;

@end
