//
//  PackPickerTableController.m
//  PanaWord
//
//  Created by Panafold on 11/22/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "PackPickerTableController.h"
#import "LanguageSource.h"
#import "LanguageWordPack.h"
#import "NSData+HMAC.h"

#import <QuartzCore/QuartzCore.h>

@interface UIView(ME)
- (NSString*)recursiveDescription;
@end


@interface PackPickerTableController() <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (strong,nonatomic) UIView *backdrop;
@property (strong,nonatomic) UITextField *nameField;
@property (strong,nonatomic) UITextField *englishNameField;

@property (strong,nonatomic) UITableView *tableView;

@property (strong,nonatomic) UIImage *cellImage;

- (void)cancelAddItemsAction:(id)sender;
- (void)addItemAction:(id)sender;

@end

@implementation PackPickerTableController

@synthesize topic;
@synthesize backdrop;
@synthesize nameField;
@synthesize englishNameField;
@synthesize tableView;
@synthesize cellImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Themes";
        self.topic = nil;
        self.contentSizeForViewInPopover = CGSizeMake(480., 580.);
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                               target:self
                                                                                               action:@selector(addItemAction:)];
    }
    return self;
}

- (void)dealloc {
    OPLogMethod;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - New Item Methods

- (void)tapToDismiss:(UITapGestureRecognizer*)recognizer {
    
    if (UIGestureRecognizerStateEnded == recognizer.state) {
        [self cancelAddItemsAction:nil];
    }
}

- (void)showNewItemFields {
    
    CGRect b = self.view.bounds;
    
    self.backdrop.frame = b;
    self.backdrop.alpha = 0.;
    [self.view addSubview:self.backdrop];
    
    CGFloat spacing = 16.;
    
    self.nameField.center = CGPointMake(b.size.width/2., 
                                        spacing+self.nameField.bounds.size.height/2.-200.);
    self.englishNameField.center = CGPointMake(b.size.width/2.,
                                               CGRectGetMaxY(self.nameField.frame)+spacing+self.englishNameField.bounds.size.height/2.-200.);
    
    self.tableView.scrollEnabled = NO;
    
    [UIView animateWithDuration:0.2
                          delay:0.
                        options:0
                     animations:^{
                         self.backdrop.alpha = 1.;
                     } completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.1
                                             options:0
                                          animations:^{
                                              self.nameField.center = CGPointMake(b.size.width/2., 
                                                                                  spacing+self.nameField.bounds.size.height/2.);
                                              self.englishNameField.center = CGPointMake(b.size.width/2.,
                                                                                         CGRectGetMaxY(self.nameField.frame)+spacing+self.englishNameField.bounds.size.height/2.);
                                          } completion:^(BOOL finished) {
                                              [self.nameField becomeFirstResponder]; 
                                          }];
                         
                     }];
    
}

- (void)hideNewItemFields {
    
    CGRect b = self.view.bounds;
    CGFloat spacing = 16.;
    
    [UIView animateWithDuration:0.2
                          delay:0.
                        options:0
                     animations:^{
                         
                         self.nameField.center = CGPointMake(b.size.width/2., 
                                                             spacing+self.nameField.bounds.size.height/2.-200.);
                         self.englishNameField.center = CGPointMake(b.size.width/2.,
                                                                    CGRectGetMaxY(self.nameField.frame)+spacing+self.englishNameField.bounds.size.height/2.-200.);
                         
                     } completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.2
                                               delay:0.
                                             options:0
                                          animations:^{
                                              self.backdrop.alpha = 0.;
                                          } completion:^(BOOL finished) {
                                              [self.backdrop removeFromSuperview];
                                              self.tableView.scrollEnabled = YES; 
                                          }];
                         
                     }];
    
}

- (void)saveNewItem:(id)sender {
    
    [self cancelAddItemsAction:nil];
    
    if (nil == self.topic) {
        // add new Topic
        LanguageTopic *aTopic = [[LanguageTopic alloc] init];
        
        aTopic.title = self.nameField.text;
        aTopic.englishTitle = self.englishNameField.text;
        
        [[LanguageSource sharedInstance].packs insertObject:aTopic
                                                    atIndex:0];
        
        [self.tableView beginUpdates];
        
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]]
                              withRowAnimation:UITableViewRowAnimationTop];
        
        [self.tableView endUpdates];
        
    } else {
        // add new Word Pack
        LanguageWordPack *pack = [[LanguageWordPack alloc] init];
        
        pack.title = self.nameField.text;
        //pack.englishTitle = self.englishNameField.text;
        
        [self.topic.subTopics insertObject:pack
                                   atIndex:0];
        
        [self.tableView beginUpdates];
        
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]]
                              withRowAnimation:UITableViewRowAnimationTop];
        
        [self.tableView endUpdates];
        
    }
}

- (void)cancelAddItemsAction:(id)sender {
    
    UIBarButtonItem *add = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                         target:self
                                                                         action:@selector(addItemAction:)];
    
    [self.navigationItem setRightBarButtonItem:add
                                      animated:YES];
    
    [self.navigationItem setLeftBarButtonItem:nil
                                     animated:YES];
    
    if (self.nameField.isFirstResponder) {
        [self.nameField resignFirstResponder];
    }
    if (self.englishNameField.isFirstResponder) {
        [self.englishNameField resignFirstResponder];
    }
    
    [self hideNewItemFields];
    
}

- (void)addItemAction:(id)sender {
    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                            target:self
                                                                            action:@selector(cancelAddItemsAction:)];
    
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                          target:self
                                                                          action:@selector(saveNewItem:)];
    
    save.enabled = NO;
    
    self.nameField.text = nil;
    self.englishNameField.text = nil;
    
    [self.navigationItem setRightBarButtonItem:save
                                      animated:YES];
    
    [self.navigationItem setLeftBarButtonItem:cancel
                                     animated:YES];
    
    [self showNewItemFields];
}

#pragma mark - Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //OPLog(@"range %@ / %@ -> %@",NSStringFromRange(range),textField.text,string);
    NSInteger existing = [textField.text length];
    NSInteger replacing = [string length];
    
    NSInteger sum = existing + replacing - range.length;
    
    //OPLog(@"sum %i",sum);
    
    BOOL shouldEnable = NO;
    
    if (sum > 0) {
        
        if (textField == self.englishNameField) {
            shouldEnable = ([self.nameField.text length] > 0);
        } else {
            shouldEnable = YES;
        }
        
    } else {
        
        if (textField == self.englishNameField) {
            
            shouldEnable = ([self.nameField.text length] > 0);
            
        }
    }
        
    self.navigationItem.rightBarButtonItem.enabled = shouldEnable;
    
    return YES;
}

#pragma mark - View lifecycle

- (void)loadView {
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.,
                                                         0.,
                                                         self.contentSizeForViewInPopover.width,
                                                         self.contentSizeForViewInPopover.height)];
    self.view = v;
    v.backgroundColor = [UIColor whiteColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds
                                                  style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);

    [v addSubview:self.tableView];
    
    v = [[UIView alloc] initWithFrame:CGRectMake(0., 0., 320., 320.)];
    v.backgroundColor = [UIColor colorWithWhite:1. alpha:0.85];
    v.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(tapToDismiss:)];
    [v addGestureRecognizer:tap];
    
    self.backdrop = v;
    
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(0., 0., 320., 40.)];
    [v addSubview:tf];
    tf.borderStyle = UITextBorderStyleBezel;
    tf.backgroundColor = [UIColor whiteColor];
    tf.placeholder = @"Norsk";
    tf.adjustsFontSizeToFitWidth = YES;
    tf.minimumFontSize = 12.;
    tf.font = [UIFont systemFontOfSize:26.];
    tf.delegate = self;
    self.nameField = tf;
    
    tf = [[UITextField alloc] initWithFrame:CGRectMake(0., 0., 320., 40.)];
    [v addSubview:tf];
    tf.borderStyle = UITextBorderStyleBezel;
    tf.backgroundColor = [UIColor whiteColor];
    tf.placeholder = @"English";
    tf.adjustsFontSizeToFitWidth = YES;
    tf.minimumFontSize = 12.;
    tf.font = [UIFont systemFontOfSize:26.];
    tf.delegate = self;
    self.englishNameField = tf;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //self.view.alpha = 0.;
   
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    OPLogMethod;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    OPLogMethod;
    /*
    if (self.navigationController) {
        CGSize S = self.contentSizeForViewInPopover;
        CGRect myBounds = CGRectMake(0., 0., S.width, S.height);
        
        myBounds.size.height += self.navigationController.navigationBar.bounds.size.height;
        
        if (animated) {
            [UIView animateWithDuration:0.5
                             animations:^{
                                 self.navigationController.view.bounds = myBounds; 
                                 self.view.alpha = 1.;
                             }];
        }
    }//*/
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    OPLogMethod;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    OPLogMethod;
    /*
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.view.alpha = 0.; 
                     }];//*/
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    OPLogMethod;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return YES;
}

#pragma mark - Table view data source

- (CGFloat)randomFloatSeededWithString:(NSString*)string {
    NSData *wd = [[string dataUsingEncoding:NSUTF8StringEncoding] SHA1];
    
    long *d = (long*)[wd bytes]; // pointer to a long
    
    long seedling = *(d);
    
    d+= 1;
    seedling += *(d);
    
    srandom(seedling);
    
    return RANDOM_FLOAT();
}

- (UIImage*)square:(CGSize)size color:(UIColor*)color {
 
    UIImage *img = nil;
    
    UIGraphicsBeginImageContext(size);
    ///CGContextRef context = UIGraphicsGetCurrentContext();
    //CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0., 0., size.width, size.height));
    [color set];
    UIRectFill(CGRectMake(0., 0., size.width, size.height));
    img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)atableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)atableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    // Return the number of rows in the section.
    if (self.topic) {
        count = [self.topic.subTopics count];
    } else {
        count = [[LanguageSource sharedInstance].packs count];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [atableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    if (self.topic) {
        LanguageWordPack *aPack = [self.topic.subTopics objectAtIndex:indexPath.row];
        cell.textLabel.text = aPack.title;
        //cell.detailTextLabel.text = aPack.englishTitle;
        
        if (self.cellImage) {
            cell.imageView.image = self.cellImage;
        }
    } else {
        LanguageTopic *aTopic = [[LanguageSource sharedInstance].packs objectAtIndex:indexPath.row];
        cell.textLabel.text = aTopic.title;
        cell.detailTextLabel.text = aTopic.englishTitle;
        
        UIColor *color = [UIColor colorWithHue:[self randomFloatSeededWithString:aTopic.title]
                                    saturation:1.
                                    brightness:0.8
                                         alpha:1.];
        
        cell.imageView.image = [self square:CGSizeMake(cell.bounds.size.height, cell.bounds.size.height)
                                      color:color];
    }

    return cell;
}

#pragma mark - Table Editing
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)atableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}*/



// Override to support editing the table view.
- (void)tableView:(UITableView *)atableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        OPLog(@"commit delete");
        if (self.topic) {
            
            [self.topic.subTopics removeObjectAtIndex:indexPath.row];
            
        } else {
            
            [[LanguageSource sharedInstance].packs removeObjectAtIndex:indexPath.row];
        }
        
        
        [atableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)atableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [atableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.topic) {
        LanguageWordPack *aPack = [self.topic.subTopics objectAtIndex:indexPath.row];
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:aPack forKey:@"wordPack"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"packPickerNotification"
                                                            object:self
                                                          userInfo:userInfo];
    } else {
        LanguageTopic *aTopic = [[LanguageSource sharedInstance].packs objectAtIndex:indexPath.row];
        
        PackPickerTableController *vc = [[PackPickerTableController alloc] initWithNibName:nil bundle:nil];
        vc.topic = aTopic;
        vc.title = aTopic.englishTitle;
        
        UITableViewCell *cell = [atableView cellForRowAtIndexPath:indexPath];
        UIImage *image = cell.imageView.image;
        
        vc.cellImage = image;
        
        [self.navigationController pushViewController:vc
                                             animated:YES];
    }
    
}

@end
