//
//  UIColor-Extras.m
//  Petals
//
//  Created by Panafold on 8/28/08.
//  Copyright 2008 Orange Petal LLC. All rights reserved.
//

#import "UIColor-Extras.h"
#import "mach/mach_time.h"

#ifndef RANDOM_FLOAT//()
#define RANDOM_SEED() srandom((unsigned)(mach_absolute_time() & 0xFFFFFFFF))
#define RANDOM_FLOAT() ((float)random() / (float)INT32_MAX)
#endif

@implementation UIColor(Extras)

+ (UIColor*)oceanColor {
	return [UIColor colorWithRed:0. green:64./255. blue:128./255. alpha:1.];
}

+ (UIColor*)infoGreen {
	return [UIColor colorWithRed:0. green:163./255. blue:60./255. alpha:0.92];
}

+ (UIColor*)alertRed {
	return [UIColor colorWithRed:223./255. green:48./255. blue:0. alpha:0.92];
}

+ (UIColor*)warningYellow {
	return [UIColor colorWithRed:233./255. green:196./255. blue:24./255. alpha:0.92];
}

+ (UIColor*)veryLightBlue {
	return [UIColor colorWithRed:230./255. green:240./255. blue:250./255. alpha:1.];
}

+ (UIColor*)messageOrange {
	return [UIColor colorWithRed:211./255. green:120./255. blue:0./255. alpha:1.];
}

+ (UIColor*)grayBlue {
	return [UIColor colorWithRed:105./255. green:117./255. blue:129./255. alpha:1.];
}

+ (UIColor*)randomColor {
	RANDOM_SEED();
	return [UIColor colorWithRed:RANDOM_FLOAT() green:RANDOM_FLOAT() blue:RANDOM_FLOAT() alpha:1.0];
}

+ (UIColor*)randomColorWithAlpha:(CGFloat)alpha {
	RANDOM_SEED();
	return [UIColor colorWithRed:RANDOM_FLOAT() green:RANDOM_FLOAT() blue:RANDOM_FLOAT() alpha:alpha];
}

+ (UIColor*)randomHueColorWithSaturation:(CGFloat)saturation brightness:(CGFloat)brightness {
    return [UIColor colorWithHue:RANDOM_FLOAT() saturation:saturation brightness:brightness alpha:1.0];
}

+ (UIColor*)lightenColor:(UIColor*)color {
#define kLightenColorPercentage 0.3
	
	CGColorRef cgColor = [color CGColor];
	// we're assuming RGBA color space!
	const CGFloat *colorComponents = CGColorGetComponents(cgColor);
	CGFloat r, g, b, a;

	r = colorComponents[0];
	g = colorComponents[1];
	b = colorComponents[2];
	a = colorComponents[3];
	
	CGFloat minComponent, maxComponent, h, s, v;
	
	minComponent = fmin(fmin(r,g),b);
	maxComponent = fmax(fmax(r,g),b);
	
	// hue
	if (maxComponent == minComponent) {
		h = 0.;
	} else if (maxComponent == r) {
		h = fmod(60.*( (g-b) / (maxComponent - minComponent)),360.);
	} else if (maxComponent == g) {
		h = 60.*( (b-r) / (maxComponent - minComponent) ) + 120.;
	} else if (maxComponent == b) {
		h = 60.*( (r-g) / (maxComponent - minComponent) ) + 240.;
	} else {
        h = 0.;
    }
	// normalize hue
	h = h / 360.;
	
	// saturation
	if (maxComponent == 0.) {
		s = 0.;
	} else {
		s = 1. - (minComponent/maxComponent);
	}
	
	// brightness
	v = maxComponent;
	
	return [UIColor colorWithHue:h saturation:s*kLightenColorPercentage brightness:v*(1/kLightenColorPercentage) alpha:a];
}

@end
