//
//  CocoaBox.h
//  PanaFlow
//
//  Created by Panafold on 9/1/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CocoaBoxEnumerateBodiesBlock)(id context, CGPoint p, CGPoint v, CGFloat angle);

extern NSString *const CocoaBoxBodyTypeKey;
extern NSString *const CocoaBoxBodyTypeStatic;
extern NSString *const CocoaBoxBodyTypeDynamic;

extern NSString *const CocoaBoxBodyPositionKey;   // NSValue holding CGPoint
extern NSString *const CocoaBoxBodyAngleKey;      // NSNumber
extern NSString *const CocoaBoxBodyVelocityKey;   // NSValue holding CGPoint

extern NSString *const CocoaBoxBodySubShapesKey;  // NSArray of NSDictionary

extern NSString *const CocoaBoxShapePositionKey;   // NSValue holding CGPoint
extern NSString *const CocoaBoxShapeTypeKey;       // see below
extern NSString *const CocoaBoxShapeDensityKey;    // NSNumber
extern NSString *const CocoaBoxShapeFrictionKey;   // NSNumber
extern NSString *const CocoaBoxShapeRestitutionKey;   // NSNumber

// shapes
// - circle
extern NSString *const CocoaBoxShapeTypeCircle;
extern NSString *const CocoaBoxShapeCircleRadius; // NSNumber

// - box
extern NSString *const CocoaBoxShapeTypeBox;
extern NSString *const CocoaBoxShapeBoxSize;  // NSValue holding CGSize

#ifdef __cplusplus
class b2World;
class b2Body;
#endif

@interface CocoaBox : NSObject {
@private
    UIAccelerationValue	g[3];
    CFMutableDictionaryRef mouseJointMap;
#ifdef __cplusplus
    b2World *world;
    b2Body *walls[4];
#endif
}

@property (nonatomic) CGFloat pixelScale; // pixels per meter, defaults to 40px = 0.1m

- (void)step;
- (void)buildWallsForScreenSize:(CGSize)size;

- (void)addBodyWithDefinition:(NSDictionary*)def context:(id)context;
- (void)enumerateBodiesWithBlock:(CocoaBoxEnumerateBodiesBlock)blk;

// Mouse joint
- (BOOL)addMouseJointAtPoint:(CGPoint)point withContext:(id)context;
- (void)updateMouseJointToPoint:(CGPoint)point withContext:(id)context;
- (void)removeMouseJointWithContext:(id)context;


@end
