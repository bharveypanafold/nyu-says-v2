//
//  AttractorView.h
//  PanaWord
//
//  Created by Panafold on 10/21/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LanguageWord;
@class BoxBody;
@class CAShapeLayer;
@class FilingView;

enum AttractorViewStates {
    kAttractorViewStateClosed = 1,
    kAttractorViewStateOpen,
    kAttractorViewStateWaitingOnNetwork
};

@interface AttractorView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic) CGFloat radius;

@property (weak,nonatomic) UITapGestureRecognizer *tapRecognizer;
@property (weak,nonatomic) UIPanGestureRecognizer *panRecognizer;
@property (weak,nonatomic) UILongPressGestureRecognizer *longPressRecognizer;
@property (weak,nonatomic) UIPinchGestureRecognizer *pinchRecognizer;

@property (strong,nonatomic) NSMutableArray *filingViews;

@property (weak,nonatomic) LanguageWord *word;
@property (weak,nonatomic) BoxBody *body;

@property (nonatomic) BOOL touched;
@property (strong,nonatomic) UILabel *label;
@property (strong,nonatomic) UILabel *defLabel;
@property (strong,nonatomic) UIView *rotatingView;
@property (strong,nonatomic) CAShapeLayer *shapeLayer;
@property (strong,nonatomic) UIButton *deleteButton;

@property (strong,nonatomic) NSString* type;

@property (strong,nonatomic) FilingView *glossFiling;

@property (nonatomic) BOOL fluffiesRunning;

@property (nonatomic) NSInteger state;
@property (nonatomic) Boolean isMorpheme;

@property (strong,nonatomic) NSMutableArray* morphemeComponents;

@property (strong,nonatomic) UIImageView* imgView;

@property (nonatomic) BOOL isCompleted;

- (void)startFluffies;
- (void)stopFluffies;
- (void)makeMorpheme;
- (void)setImage;
- (void)setSizeWithWidth:(int)width andHeight:(int)height;
- (void)setColorLayer;
- (void)setSizeWithWidth:(int)width andHeight:(int)height;
- (void)fadeIn;

@end
