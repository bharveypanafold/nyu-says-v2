//
//  WordpackDatabase.h
//  EnglishDiscovery
//
//  Created by Guillaume on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WordDescription.h"
#import "LanguageWordPack.h"

//This DB stores all word definitions for a loaded dictionary

@interface WordpackDatabase : NSObject
@property (strong, nonatomic) NSMutableArray* words;
@property (strong, nonatomic) NSMutableArray* wordpacks; //an array of arrays, each one being an array of WordDescription*
@property (strong, nonatomic) NSMutableArray* wordpackTitles; //an array of NSString*, each one being the title of a wordpack

- (BOOL) wordInDatabase:(NSString*)word;
- (void) addWord:(WordDescription*)l;
- (void) addWordPack:(LanguageWordPack*)w;
- (WordDescription*) getWord:(NSString*)word;
- (NSArray*) getWordsWithAffix:(NSString*)affix;
- (NSString*) getRewriteRuleResultFor:(NSString*)a and:(NSString*)b;
- (LanguageWordPack*) getWordPackForWord:(NSString*)word;
- (int) numberOfWordsInWordPack:(NSString*)wordpack;
- (bool) isFullWord:(NSString*)w;

@end