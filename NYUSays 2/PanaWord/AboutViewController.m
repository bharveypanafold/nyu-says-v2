//
//  AboutViewController.m
//  PanaWord
//
//  Created by Panafold on Summer 2013.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "AboutViewController.h"
#import <Twitter/Twitter.h>


@implementation AboutViewController

@synthesize scrollView;
@synthesize pageControl;
@synthesize animatingOffset;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"About";
        self.contentSizeForViewInPopover = CGSizeMake(500., 500.);
    }
    return self;
}

- (void)dismiss {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Button Actions

//- (IBAction)twitterButtonAction:(id)sender {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/#!/Panafold"]];
//}

// - (IBAction)fullVersionAction:(id)sender {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/power-words-advanced-english/id541302696?mt=8"]];
//}

 - (IBAction)supportButtonAction:(id)sender {
    //Create the tweet sheet
    TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
    //Add a tweet message
    [tweetSheet setInitialText:@"@Panafold: "];
    
    //Set a blocking handler for the tweet sheet
    tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
       [self dismissModalViewControllerAnimated:YES];
    };
    
    [self.parent.popoverController dismissPopoverAnimated:YES];
    //Show the tweet sheet!
    [[UIApplication sharedApplication] setStatusBarOrientation:[[UIApplication sharedApplication] statusBarOrientation] animated:NO];
   [self presentModalViewController:tweetSheet animated:YES];    
}

#pragma mark — Scrollview delegates

- (void)scrollViewDidScroll:(UIScrollView *)ascrollView {
    NSLog(@"scrollviewdidscroll");
    if (!animatingOffset) {        
        NSInteger currentPage = (ascrollView.contentOffset.x+ascrollView.bounds.size.width/2.) / ascrollView.bounds.size.width;
        self.pageControl.currentPage = currentPage;
        
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    self.animatingOffset = NO;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    self.animatingOffset = NO;
    
}

- (IBAction)pageControlAction:(id)sender {
    CGFloat newOffset = self.pageControl.currentPage*500.;
    self.animatingOffset = YES;
    [self.scrollView setContentOffset:CGPointMake(newOffset, 0.) animated:YES];
}

#pragma mark - Actions

- (IBAction)panafoldWebsite:(id)sender {
    [self openLinkInSafari:@"http://panafold.com/pow"];
}

- (IBAction)facebookWebsite:(id)sender {
    [self openLinkInSafari:@"http://facebook.com/Panafold"];
}

- (IBAction)composeEmail:(id)sender {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setToRecipients:[NSArray arrayWithObject:@"jmunger@pausd.org"]];
    [picker setSubject:@"Feedback"];
    
        
    [self presentModalViewController:picker 
                            animated:YES];
    
}

- (void)openLinkInSafari:(NSString*) url{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message 
// field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller 
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError*)error {    
	
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[self.navigationController setNavigationBarHidden:NO
    //                                         animated:animated];
    self.scrollView.contentSize = CGSizeMake(1000., 500.);
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
