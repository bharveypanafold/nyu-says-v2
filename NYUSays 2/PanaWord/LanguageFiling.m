//
//  LanguageFiling.m
//  PanaWord
//
//  Created by Panafold on 10/10/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "LanguageFiling.h"

@implementation LanguageFiling

@synthesize word,content;
@synthesize read;
@synthesize isDef;


+ (LanguageFiling*)filingFromDictionary:(NSDictionary*)dict {
    LanguageFiling *fil = [[self alloc] init];
   
    fil.content = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    return fil;
}

- (id)initWithCoder:(NSCoder *)coder {
    
    self = [super init];
    if (self) {
        self.content = [coder decodeObjectForKey:@"content"];
        self.read = [coder decodeBoolForKey:@"read"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.content
                 forKey:@"content"];
    
    [coder encodeBool:self.read
               forKey:@"read"];
    
}

@end
