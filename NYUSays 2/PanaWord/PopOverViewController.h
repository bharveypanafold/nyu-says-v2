//
//  PopOverViewController.h
//  CaliforniaEnglish
//
//  Created by Guillaume on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PFViewController;

@interface PopOverViewController : UIViewController

@property (strong, nonatomic) PFViewController* parent;

@end
