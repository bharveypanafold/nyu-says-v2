//
//  PoemWordsDatabase.h
//  PowerOfWords
//
//  Created by Guillaume on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PoemWordsDatabase : NSObject

@property (strong, nonatomic) NSMutableArray* poemwords;
@property (strong, nonatomic) NSMutableArray* highfreqpoemwords;
@property (strong, nonatomic) NSMutableArray* lowfreqpoemwords;

- (void) addWord:(NSString*)w forFrequency:(NSString*)freq;
- (NSMutableArray*) getRandomPoemWordsNumLowFreq:(int)low andNumHighFreq:(int)high;

@end
