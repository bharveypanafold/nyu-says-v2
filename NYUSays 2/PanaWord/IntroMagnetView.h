//
//  IntroMagnetView.h
//  PowerOfWords
//
//  Created by Guillaume on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AttractorView.h"

#define LEFT_SIDE 1
#define RIGHT_SIDE 2

@interface IntroMagnetView : AttractorView

- (id)initWithFrame:(CGRect)frame andSide:(int)side;

@end
