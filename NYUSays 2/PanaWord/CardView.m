//
//  CardView.m
//  PanaWord
//
//  Created by Panafold on 10/11/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "CardView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor-Extras.h"
#import <MessageUI/MessageUI.h>
#import "LanguageFiling.h"
#import "LanguageSource.h"
#import "NSData+HMAC.h"


@interface CardView() <UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,UITextFieldDelegate>


@property (strong,nonatomic) UIView *wordView;
@property (strong,nonatomic) UIView *noteView;
@property (strong,nonatomic) UIView *definitionView;
@property (strong,nonatomic) UIView *glossView;
@property (strong,nonatomic) UIView *emptyFilingsView;

@property (strong,nonatomic) UITextField *textField;

- (UIView*)placardWithFrame:(CGRect)frame;
- (CGFloat)randomFloatSeededWithString:(NSString*)string;
- (NSString*)subString:(NSString*)string centeredAround:(NSString*)center;
@end


@implementation CardView

@synthesize word;
@synthesize filingViews,wordView,noteView,definitionView,glossView;
@synthesize emptyFilingsView;
@synthesize textField;
@synthesize viewController;

#define kCardButtonSize 120.

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //OPLog(@"initWithFrame: %@",NSStringFromCGRect(frame));
        self.autoresizesSubviews = YES;
        
        self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        
        self.filingViews = [NSMutableArray arrayWithCapacity:1];
        
        self.wordView = [self placardWithFrame:CGRectMake(10.,
                                                          10.,
                                                          self.bounds.size.width-20.,
                                                          200.)];
        self.wordView.backgroundColor = [UIColor randomHueColorWithSaturation:0.8 brightness:.7];
        [self addSubview:self.wordView];
        
        
        self.noteView = [self placardWithFrame:CGRectMake(10.,
                                                          CGRectGetMaxY(self.wordView.frame),
                                                          self.bounds.size.width-20.,
                                                          100.)];
        self.noteView.backgroundColor = [UIColor randomHueColorWithSaturation:0.8 brightness:.7];
        [self addSubview:self.noteView];
        [self.noteView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(longPressToEdit:)]];
        
        
        self.definitionView = [self placardWithFrame:CGRectMake(10.,
                                                                CGRectGetMaxY(self.bounds) - 210.,
                                                                self.bounds.size.width-20.,
                                                                100.)];
        self.definitionView.backgroundColor = [UIColor randomHueColorWithSaturation:0.8 brightness:.7];
        [self addSubview:self.definitionView];
        [self.definitionView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                action:@selector(longPressToEdit:)]];
        
        self.glossView = [self placardWithFrame:CGRectMake(10.,
                                                           CGRectGetMaxY(self.definitionView.frame),
                                                           self.bounds.size.width-20.,
                                                           100.)];
        self.glossView.backgroundColor = [UIColor randomHueColorWithSaturation:0.8 brightness:.7];
        [self addSubview:self.glossView];
        [self.glossView addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(longPressToEdit:)]];
        
        UILabel *l;
        
        // WORD
        l = [self.wordView.layer valueForKey:@"front"];
        l.font = [UIFont boldSystemFontOfSize:72.];
        
        // NOTE
        l = [self.noteView.layer valueForKey:@"front"];
        l.text = @"Note";
        
        l = [self.noteView.layer valueForKey:@"back"];
        l.font = [UIFont systemFontOfSize:26.];
        
        // DEFINITION
        l = [self.definitionView.layer valueForKey:@"front"];
        l.text = @"Definition";

        l = [self.definitionView.layer valueForKey:@"back"];
        l.font = [UIFont boldSystemFontOfSize:36.];
        l.textAlignment = UITextAlignmentCenter;
        
        // GLOSS
        l = [self.glossView.layer valueForKey:@"front"];
        l.text = @"Gloss";
        
        l = [self.glossView.layer valueForKey:@"back"];
        l.font = [UIFont boldSystemFontOfSize:36.];
        l.textAlignment = UITextAlignmentCenter;
        
        // ----
        UIImageView *v = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nofilings"]];
        self.emptyFilingsView = v;
        //v.layer.borderColor = [UIColor redColor].CGColor;
        //v.layer.borderWidth = 4.;
        //v.contentMode = UIViewContentModeCenter;
        v.contentMode = UIViewContentModeScaleToFill;
        v.clipsToBounds = YES;
        
        UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(0., 0., frame.size.width, 100.)];
        tf.minimumFontSize = 10.;
        tf.font = [UIFont systemFontOfSize:24.];
        tf.textColor = [UIColor darkGrayColor];
        tf.backgroundColor = [UIColor whiteColor];
        tf.placeholder = @"(tap and hold to edit)";
        tf.delegate = self;
        self.textField = tf;
        
        //tf.layer.borderWidth = 2.;
        //tf.layer.borderColor = [UIColor yellowColor].CGColor;
        
    }
    return self;
}

- (NSString*)description {
    NSString *s = [super description];
    
    return [NSString stringWithFormat:@"%@ (%@)",s,self.word.term];
}

- (void)layoutSubviews {
    
    //OPLog(@"layoutSubviews: %@",NSStringFromCGRect(self.frame));
    
    UILabel *l = nil;

    // WORD
    self.wordView.frame = CGRectMake(10.,
                                     10.,
                                     self.bounds.size.width-20.,
                                     180.);
    CGFloat wordHue = [self randomFloatSeededWithString:self.word.term];
    l = [self.wordView.layer valueForKey:@"front"];
    l.text = self.word.term;
    [self.wordView addSubview:l];
    [[self.wordView.layer valueForKey:@"back"] removeFromSuperview];
    self.wordView.backgroundColor = [UIColor colorWithHue:wordHue
                                               saturation:0.6
                                               brightness:0.8
                                                    alpha:1.];

    
    
    // NOTE
    self.noteView.frame = CGRectMake(10.,
                                     CGRectGetMaxY(self.wordView.frame),
                                     self.bounds.size.width-20.,
                                     100.);
    CGRect labelRect = CGRectInset(self.noteView.bounds, 10., 10.);
    
    [[self.noteView.layer valueForKey:@"front"] setFrame:labelRect];
    [[self.noteView.layer valueForKey:@"back"] setFrame:labelRect];
    l = [self.noteView.layer valueForKey:@"back"];
    if (self.word.note) {
        l.text = self.word.note;
        l.textAlignment = UITextAlignmentLeft;
    } else {
        l.text = @"(tap and hold to edit)";
        l.textAlignment = UITextAlignmentCenter;
    }
    [self.noteView addSubview:[self.noteView.layer valueForKey:@"front"]];
    [l removeFromSuperview];
    self.noteView.backgroundColor = [UIColor colorWithHue:RANDOM_FLOAT()
                                               saturation:0.6
                                               brightness:0.8
                                                    alpha:1.];
    
    // DEFINITION
    self.definitionView.frame = CGRectMake(10.,
                                           CGRectGetMaxY(self.noteView.frame),
                                           self.bounds.size.width-20.,
                                           100.);
    [[self.definitionView.layer valueForKey:@"front"] setFrame:labelRect];
    [[self.definitionView.layer valueForKey:@"back"] setFrame:labelRect];
    l = [self.definitionView.layer valueForKey:@"back"];
    l.text = self.word.definition;
    [self.definitionView addSubview:[self.definitionView.layer valueForKey:@"front"]];
    [l removeFromSuperview];
    self.definitionView.backgroundColor = [UIColor colorWithHue:RANDOM_FLOAT()
                                                     saturation:0.6
                                                     brightness:0.8
                                                          alpha:1.];

    
    // GLOSS
    self.glossView.frame = CGRectMake(10.,
                                      CGRectGetMaxY(self.definitionView.frame),
                                      self.bounds.size.width-20.,
                                      100.);
    [[self.glossView.layer valueForKey:@"front"] setFrame:labelRect];
    [[self.glossView.layer valueForKey:@"back"] setFrame:labelRect];
    
    l = [self.glossView.layer valueForKey:@"back"];
    NSString *gloss = self.word.gloss;
    l.text = gloss;
    [self.glossView addSubview:[self.glossView.layer valueForKey:@"front"]];
    [l removeFromSuperview];
    self.glossView.backgroundColor = [UIColor colorWithHue:RANDOM_FLOAT()
                                                saturation:0.6
                                                brightness:0.8
                                                     alpha:1.];

    
    // FILINGS
    NSInteger filingsCount = 3; // [self.word.filings count];
    
    CGFloat height = ( CGRectGetMaxY(self.frame)-CGRectGetMaxY(self.glossView.frame) )/filingsCount;
    
    if ([self.word.filings count] == 0) {
        [self addSubview:self.emptyFilingsView];
        self.emptyFilingsView.frame = CGRectMake(10.,
                                                 CGRectGetMaxY(self.glossView.frame),
                                                 self.bounds.size.width-20.,
                                                 CGRectGetMaxY(self.frame)-CGRectGetMaxY(self.glossView.frame));
    } else {
        [self.emptyFilingsView removeFromSuperview];
        
        for (UIView *aView in self.filingViews) {
            [aView removeFromSuperview];
        }
        [self.filingViews removeAllObjects];
        
        for (NSInteger j = 0; j < filingsCount; j++) {
            
            UIView *v = [self placardWithFrame:CGRectMake(self.wordView.frame.origin.x,
                                                          CGRectGetMaxY(self.glossView.frame)+j*height,
                                                          self.wordView.bounds.size.width,
                                                          height)];
            [self addSubview:v];
            [self.filingViews addObject:v];
            v.backgroundColor = [UIColor colorWithHue:RANDOM_FLOAT()
                                           saturation:0.3
                                           brightness:0.8
                                                alpha:1.];
            
            UILabel *front = [v.layer valueForKey:@"front"];
            front.text = @"Filing";            
        }
        
        // Fill in data
        for (NSInteger j = 0; j < filingsCount; j++ ) {
            UIView *container = [self.filingViews objectAtIndex:j];
            LanguageFiling *filing = [self.word.filings objectAtIndex:j];
            
            UILabel *back = [container.layer valueForKey:@"back"];
            UILabel *front = [container.layer valueForKey:@"front"];
            
            NSString *content = [filing.content valueForKey:@"description"];
            
            back.text = [self subString:content centeredAround:self.word.term];
            
            //OPLog(@"----------");            
            //OPLog(@"content: %i",[content length]);
            //OPLog(@" ");
            //OPLog(@"substring: %i",[[self subString:content centeredAround:self.word.term] length]);
            
            back.font = [UIFont systemFontOfSize:height/5.];
            [container addSubview:front];
            [back removeFromSuperview];
        }
    }
}

- (void)layoutData {

}

#pragma mark - Private

#define kLeadingStringLength 30
#define kTrailingStringLength 200

- (NSString*)subString:(NSString*)string centeredAround:(NSString*)center {
    
    NSString *str = string;
    
    NSRange foundRange = [string rangeOfString:center
                                       options:NSCaseInsensitiveSearch
                                         range:NSMakeRange(0, [string length])
                                        locale:nil];
    
    if (NSNotFound != foundRange.location) {
        
        //OPLog(@"substring found");
        NSUInteger start = 0;
        NSUInteger stop = kLeadingStringLength + kTrailingStringLength;
        
        if (foundRange.location > kLeadingStringLength) {
            start = foundRange.location - kLeadingStringLength;
        }
        
        if (start+stop > [string length]-1) {
            stop = ([string length]-1) - start;
        }
        
        //OPLog(@"%i [%i %i] %i",0,start,stop,[string length]);
        
        str = [string substringWithRange:NSMakeRange(start, stop)];
    } else {
        //OPLog(@"substring NOT found");
    }
    
    return str;
}

- (CGFloat)randomFloatSeededWithString:(NSString*)string {
    
    NSAssert(string,@"no string provided. a problem.");
    
    NSData *wd = [[string dataUsingEncoding:NSUTF8StringEncoding] SHA1];
    
    long *d = (long*)[wd bytes]; // pointer to a long
    
    long seedling = *(d);
    
    d+= 1;
    seedling += *(d);
    
    srandom(seedling);
    
    return RANDOM_FLOAT();
}

- (UIView*)placardWithFrame:(CGRect)frame {
    
    UIView *v = [[UIView alloc] initWithFrame:frame];
    UILabel *s1, *s2;
    
    // FRONT
    CGRect R = v.bounds; //CGRectInset(v.bounds, 10., 10.);
    s1 = [[UILabel alloc] initWithFrame:R];
    s1.font = [UIFont boldSystemFontOfSize:42.];
    s1.textAlignment = UITextAlignmentCenter;
    s1.textColor = [UIColor whiteColor];
    s1.backgroundColor = [UIColor clearColor];
    s1.numberOfLines = 1;
    s1.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    s1.minimumFontSize = 12.;
    s1.adjustsFontSizeToFitWidth = YES;
    
    // BACK
    s2 = [[UILabel alloc] initWithFrame:R];
    s2.font = [UIFont boldSystemFontOfSize:38.];
    s2.textAlignment = UITextAlignmentLeft;
    s2.textColor = [UIColor whiteColor];
    s2.backgroundColor = [UIColor clearColor];
    s2.numberOfLines = 0;
    s2.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    s2.minimumFontSize = 12.;
    s2.adjustsFontSizeToFitWidth = YES;
    
    //s2.layer.borderWidth = 2.;
    //s2.layer.borderColor = [UIColor yellowColor].CGColor;
    
    [v.layer setValue:s1
               forKey:@"front"];
    [v.layer setValue:s2
               forKey:@"back"];
    
    // start with the front 
    [v addSubview:s1];
    
    [v addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                    action:@selector(tapToFlip:)]];
    
    return v;
}

#pragma mark - TextField

- (void)showTextfieldForView:(UIView*)view {
        
    UIBarButtonItem *barButton = nil;
    
    if (self.viewController) {
        
        barButton = self.viewController.navigationItem.leftBarButtonItem;
        
        if (barButton) {
            [self.layer setValue:barButton forKey:@"leftBarButtonItem"];
        }
        
        barButton = self.viewController.navigationItem.rightBarButtonItem;
        
        if (barButton) {
            [self.layer setValue:barButton forKey:@"rightBarButtonItem"];
        }
        
        barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                  target:self
                                                                  action:@selector(dismissTextfield)];
        [self.viewController.navigationItem setRightBarButtonItem:barButton
                                                         animated:YES];
        
        barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                  target:self
                                                                  action:@selector(saveTextfield)];
        
        [self.viewController.navigationItem setLeftBarButtonItem:barButton
                                                        animated:YES];
    }
    
    UIView *front = [view.layer valueForKey:@"front"];
    UIView *back = [view.layer valueForKey:@"back"];
    
    UIViewAnimationTransition tr = UIViewAnimationOptionTransitionNone;
    
    if (front.superview)
        tr = UIViewAnimationOptionTransitionFlipFromTop;
    
    [UIView transitionWithView:view
                      duration:0.4
                       options:tr
                    animations:^{
                        
                        if (front.superview) {
                            // front is showing, flip to back
                            [front removeFromSuperview];
                            [view addSubview:back];
                        }
                        
                    }
                    completion:^(BOOL finished) {
                        
                        self.textField.alpha = 0.;
                        self.textField.frame = back.bounds;
                        
                        NSString *stringValue = nil;
                        
                        if (view == self.noteView) {
                            stringValue = self.word.note;
                        } else if (view == self.definitionView) {
                            stringValue = self.word.definition;
                        } else if (view == self.glossView) {
                            stringValue = self.word.gloss;
                        }
                        
                        self.textField.text = stringValue;

                        [back addSubview:self.textField];
                        
                        [UIView animateWithDuration:0.2
                                              delay:0.
                                            options:0
                                         animations:^{
                                             
                                             self.textField.alpha = 1.;
                                             
                                         } completion:^(BOOL finished) {
                                             
                                             [self.textField becomeFirstResponder];
                                         }];
                    }];     

}

- (void)dismissTextfield {
    
    if (self.viewController) {
        
        UIBarButtonItem *barButton = nil;
        
        barButton = [self.layer valueForKey:@"leftBarButtonItem"];
        
        if (barButton) {
            [self.viewController.navigationItem setLeftBarButtonItem:barButton
                                                            animated:YES];
        } else {
            [self.viewController.navigationItem setLeftBarButtonItem:nil
                                                            animated:YES];
        }
        
        barButton = [self.layer valueForKey:@"rightBarButtonItem"];
        
        if (barButton) {
            [self.viewController.navigationItem setRightBarButtonItem:barButton
                                                             animated:YES];
        } else {
            [self.viewController.navigationItem setRightBarButtonItem:nil
                                                             animated:YES];
        }
    }
    
    [self.textField resignFirstResponder];
    [UIView animateWithDuration:0.3
                          delay:0.
                        options:0
                     animations:^{
                         
                         self.textField.alpha = 0.;
                         
                     } completion:^(BOOL finished) {
                         
                         [self.textField removeFromSuperview];
                         
                     }];
}

- (void)saveTextfield {
    NSString *str = self.textField.text;
    
    UIView *view = self.textField.superview.superview; // this is clearly not elegant. :/
    
    UILabel *l = [view.layer valueForKey:@"back"];
    
    if (0 < [str length]) {
        
        if (view == self.noteView) {
            self.word.note = str;
        } else if (view == self.definitionView) {
            self.word.definition = str;
        } else if (view == self.glossView) {
            self.word.gloss = str;
        }
        
        l.text = str;
        l.textAlignment = UITextAlignmentLeft;
    } else {
        
        if (view == self.noteView) {
            self.word.note = nil;
        } else if (view == self.definitionView) {
            self.word.definition = nil;
        } else if (view == self.glossView) {
            self.word.gloss = nil;
        }
        l.text = nil;
        l.textAlignment = UITextAlignmentCenter;
    }
    [self.textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)atextField {
    
    //OPLogMethod;
    
    [self saveTextfield];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self dismissTextfield];
    //OPLogMethod;
    
    return YES;
}

#pragma mark - Gestures

- (void)tapToFlip:(UITapGestureRecognizer*)recognizer {
    
    if (UIGestureRecognizerStateBegan == recognizer.state) {
        
    } else if (UIGestureRecognizerStateChanged == recognizer.state) {
        
    } else if (UIGestureRecognizerStateEnded == recognizer.state) {
        UIView *v = recognizer.view;
        
        if (v != self.wordView) {
            UIView *front = [v.layer valueForKey:@"front"];
            UIView *back = [v.layer valueForKey:@"back"];
            
            //front.frame = v.bounds;
            //back.frame = v.bounds;
            
            [UIView transitionWithView:v
                              duration:0.4
                               options:UIViewAnimationOptionTransitionFlipFromTop
                            animations:^{
                                
                                if (front.superview) {
                                    // front is showing, flip to back
                                    [front removeFromSuperview];
                                    [v addSubview:back];
                                } else {
                                    [back removeFromSuperview];
                                    [v addSubview:front];
                                }
                                
                            }
                            completion:^(BOOL finished) {
                                
                            }];     

        }
                
    } else if (UIGestureRecognizerStateFailed == recognizer.state) {
        
    }
}

- (void)longPressToEdit:(UILongPressGestureRecognizer*)recognizer {
        
    if (UIGestureRecognizerStateBegan == recognizer.state) {
        
        [self showTextfieldForView:recognizer.view];
        
    }
}

@end
