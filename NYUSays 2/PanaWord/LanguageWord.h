//
//  LanguageWord.h
//  PanaWord
//
//  Created by Panafold on 10/10/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LanguageWordPack;
@class LanguageWord;

typedef void(^LanguageWordFilingCompletionBlock)(BOOL success, LanguageWord* word);

@interface LanguageWord : NSObject


@property (strong,nonatomic) NSString *term;
@property (strong,nonatomic) NSString *wordpackStr;
@property (strong,nonatomic) NSString *type;
@property (strong,nonatomic) NSString *def;
@property (strong,nonatomic) NSString *name;

@property (strong,nonatomic) NSMutableArray *filings;

@property (strong,nonatomic) NSString *note;
@property (strong,nonatomic) NSString *definition;
@property (strong,nonatomic) NSString *gloss;

@property (weak,nonatomic) LanguageWordPack *wordPack;


// State
@property (nonatomic,getter = hasMemorized) BOOL memorized;

+ (LanguageWord*)wordFromDictionary:(NSDictionary*)dict;

- (void)loadFilingsCompletion:(LanguageWordFilingCompletionBlock)completion;

@end
