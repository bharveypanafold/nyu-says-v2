//
//  WordDescription.h
//  EnglishDiscovery
//
//  Created by Guillaume on 6/4/12.
//  Copyright (c) 2012 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RewriteRule.h"

@interface WordDescription : NSObject

@property (strong,nonatomic) NSMutableArray* affixes; //describes the words affixes, in the order in which they assemble
@property (strong,nonatomic) NSMutableArray* rewriteRules; //all the rewrite rules for a given word
@property (strong,nonatomic) NSString* fullword;
//VV added
@property (strong,nonatomic) NSMutableArray* def;

- (void) addAffix:(NSString*)affix;
//VV added
- (void) addDef:(NSString*)def;

- (void) addRewriteRuleForAffixA:(NSString*)affixA andAffixB:(NSString*)affixB withResult:(NSString*)finalStr;
- (void) addRewriteRule:(RewriteRule*)r;
- (BOOL) existsRewriteRuleFor:(NSString*)affix1 and:(NSString*)affix2;
- (RewriteRule*) rewriteRuleFor:(NSString*)affix1 and:(NSString*)affix2;
- (BOOL) hasAffix:(NSString*)a;
- (BOOL) stringIsRewriteResult:(NSString*)str;
@end
