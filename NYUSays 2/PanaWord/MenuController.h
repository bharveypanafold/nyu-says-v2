//
//  MenuController.h
//  PanaWord
//
//  Created by Panafold on 11/22/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PFViewController.h"

@interface MenuController : UIViewController

@property (strong,nonatomic) IBOutlet UIImageView *imageView;

@property (strong,nonatomic) PFViewController* pfView;

- (IBAction)wordPackButtonAction:(id)sender;
- (IBAction)contextCardsButtonAction:(id)sender;
- (IBAction)aboutButtonAction:(id)sender;

@end
