//
//  BoxShape.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "BoxShape.h"
#import "Box2D.h"
#import "BoxWorld.h"

@implementation BoxShape

@synthesize radius;

- (id)initWithRadius:(CGFloat)theRadius {
    self = [super init];
    
    if (self) {
        self.radius = theRadius;
    }
    return self;
}

- (b2Shape*)createB2Shape {
    return NULL;
}

@end


@implementation CircleShape

@synthesize position;

- (id)initWithRadius:(CGFloat)theRadius position:(CGPoint)thePosition {
    self = [super initWithRadius:theRadius];
    
    if (self) {
        self.position = thePosition;
    }
    return self;
}

+ (CircleShape*)circleWithRadius:(CGFloat)theRadius Position:(CGPoint)thePosition {
    return [[self alloc] initWithRadius:theRadius position:thePosition];
}

// follows the CF memory convention - create* means you have to release what you get from the method
- (b2Shape*)createB2Shape {
    b2CircleShape *shape = new b2CircleShape;
    
    shape->m_radius = self.radius/kBoxWorldPixelScale;
    shape->m_p = b2Vec2(self.position.x/kBoxWorldPixelScale,
                        self.position.y/kBoxWorldPixelScale);
    
    return shape;
}

@end


@implementation PolygonShape

@synthesize centroid,verticies,normals;

/* setAsBox()
 m_vertexCount = 4;
 m_vertices[0].Set(-hx, -hy);
 m_vertices[1].Set( hx, -hy);
 m_vertices[2].Set( hx,  hy);
 m_vertices[3].Set(-hx,  hy);
 m_normals[0].Set(0.0f, -1.0f);
 m_normals[1].Set(1.0f, 0.0f);
 m_normals[2].Set(0.0f, 1.0f);
 m_normals[3].Set(-1.0f, 0.0f);
 m_centroid.SetZero(); */

+ (PolygonShape*)rectangleWithSize:(CGSize)size {
    PolygonShape *shape = [[self alloc] initWithRadius:0.0];
    
    CGFloat hx = (size.width)/2.0, hy = (size.height)/2.0;
    shape.verticies = [NSArray arrayWithObjects:
                       [NSValue valueWithCGPoint:CGPointMake(-hx, -hy)],
                       [NSValue valueWithCGPoint:CGPointMake(hx, -hy)],
                       [NSValue valueWithCGPoint:CGPointMake(hx, hy)],
                       [NSValue valueWithCGPoint:CGPointMake(-hx, hy)],
                       nil];
    
    shape.centroid = CGPointZero;
    
    return shape;
}

- (b2Shape*)createB2Shape {

    b2PolygonShape *poly = new b2PolygonShape;
    
    NSAssert(b2_maxPolygonVertices >= [self.verticies count],@"vertex count > b2_maxPolygonVertices");
    b2Vec2 verts[b2_maxPolygonVertices];

    for (NSInteger j = 0; j < [self.verticies count]; j++) {
        
        CGPoint p = [[self.verticies objectAtIndex:j] CGPointValue];
        verts[j].x = p.x/kBoxWorldPixelScale;
        verts[j].y = p.y/kBoxWorldPixelScale;
    }
    
    poly->Set(verts,[self.verticies count]);
    
    return poly;
}

@end

@implementation BoxFixture

@synthesize shape;
@synthesize friction;
@synthesize restitution;
@synthesize density;
@synthesize sensor;

- (id)initWithShape:(BoxShape*)theShape {
    self = [super init];
    
    if (self) {
        self.shape = theShape;
        self.sensor = NO;
    }
    
    return self;
}

- (void)dealloc {
    if (NULL != _b2Shape) {
        delete (b2Shape*)_b2Shape;
        _b2Shape = NULL;
    }
}

+ (BoxFixture*)fixtureWithShape:(BoxShape*)theShape {
    return [[self alloc] initWithShape:theShape];
}

- (b2FixtureDef*)createb2Definition {
    
    b2FixtureDef *def = new b2FixtureDef;
    
    def->friction = self.friction;
    def->restitution = self.restitution;
    def->density = self.density;
    def->isSensor = self.sensor;
    
    if (NULL != _b2Shape) {
        // toss any previous shape
        delete (b2Shape*)_b2Shape;
        _b2Shape = NULL;
    }
    _b2Shape = [self.shape createB2Shape];
    
    NSAssert(_b2Shape != NULL,@"shape is NULL");
    
    def->shape = (b2Shape*)_b2Shape;
    
    return def;
}

- (void)setFixturePtr:(b2Fixture*)fptr {
    _b2Fixture = fptr;
}

// this is a hack. but i need it now and don't have the time or money to do it correctly.
- (void)setShapeRadius:(CGFloat)radius {
    b2Fixture *fix = (b2Fixture*)_b2Fixture;
    b2Shape *boxshape = fix->GetShape();
    boxshape->m_radius = radius/kBoxWorldPixelScale;
}

@end