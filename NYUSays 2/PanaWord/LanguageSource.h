//
//  LanguageSource.h
//  PanaWord
//
//  Created by Panafold on Summer 2013.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>

// This is a SINGLETON that owns the data model tree.

@interface LanguageSource : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong,nonatomic) NSMutableArray *packs;

+ (LanguageSource*)sharedInstance;

- (void)saveContext;

+ (NSMutableArray*)topics;
+ (NSDictionary*)gloss;

+ (void)saveData;

@end
