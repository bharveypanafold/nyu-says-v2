//
//  PagedArrayScrollView.m
//  PanaWord
//
//  Created by Panafold on 12/12/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "PagedArrayScrollView.h"
#import <QuartzCore/QuartzCore.h>


@interface PagedArrayScrollView() {
    NSInteger _pageCount;
    
    NSInteger _lastLeft, _lastRight;
}

@property (strong,nonatomic) UIView *containerView;
@property (strong,nonatomic) NSMutableDictionary *visibleViewMap;

- (void)tileViewsFromMinX:(CGFloat)minimumVisibleX toMaxX:(CGFloat)maximumVisibleX;
@end


@implementation PagedArrayScrollView

@synthesize pageDelegate;
@synthesize visibleViewMap;
@synthesize containerView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.visibleViewMap = [[NSMutableDictionary alloc] initWithCapacity:1];
        
        RANDOM_SEED();
                
        self.directionalLockEnabled = YES;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.pagingEnabled = YES;
        
        _pageCount = 0;
        _lastLeft = -1;
        _lastRight = -1;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //OPLog(@"%@",NSStringFromCGRect(self.frame));

    
    if (!self.containerView) {
        
        NSUInteger count = [self.pageDelegate numberOfPages];
        CGRect frame = self.frame;
        _pageCount = count;
        
        OPLog(@"number of pages: %i",count);
        
        UIView *c = [[UIView alloc] initWithFrame:CGRectMake(0, 0, count*frame.size.width, frame.size.height-1.)];
        [self addSubview:c];
        //c.userInteractionEnabled = NO;
        c.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
        self.containerView = c;
        
        self.contentSize = CGSizeMake(count*frame.size.width, frame.size.height);
    }
    
    CGRect visibleBounds = [self convertRect:[self bounds] toView:self.containerView];
    CGFloat minimumVisibleX = CGRectGetMinX(visibleBounds);
    CGFloat maximumVisibleX = CGRectGetMaxX(visibleBounds);
    
    [self tileViewsFromMinX:minimumVisibleX 
                     toMaxX:maximumVisibleX];
}

- (UIView*)viewForIndex:(NSInteger)index {
    UIView *v = [self.visibleViewMap objectForKey:[NSIndexPath indexPathForRow:index inSection:0]];
    return v;
}

- (void)animateCardFromIndex:(NSInteger)from toIndex:(NSInteger)to {
    
    // if from==to, ignore (or shrink-bounce in place)
    
    // if from==to+1 or to-1
    
    // otherwise animate out the view and then shift the existing views
    
    
    [self.visibleViewMap removeObjectForKey:[NSIndexPath indexPathForRow:from inSection:0]];
    //[self.pageDelegate recycleViewAtIndex:from];
    
    UIView *v = [self.visibleViewMap objectForKey:[NSIndexPath indexPathForRow:from+1 inSection:0]];
    [self.visibleViewMap setObject:v
                            forKey:[NSIndexPath indexPathForRow:from inSection:0]];
    [self.visibleViewMap removeObjectForKey:[NSIndexPath indexPathForRow:from+1 inSection:0]];
    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.
                        options:0
                     animations:^{
                         
                         CGRect f = v.frame;
                         f.origin.x = from*self.bounds.size.width;
                         v.frame = f;
                         
                     } completion:^(BOOL finished) {
                         
                         //OPLog(@"%@",self.visibleViewMap);

                         
                         CGRect visibleBounds = [self convertRect:[self bounds] toView:self.containerView];
                         CGFloat minimumVisibleX = CGRectGetMinX(visibleBounds);
                         CGFloat maximumVisibleX = CGRectGetMaxX(visibleBounds);
                         
                         _lastLeft = 0;
                         _lastRight = 0;
                         
                         [self tileViewsFromMinX:minimumVisibleX 
                                          toMaxX:maximumVisibleX];
                         
                         //OPLog(@"%@",self.visibleViewMap);
                         
                     }];
}

- (void)tileViewsFromMinX:(CGFloat)minimumVisibleX toMaxX:(CGFloat)maximumVisibleX {

    CGFloat w = self.frame.size.width;
    NSInteger leftVisibleIndex = floorf(minimumVisibleX/w);
    NSInteger rightVisibleIndex = ceilf(maximumVisibleX/w);
    
    if (_pageCount <= 0)
        return;
    
    if (leftVisibleIndex < 0)
        leftVisibleIndex = 0;
    
    if (rightVisibleIndex >= _pageCount)
        rightVisibleIndex = _pageCount-1;
        
    
    if ((leftVisibleIndex != _lastLeft) || (rightVisibleIndex != _lastRight)) { // avoid unneeded work

        NSMutableSet *indiciesToRemove = [[NSMutableSet alloc] initWithCapacity:1];
        // remove any existing view that isn't in this range
        for (NSIndexPath *key in self.visibleViewMap) {
            
            NSInteger idx = key.row;
            
            if (idx < leftVisibleIndex || idx > rightVisibleIndex) {
                
                [self.pageDelegate recycleViewAtIndex:idx];
                UIView *view = [self.visibleViewMap objectForKey:key];
                [view removeFromSuperview];
                [indiciesToRemove addObject:key];
            }
        }
        [self.visibleViewMap removeObjectsForKeys:[indiciesToRemove allObjects]];
        
        
        for (NSUInteger j = leftVisibleIndex; j <= rightVisibleIndex; j++) {
            
            NSIndexPath *key = [NSIndexPath indexPathForRow:j inSection:0];
            UIView *view = [self.visibleViewMap objectForKey:key];
            
            if (!view) {
                
                view = [self.pageDelegate viewForIndex:j];
                
                [self.containerView addSubview:view];
                
                [self.visibleViewMap setObject:view
                                        forKey:key];
            }
            
            CGRect f = view.frame;
            
            f.origin.x = j*self.bounds.size.width;
            
            view.frame = f;
        }
        
        _lastLeft = leftVisibleIndex;
        _lastRight = rightVisibleIndex;
        
    }
}


@end
