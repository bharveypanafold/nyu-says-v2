//
//  BoxWorld.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "BoxWorld.h"
#import "Box2D.h"

@interface BoxBody(PrivateWorldMethods)
- (void)setBodyPtr:(b2Body*)bptr;
- (b2BodyDef*)b2Definition;
- (void)insertFixtures;
- (b2Body*)b2Ptr;
@end

@interface BoxJoint(PrivateWorldMethods)
- (void)setJointPtr:(b2Joint*)bptr;
- (b2Joint*)b2Ptr;
- (b2JointDef*)b2Definition;
@end

@interface BoxWorld() <UIAccelerometerDelegate>

@property (strong,nonatomic) NSMutableSet *bodies;
@property (strong,nonatomic) NSMutableSet *joints;

@end

@implementation BoxWorld

//@synthesize pixelScale;
@synthesize bodies;
@synthesize joints;

- (id)initWithGravity:(CGPoint)gravity {
    self = [super init];
    if (self) {
        g[0] = gravity.x;
        g[1] = gravity.y;
        
        bool doSleep = true;
        world = new b2World(b2Vec2(gravity.x, gravity.y), doSleep);
        
        //self.pixelScale = 42.;
        self.bodies = [[NSMutableSet alloc] initWithCapacity:1];
        self.joints = [[NSMutableSet alloc] initWithCapacity:1];
        
        accelerometerEnabled = NO;
    }
    return self;
}

+ (BoxWorld*)worldWithGravity:(CGPoint)gravity {
    return [[self alloc] initWithGravity:gravity];
}

- (void)dealloc {
    
    NSSet *set = [NSSet setWithSet:self.bodies];
    for (BoxBody *body in set) {
        [self removeBody:body];
    }
    
    [self setAccelerometerEnabled:NO];
}

- (void)setAccelerometerEnabled:(BOOL)enabled {
    if (accelerometerEnabled != enabled) {
        accelerometerEnabled = enabled;
        if (enabled) {
            OPLog(@"Enable Gravity");
            [[UIAccelerometer sharedAccelerometer] setUpdateInterval:1./20.];
            [[UIAccelerometer sharedAccelerometer] setDelegate:self];
        } else {
            OPLog(@"Disable Gravity");
            [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
        }
    }
}

- (BOOL)isAccelerometerEnabled {
    return accelerometerEnabled;
}

- (b2World*)worldPtr {
    return world;
}

- (void)step {
    world->SetGravity(b2Vec2(g[0], -g[1]));
    // fixed stepsize of 1/60 s
    // call this at 60hz for 'real time', or faster/slower to speed up or slow down 'time'
    world->Step(1.0/60.0, 8, 3);
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	CGFloat x = acceleration.x;
	CGFloat y = acceleration.y;
	CGFloat z = acceleration.z;
	
	g[0] = x;
	g[1] = y;
	g[2] = z;
}

- (void)addBody:(BoxBody*)newBody {
    
    [self.bodies addObject:newBody];
    
    b2BodyDef *def = [newBody b2Definition];
    
    b2Body *body = world->CreateBody(def);
    
    body->SetSleepingAllowed(false); // for some reason changes in gravity don't waken bodies. :(

    [newBody setBodyPtr:body];
    [newBody insertFixtures];
}

- (void)removeBody:(BoxBody *)body {
    
    b2Body *b = [body b2Ptr];
    if (NULL != b) {
    
        world->DestroyBody([body b2Ptr]); // b2DestructionListener is notified of shape and joints that die implicitly
        [body setBodyPtr:NULL];
        [self.bodies removeObject:body];
    }
}

- (void)enumerateBodies:(BoxBodyEnumerationBlock)block {
    
    for (BoxBody *b in self.bodies) {
        block(b);
    }
}

- (void)addJoint:(BoxJoint*)joint {
    
    [self.joints addObject:joint];
    
    b2JointDef *theDef = [joint b2Definition];
    b2Joint *theJoint = world->CreateJoint(theDef);
    
    [joint setJointPtr:theJoint];
}

- (void)removeJoint:(BoxJoint*)joint {
    
    b2Joint *theJoint = [joint b2Ptr];
    
    if (NULL != theJoint) {
        world->DestroyJoint(theJoint);
        [joint setJointPtr:NULL];
        
        [self.joints removeObject:joint];
    }
}


@end
