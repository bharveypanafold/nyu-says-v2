//
//  SnippetViewController.h
//  PanaWord
//
//  Created by Panafold on 9/22/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageFiling.h"

typedef void(^SnippetDismissBlock)(void);

@interface SnippetViewController : UIViewController

@property (strong,nonatomic) LanguageFiling *filing;
@property (copy,nonatomic) SnippetDismissBlock completion;
@property (strong,nonatomic) UIWebView *web;

@end
