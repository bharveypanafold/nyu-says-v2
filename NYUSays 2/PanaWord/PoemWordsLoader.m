//
//  PoemWordsLoader.m
//  PowerOfWords
//
//  Created by Guillaume on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PoemWordsLoader.h"

@implementation PoemWordsLoader

- (id) init{
    self=[super init];
    
    if (self){
        db= [[PoemWordsDatabase alloc] init];
        
        NSString* filePath = [[NSBundle mainBundle] pathForResource:@"PoemWordsLowerFreq" ofType:@"xml"];  
        NSData* data= [[NSData alloc] initWithContentsOfFile:filePath];
        parser1= [[NSXMLParser alloc] initWithData:data];
        [parser1 setDelegate:self];
        [parser1 parse];
        
        NSString* filePath2 = [[NSBundle mainBundle] pathForResource:@"PoemWordsHiFreq" ofType:@"xml"];  
        NSData* data2= [[NSData alloc] initWithContentsOfFile:filePath2];
        parser2= [[NSXMLParser alloc] initWithData:data2];
        [parser2 setDelegate:self];
        [parser2 parse];
        
    }
    
    return self;
}

- (PoemWordsDatabase*) getPoemWordsDB{
    return db;
}

#pragma mark — NSXMLParser delegate

- (void)parserDidStartDocument:(NSXMLParser *)parser{
    NSLog(@"Parsing poem words file");
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    if (parser==parser1)
        NSLog(@"Parser 1 done");
    else if (parser==parser2)
        NSLog(@"Parser 2 done");

    
    NSLog(@"Loaded %d poem words", [db.poemwords count]);
    NSLog(@"Low: %i, high: %i", [db.lowfreqpoemwords count],[db.highfreqpoemwords count]);
    NSLog(@"----");
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSLog(@"%@", parseError);

    if (parser==parser1)
        NSLog(@"for parser 1");
    else if (parser==parser2)
        NSLog(@"for parser 2");
}

- (void)parser:(NSXMLParser *)parser 
didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName 
    attributes:(NSDictionary *)attributeDict {
    
    if ([elementName isEqualToString:@"database"]){
    } else if ([elementName isEqualToString:@"poemw"]){
        addPoemWord=YES;
    } else if ([elementName isEqualToString:@"name"]){
        addName=YES;
    }
}


- (void)parser:(NSXMLParser *)parser 
 didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"poemw"]){
        addPoemWord=NO;

        if (parser==parser2){
            [db addWord:currentEntry forFrequency:@"high"];
        }else if (parser==parser1){
            [db addWord:currentEntry forFrequency:@"low"];
        }
    
    } else if ([elementName isEqualToString:@"name"]){
        addName=NO;
    }
}

- (void)parser:(NSXMLParser *)parser 
foundCharacters:(NSString *)string{

    if (addName)
        currentEntry=string;
}

@end
