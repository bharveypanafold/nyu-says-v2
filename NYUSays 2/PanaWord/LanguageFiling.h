//
//  LanguageFiling.h
//  PanaWord
//
//  Created by Panafold on 10/10/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    FilingURL,
    FilingImage
} filingType;

@class LanguageWord;

@interface LanguageFiling : NSObject

@property (weak,nonatomic) LanguageWord *word;
@property (strong,nonatomic) NSMutableDictionary *content; // rather generic for the moment

// State
@property (nonatomic,getter = hasRead) BOOL read;
@property (nonatomic) BOOL isDef; //set to true if it is the definition

+ (LanguageFiling*)filingFromDictionary:(NSDictionary*)dict;

@end
