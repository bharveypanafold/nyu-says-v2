//
//  UIColor-Extras.h
//  Petals
//
//  Created by Panafold on 8/28/08.
//  Copyright 2008 Orange Petal LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIColor(Extras)

// Utilities
+ (UIColor*)randomColor;
+ (UIColor*)randomColorWithAlpha:(CGFloat)alpha;
+ (UIColor*)randomHueColorWithSaturation:(CGFloat)saturation brightness:(CGFloat)brightness;

// Custom Colors
+ (UIColor*)oceanColor;
+ (UIColor*)alertRed;
+ (UIColor*)warningYellow;
+ (UIColor*)infoGreen;
+ (UIColor*)veryLightBlue;
+ (UIColor*)grayBlue;
+ (UIColor*)messageOrange;

@end
