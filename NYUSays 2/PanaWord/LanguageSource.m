//
//  LanguageSource.m
//  PanaWord
//
//  Created by Panafold on Summer 2013.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "LanguageSource.h"
#import "LanguageTopic.h"
#import "LanguageWordPack.h"
#import "LanguageWord.h"
#import "LanguageFiling.h"


@interface NSString(LanguageSourceAdditions)
- (NSString*)whiteSpaceStripped;
@end

@implementation NSString(LanguageSourceAdditions)
- (NSString*)whiteSpaceStripped {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}
@end


@interface LanguageSource()
- (NSURL *)applicationDocumentsDirectory;
+ (NSMutableDictionary*)loadAndParseGloss;
+ (NSMutableArray*)loadAndParsePacks;

@end

@implementation LanguageSource

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;


@synthesize packs;

- (id)init {
    self = [super init];
    if (self) {
        OPLog(@"LanguageSource init");
    }
    return self;
}


+ (LanguageSource*)sharedInstance {
    
    static LanguageSource *_sharedSource = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedSource = [[self alloc] init];
    });

    return _sharedSource;
}

+ (NSDictionary*)gloss {
    
    static NSDictionary *_nGloss = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // once
        NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
                          stringByAppendingPathComponent:@"norskEnglishGloss.plist"];
        NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:path];
        
        if (dictionary) {
            _nGloss = dictionary;
        } else {
            _nGloss = [self loadAndParseGloss];
        }
        
    });
    return _nGloss;
    
}

+ (NSMutableArray*)topics {
    
    static NSMutableArray *_topicArray = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // once
        NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
                          stringByAppendingPathComponent:@"topicsArchive.plist"];
        
        BOOL shouldReset = [[[NSUserDefaults standardUserDefaults] valueForKey:@"reset_data"] boolValue];
        
        if (shouldReset) {
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:NO] forKey:@"reset_data"];
        }        
        
        NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithContentsOfFile:path]];
        
        if (array != nil && !shouldReset) {
            // we need to re-establish the weak backpointers
            
            for (LanguageTopic *topic in array) {
                
                for (LanguageWordPack *pack in topic.subTopics) {
                    
                    for (LanguageWord *word in pack.words) {
                        
                        word.wordPack = pack;
                        
                        for (LanguageFiling *filing in word.filings) {
                            
                            filing.word = word;
                        }
                    }
                }
            }
            
            _topicArray = array;
        } else {
            _topicArray = [self loadAndParsePacks];
        }
        
    });
    return _topicArray;
}

+ (void)saveData {
    
    OPLog(@"saving topic tree");
    NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] 
                      stringByAppendingPathComponent:@"topicsArchive.plist"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[self topics]];
    
    if (![data writeToFile:path atomically:YES]) {
        OPLog(@"failed to write topics archive data");
    }
}

#pragma mark -


+ (NSArray*)reverseMegFormatForLine:(NSString*)line {
    
    NSArray *a = nil;
    NSString *str = line;
    
    if ([[line substringToIndex:1] isEqualToString:@"+"]) {
        
        str = [str stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        NSArray *a1 = [str componentsSeparatedByString:@"^"];
        
        if ([a1 count] == 2) {
            //NSAssert([a1 count] == 2, @"invalid ^ split");
            
            NSString *first = [[a1 objectAtIndex:0] whiteSpaceStripped];
            
            str = [[a1 objectAtIndex:1] whiteSpaceStripped];
            
            NSArray *a2 = [str componentsSeparatedByString:@";"];
            if ([a2 count] == 2) {
                //NSAssert([a2 count] == 2, @"invalid  split");
                
                NSString *second = [[a2 objectAtIndex:0] whiteSpaceStripped];
                
                NSString *third = [[a2 objectAtIndex:1] whiteSpaceStripped];
                
                a = [NSArray arrayWithObjects:first,second,third, nil];

            }
        }
    }
    
    return a;
}

+ (NSMutableDictionary*)loadAndParseGloss {
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:1];
    
    
    NSString *rawDataPath = [[NSBundle mainBundle] pathForResource:@"Norsk-English" ofType:@"txt"];
    NSData *rawData = [NSData dataWithContentsOfFile:rawDataPath];
    
    NSAssert(rawData != nil, @"data fail");
    
    NSString *rawString = [[NSString alloc] initWithBytes:[rawData bytes]
                                                   length:[rawData length]
                                                 encoding:NSUTF8StringEncoding];
    
    NSAssert([rawString length] > 0,@"wtf string");
    
    NSArray *lines = [rawString componentsSeparatedByString:@"\n"];
    NSInteger lineCount = [lines count]; 
    OPLog(@"%i lines - Norsk-English",lineCount);
    
    // skip first 10 lines
    OPLog(@" ");
    for (NSInteger j = 10; j < lineCount; j++ ) {
        
        NSString *line = [lines objectAtIndex:j];
        
        NSArray *bits = [self reverseMegFormatForLine:line];
        
        if (bits) {
            [dictionary setValue:[bits objectAtIndex:2]
                          forKey:[bits objectAtIndex:0]];
            
        } else {
            OPLog(@"%@",line);
        }
    }
    OPLog(@" ");

    
    NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] 
                      stringByAppendingPathComponent:@"norskEnglishGloss.plist"];
    
    if (![dictionary writeToFile:path atomically:YES]) {
        OPLog(@"failed to write dictionary");
    }
    
    return dictionary;
}

+ (NSMutableArray*)loadAndParsePacks {
    
    NSString *rawDataPath = [[NSBundle mainBundle] pathForResource:@"TopicList" ofType:@"txt"];
    NSData *rawData = [NSData dataWithContentsOfFile:rawDataPath];
    
    NSAssert(rawData != nil, @"data fail");
    
    NSString *rawString = [[NSString alloc] initWithBytes:[rawData bytes]
                                                   length:[rawData length]
                                                 encoding:NSUTF8StringEncoding];
    
    NSAssert([rawString length] > 0,@"wtf string");
    
    NSArray *lines = [rawString componentsSeparatedByString:@"\n"];
    NSInteger lineCount = [lines count]; 
    OPLog(@"%i lines",lineCount);
    
    NSMutableArray *topics = [[NSMutableArray alloc] initWithCapacity:1];
    
    LanguageTopic *topic = nil;
    LanguageWordPack *subTopic = nil;
    
    // skip first 5 lines
    for (NSInteger j = 5; j < lineCount; j++ ) {
        
        NSString *line = [lines objectAtIndex:j];
        
        if ([line length] > 0) {
            NSString *first = [line substringToIndex:1];
            
            if ([first isEqualToString:@"="]) {
                
                topic = [[LanguageTopic alloc] init];
                
                NSArray *parts = [line componentsSeparatedByString:@"/"];
                
                NSString *norskName = [[parts objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                // remove the =
                norskName = [norskName stringByReplacingOccurrencesOfString:@"=" withString:@""];
                
                if ([parts count] == 2) {
                    NSString *engName = [[parts objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    topic.englishTitle = [engName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                
                topic.subTopics = [[NSMutableArray alloc] initWithCapacity:1];
                topic.title = [norskName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                [topics addObject:topic];
                
                
            } else if ([first isEqualToString:@">"]) {
                
                subTopic = [[LanguageWordPack alloc] init];
                
                NSArray *parts = [line componentsSeparatedByString:@"/"];
                
                NSString *norskName = [[parts objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                // remove the >
                norskName = [norskName stringByReplacingOccurrencesOfString:@">" withString:@""];
                
                if ([parts count] == 2) {
                    //NSString *engName = [[parts objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    //subTopic.englishTitle = [engName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                
                subTopic.words = [[NSMutableArray alloc] initWithCapacity:1];
                subTopic.title = [norskName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                [topic.subTopics addObject:subTopic];
                
            } else if ([first isEqualToString:@"+"]) {
                
                // +WORD ^ Nor Def ; Eng gloss
                LanguageWord *word = nil;
                
                //NSArray *parts = [line componentsSeparatedByString:@"^"];
                
                NSArray *parts = [self reverseMegFormatForLine:line];
                
                if (3 == [parts count]) {
                    word = [[LanguageWord alloc] init];
                    word.term = [parts objectAtIndex:0];
                    word.definition = [parts objectAtIndex:1];
                    word.gloss = [parts objectAtIndex:2];
                } else {
                    OPLog(@"%@",line);
                    //OPLog(@"%@",parts);
                    OPLog(@" ");
                }
                /*
                if ([parts count] > 1) {
                    word.term = [[parts objectAtIndex:0] stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    
                    parts = [[parts objectAtIndex:1] componentsSeparatedByString:@";"];
                    
                    NSAssert([parts count] >= 2,@"wtf");
                    NSString *def = [parts objectAtIndex:0];
                    NSString *gloss = [parts objectAtIndex:1];
                    
                    word.definition = [def stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    word.gloss = [gloss stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                } else {
                    
                    parts = [line componentsSeparatedByString:@";"];
                    NSString *t = [[parts objectAtIndex:0] stringByReplacingOccurrencesOfString:@"+" withString:@""];
                    word.term = [t stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    word.gloss = [[parts objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                }*/
                if (word)
                    [subTopic.words addObject:word];
            }
        }
    }
    
    
    NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] 
                      stringByAppendingPathComponent:@"topicsArchive.plist"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:topics];
    
    if (![data writeToFile:path atomically:YES]) {
        OPLog(@"failed to write topics archive data");
    }
    
    OPLog(@"%i topics",[topics count]);
    
    return topics;
    
}



- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"EmptyCoreData" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"EmptyCoreData.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
