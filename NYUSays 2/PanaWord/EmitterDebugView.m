//
//  EmitterDebugView.m
//  PanaWord
//
//  Created by Panafold on 9/27/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "EmitterDebugView.h"
#import "UIColor-Extras.h"

@interface EmitterDebugView()
@property BOOL hasViews;
@end

@implementation EmitterDebugView

@synthesize emitter;
@synthesize hasViews;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _blockDict = CFDictionaryCreateMutable(NULL,
                                               16, 
                                               NULL,
                                               &kCFTypeDictionaryValueCallBacks);
        
        self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
        
        self.layer.borderColor = [UIColor darkGrayColor].CGColor;
        self.layer.borderWidth = 2.;
    }
    return self;
}

- (void)dealloc {
    CFRelease(_blockDict);
}

/* From CAEmitterLayer.h
 --------------------------------
@property float birthRate;

/ * The cell lifetime range is multiplied by this value when particles are
 * created. Defaults to one. Animatable. /

@property float lifetime;

/ * The center of the emission shape. Defaults to (0, 0, 0). Animatable. /

@property CGPoint emitterPosition;
@property CGFloat emitterZPosition;

/ * The size of the emission shape. Defaults to (0, 0, 0). Animatable.
 * Depending on the `emitterShape' property some of the values may be
 * ignored. /

@property CGSize emitterSize;
@property CGFloat emitterDepth;

/ * A string defining the type of emission shape used. Current options are:
 * `point' (the default), `line', `rectangle', `circle', `cuboid' and
 * `sphere'. /

@property(copy) NSString *emitterShape;

/ * A string defining how particles are created relative to the emission
 * shape. Current options are `points', `outline', `surface' and
 * `volume' (the default). /

@property(copy) NSString *emitterMode;

/ * A string defining how particles are composited into the layer's
 * image. Current options are `unordered' (the default), `oldestFirst',
 * `oldestLast', `backToFront' (i.e. sorted into Z order) and
 * `additive'. The first four use source-over compositing, the last
 * uses additive compositing. /

@property(copy) NSString *renderMode;

/ * When true the particles are rendered as if they directly inhabit the
 * three dimensional coordinate space of the layer's superlayer, rather
 * than being flattened into the layer's plane first. Defaults to false.
 * If true, the effect of the `filters', `backgroundFilters' and shadow-
 * related properties of the layer is undefined. /

@property BOOL preservesDepth;

/ * Multiplies the cell-defined particle velocity. Defaults to one.
 * Animatable. /

@property float velocity;

/ * Multiplies the cell-defined particle scale. Defaults to one. Animatable. /

@property float scale;

/ * Multiplies the cell-defined particle spin. Defaults to one. Animatable. /

@property float spin;

*/

typedef void(^ControlCallback)(UIControl* theControl);

- (void)controlCallback:(UIControl*)control {
    
    //void(^ControlBlock)(UIControl* aControl) myBlock;
    const void* blockPtr = NULL;
    
    if (CFDictionaryGetValueIfPresent(_blockDict, (__bridge void*)control, &blockPtr)) {
        ControlCallback theBlock = (__bridge ControlCallback)blockPtr;
        if (theBlock)
            theBlock(control);
    }
}

- (UIView *)controlBoxWithTitle:(NSString*)title {
    UIView *box = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.bounds.size.width, 44.)];
    
    box.layer.borderWidth = 1.;
    box.layer.borderColor = [UIColor randomColor].CGColor;
    box.backgroundColor = [UIColor clearColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(4., 4., box.bounds.size.width, 14.)];
    label.font = [UIFont systemFontOfSize:12.];
    label.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor clearColor];
    label.text = title;
    [box addSubview:label];
    
    return box;
}

- (void)triggerEmitterUpdate {
    CGFloat rate = self.emitter.birthRate;
    
    self.emitter.birthRate = 0;
    
    self.emitter.birthRate = rate;
}

- (void)buildViews {
    // Tweakable values:
    
    // birthrate    3.0
    UIView *container = [self controlBoxWithTitle:@"birth rate"];
    UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(20.,
                                                                  container.bounds.size.height/2.-10.,
                                                                  100.,
                                                                  30.)];
    
    slider.minimumValue = 0.0;
    slider.maximumValue = 4.0;
    slider.value = self.emitter.birthRate;
    [slider addTarget:self action:@selector(controlCallback:) forControlEvents:UIControlEventValueChanged];
    [container addSubview:slider];
    
    [self addSubview:container];
    
    ControlCallback call = ^(UIControl *theControl) {
        self.emitter.birthRate = ((UISlider*)theControl).value;
    };
    
    CFDictionarySetValue(_blockDict,
                         (__bridge void*)slider,
                         Block_copy((__bridge void*)call));
    
    
    
    // CELL
    CAEmitterCell *cell = [self.emitter.emitterCells lastObject];
    
    // velocity     150
    CGRect f = container.frame;
    container = [self controlBoxWithTitle:@"cell velocity"];
    f.origin.y += container.bounds.size.height;
    container.frame = f;
    
    slider = [[UISlider alloc] initWithFrame:CGRectMake(20.,
                                                        container.bounds.size.height/2.-10.,
                                                        100.,
                                                        30.)];
    slider.minimumValue = 0.0;
    slider.maximumValue = 400.0;
    slider.value = cell.velocity;
    [slider addTarget:self action:@selector(controlCallback:) forControlEvents:UIControlEventValueChanged];
    [container addSubview:slider];
    
    [self addSubview:container];
    
    call = ^(UIControl *theControl) {
        cell.velocity = ((UISlider*)theControl).value;
        [self triggerEmitterUpdate];
    };
    
    CFDictionarySetValue(_blockDict,
                         (__bridge void*)slider,
                         Block_copy((__bridge void*)call));
    
    
    // velocity range       100
    f = container.frame;
    container = [self controlBoxWithTitle:@"cell velocity range"];
    f.origin.y += container.bounds.size.height;
    container.frame = f;
    
    slider = [[UISlider alloc] initWithFrame:CGRectMake(20.,
                                                        container.bounds.size.height/2.-10.,
                                                        100.,
                                                        30.)];
    slider.minimumValue = 0.0;
    slider.maximumValue = 100.0;
    slider.value = cell.velocityRange;
    [slider addTarget:self action:@selector(controlCallback:) forControlEvents:UIControlEventValueChanged];
    [container addSubview:slider];
    
    [self addSubview:container];
    
    call = ^(UIControl *theControl) {
        cell.velocityRange = ((UISlider*)theControl).value;
        [self triggerEmitterUpdate];
    };
    
    CFDictionarySetValue(_blockDict,
                         (__bridge void*)slider,
                         Block_copy((__bridge void*)call));
    
    
    // lifetime     15
    f = container.frame;
    container = [self controlBoxWithTitle:@"cell lifetime"];
    f.origin.y += container.bounds.size.height;
    container.frame = f;
    
    slider = [[UISlider alloc] initWithFrame:CGRectMake(20.,
                                                        container.bounds.size.height/2.-10.,
                                                        100.,
                                                        30.)];
    slider.minimumValue = 0.0;
    slider.maximumValue = 40.0;
    slider.value = cell.lifetime;
    [slider addTarget:self action:@selector(controlCallback:) forControlEvents:UIControlEventValueChanged];
    [container addSubview:slider];
    
    [self addSubview:container];
    
    call = ^(UIControl *theControl) {
        cell.lifetime = ((UISlider*)theControl).value;
        [self triggerEmitterUpdate];
    };
    
    CFDictionarySetValue(_blockDict,
                         (__bridge void*)slider,
                         Block_copy((__bridge void*)call));
}

- (void)layoutSubviews {
    
    if (!self.hasViews) {
        [self buildViews];
        self.hasViews = YES;
    }
    
    
    
}

@end
