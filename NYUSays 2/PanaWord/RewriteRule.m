//
//  RewriteRule.m
//  EnglishDiscovery
//
//  Created by Guillaume on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RewriteRule.h"

@implementation RewriteRule
@synthesize affix1, affix2, result;

-(id) initWithAffix1:(NSString*)a1 andAffix2:(NSString*)a2 forResult:(NSString*)r{
    self=[super init];
    
    self.affix1=a1;
    self.affix2=a2;
    self.result=r;

    return self;
}

@end
