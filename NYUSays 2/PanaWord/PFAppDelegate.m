//
//  PFAppDelegate.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "PFAppDelegate.h"

#import "PFViewController.h"

#import "LanguageSource.h"
#import "LanguageWordPack.h"
#import "LanguageWord.h"
#import "LanguageFiling.h"

#import "SimpleVerb.h"
#import "JSONKit.h"

@implementation PFAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (NSString*)uniqueAppIdentifier {
    
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    
    NSString *uid = [df stringForKey:@"uniqueAppIdentifier"];
    
    if (!uid) {
        OPLog(@"uniqueAppIdentifier not found.");
        CFUUIDRef ux = CFUUIDCreate(NULL);
        uid = (__bridge_transfer NSString*)CFUUIDCreateString(NULL,ux);
        
        [df setValue:uid
              forKey:@"uniqueAppIdentifier"];

        ///free(&ux);
    }
    
    
    return uid;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    OPLogMethod;
//#ifndef TARGET_IPHONE_SIMULATOR
    NSString *teamToken = @"c8e2a7d9aea8f520d909a288801231ae_MjQ0MDYyMDExLTEwLTIwIDEzOjA2OjQyLjEyODY4MA";
    [TestFlight takeOff:teamToken];
    
    [TestFlight addCustomEnvironmentInformation:[self uniqueAppIdentifier]
                                         forKey:@"panafoldUniqueAppIdentifier"];
//#endif
    
    //LanguageSource *shared = [LanguageSource sharedInstance];
    //NSMutableArray *topicList = [LanguageSource topics];
    //shared.packs = topicList;


    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[PFViewController alloc] initWithNibName:@"PFViewController" bundle:nil];
    
    
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    nav.navigationBarHidden = YES;
    nav.toolbarHidden = NO;
    [nav.toolbar setAlpha:0.7f];
    self.window.rootViewController = nav;
    
    [self.window makeKeyAndVisible];
        
    
    OPLog(@"appdidfinish done.");
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    
//    [LanguageSource saveData];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    
    //[[LanguageSource sharedInstance] saveContext];
    
//    [LanguageSource saveData];
}

@end
