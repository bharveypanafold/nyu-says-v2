//
//  GhostBoxView.h
//  PanaFlow
//
//  Created by Panafold on 9/13/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BoxWorld;

@interface GhostBoxView : UIView

@property (weak,nonatomic) BoxWorld *world;
@end
