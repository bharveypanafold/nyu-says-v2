//
//  UIView+Context.m
//  PanaWord
//
//  Created by Panafold on 9/19/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "UIView+Context.h"
#import <QuartzCore/QuartzCore.h>

@interface OPWeakContainer : NSObject
@property (weak,nonatomic) id pointer;
@end

@implementation OPWeakContainer
@synthesize pointer;
@end

@implementation UIView(Context)

@dynamic weakContext;

- (void)setWeakContext:(id)weakContext {
    OPWeakContainer *container = [[OPWeakContainer alloc] init];
    container.pointer = weakContext;
    [self.layer setValue:container
                  forKey:@"OPWeakContainer"];
}

- (id)weakContext {
    OPWeakContainer *container = [self.layer valueForKey:@"OPWeakContainer"];
    return container.pointer;
}

@end
