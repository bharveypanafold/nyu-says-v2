//
//  MorphemeLoader.h
//  EnglishDiscovery
//
//  Created by Guillaume on 5/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LanguageWord.h"
#import "MorphemeDatabase.h"

#define FILENAME_LATINGREEK_ROOTS @"latin-greek-roots"
#define FILENAME_AFFIXES @"affixes"

@interface MorphemeLoader : NSObject <NSXMLParserDelegate>{
    NSXMLParser* parser;
    BOOL addEntry;
    BOOL addName;
    BOOL addDef;
    LanguageWord* currentEntry;
    MorphemeDatabase* db;
}

- (id) init;
- (MorphemeDatabase*) getMorphemeDB;

@end
