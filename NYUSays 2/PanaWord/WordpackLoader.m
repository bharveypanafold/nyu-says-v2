//
//  WordpackLoader.m
//  EnglishDiscovery
//
//  Created by Guillaume on 5/30/12.
//  Copyright (c) 2012 Guillaume. All rights reserved.
//

#import "WordpackLoader.h"

@implementation WordpackLoader
@synthesize dictionaryManager;

//@synthesize setdefinition;

//VV
//NSMutableDictionary *FINALDICT=nil;
- (id)initWithDictManager:(DictionaryManager*)dict{
    
    self = [super init];
    if (self) {
        self.dictionaryManager=dict;
        //self.words = [NSMutableArray arrayWithCapacity:10];
        //VV added
           NSLog(@"here");
                db= [[WordpackDatabase alloc] init];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:WORDPACK_FILE ofType:@"xml"];
        NSData* data= [[NSData alloc] initWithContentsOfFile:filePath];
        
        parser= [[NSXMLParser alloc] initWithData:data];
        [parser setDelegate:self];
        [parser parse];
    }
    return self;
}
- (id) init{
    self=[super init];
    
    if (self){
        
        NSLog(@"here");
        db= [[WordpackDatabase alloc] init];
        dictionaryManager  = [[DictionaryManager alloc] init];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:WORDPACK_FILE ofType:@"xml"];  
        NSData* data= [[NSData alloc] initWithContentsOfFile:filePath];
        
        parser= [[NSXMLParser alloc] initWithData:data];
        [parser setDelegate:self];
        [parser parse];
    }

    return self;
}

- (WordpackDatabase*) getWordpackDB{
    return db;
}

//- (WordPackDatabase*) getWordPackNamed:(NSString*) wordpack{}

#pragma mark — NSXMLParser delegate

- (void)parserDidStartDocument:(NSXMLParser *)parser{
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    NSLog(@"Loaded %d words", [db.words count]);
    NSLog(@"Loaded %d wordpacks", [db.wordpackTitles count]);
    NSLog(@"Wordpacks: %@", db.wordpackTitles);
}

- (void)parser:(NSXMLParser *)parser 
didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName 
    attributes:(NSDictionary *)attributeDict {

    if ([elementName isEqualToString:@"wordpack"]){
        addWordpack=YES;
        currentWordpack=[[LanguageWordPack alloc] init];
        
        if ([attributeDict objectForKey:@"name"] != nil)
            currentWordpack.title=[attributeDict objectForKey:@"name"];
        else
            currentWordpack.title=@"Unnamed wordpack";

        
    } else if ([elementName isEqualToString:@"prefix"] || [elementName isEqualToString:@"stem"] || [elementName isEqualToString:@"suffix"]){
        addAffix=YES;
    } else if ([elementName isEqualToString:@"fullword"]){
        addFullword=YES; 
    } else if ([elementName isEqualToString:@"word"]){
        addEntry=YES;
        currentEntry= [[WordDescription alloc] init];
    } else if ([elementName isEqualToString:@"rewrite"]){
        addRewriteRule=YES;
        rule= [[RewriteRule alloc] init];
    } else if ([elementName isEqualToString:@"elementA"]){
        addElementA=YES;
    } else if ([elementName isEqualToString:@"elementB"]){
        addElementB=YES;
    } else if ([elementName isEqualToString:@"result"]){
        addResult=YES;
    } //VV added to read defintion
    else if ([elementName isEqualToString:@"definition"]){
        setdefinition=YES;
    }
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"wordpack"]){
        addWordpack=NO;
        [[db wordpackTitles] addObject:currentWordpack.title]; //TODO
        [db addWordPack:currentWordpack];
    } else if ([elementName isEqualToString:@"prefix"] || [elementName isEqualToString:@"stem"] || [elementName isEqualToString:@"suffix"]){
        addAffix=NO;        
    } else if ([elementName isEqualToString:@"fullword"]){
        addFullword=NO;
    } else if ([elementName isEqualToString:@"word"]){
        addEntry=NO;
        [db addWord:currentEntry];
        [[currentWordpack words] addObject:currentEntry];
        //NSLog(@"Entries in current wordpack: %d", [[currentWordpack words] count]);
    } else if ([elementName isEqualToString:@"rewrite"]){
        addRewriteRule=NO;
        [currentEntry addRewriteRule:rule];
    } else if ([elementName isEqualToString:@"elementA"]){
        addElementA=NO;
    } else if ([elementName isEqualToString:@"elementB"]){
        addElementB=NO;
    } else if ([elementName isEqualToString:@"result"]){
        addResult=NO;
    }//VV added to read definition
    else if ([elementName isEqualToString:@"definition"]){
       setdefinition=NO;
    }
}

- (void)parser:(NSXMLParser *)parser 
foundCharacters:(NSString *)string{
    if (addFullword){
        currentEntry.fullword=string;
    } else if (addAffix){
        [currentEntry addAffix:string];
    } else if (addElementA){
        rule.affix1=string;
    } else if (addElementB){
        rule.affix2=string;
    } else if (addResult){
        rule.result=string;
    }else if (setdefinition){
    
        //VV added from languagewordpack
        [self.dictionaryManager setDefinition:string ForWord: currentEntry.fullword];
            NSLog(@"old paris %@ ", [self.dictionaryManager getDefinitionForWord:@"Paris"]);

        
        //NSLog(@"Added definition: %@ for word: %@", [self.dictionaryManager getDefinitionForWord:currentEntry.fullword], currentEntry.fullword);
    }
    
}

- (NSString *)retDef: (NSString *)term{
     //[dictionaryManager setDefinition:string ForWord: currentEntry.fullword];
    NSString *theDef = [self.dictionaryManager getDefinitionForWord:term];
    return theDef;
}

@end
