//
//  PackPickerController.h
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LanguageWordPack;

@interface PackPickerController : UIViewController

@property (strong,nonatomic) LanguageWordPack *chosenPack;

@end
