//
//  FluffyRiverView.m
//  PanaWord
//
//  Created by Panafold on 9/18/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "FluffyRiverView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor-Extras.h"

@interface FluffyRiverView()

@property (strong,nonatomic) CAEmitterLayer *emitter;

@end

@implementation FluffyRiverView

@synthesize emitter;
@dynamic emitterLayer;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CAEmitterLayer *emitterLayer = [CAEmitterLayer layer];
        self.emitter = emitterLayer;
        [self.layer addSublayer:emitterLayer];
        
    }
    return self;
}

- (CAEmitterLayer*)emitterLayer {
    return self.emitter;
}

- (void)layoutSubviews {

    CAEmitterLayer *emitterLayer = self.emitter;
    emitterLayer.position = CGPointMake(0,768./2);
    emitterLayer.emitterShape = kCAEmitterLayerRectangle;
    emitterLayer.emitterMode = kCAEmitterLayerSurface;
    emitterLayer.renderMode = kCAEmitterLayerUnordered;
    emitterLayer.emitterSize = CGSizeMake(0,768);
    
    CAEmitterCell *cell = [CAEmitterCell emitterCell];
    cell.contents = (__bridge id)[UIImage imageNamed:@"filing"].CGImage;
    cell.birthRate = 2;
    cell.emissionLongitude = 0;
    
    cell.velocity = 60;
    cell.velocityRange = 60.0;
    
    cell.lifetime = 30.0;
    
    //cell.color = [UIColor randomColor].CGColor;
    
    //CGFloat particleMultiplier = 0.4;
    
    //cell.birthRate = particleMultiplier * 85.0;
    //cell.velocity = 180;
    
    //cell.emissionRange = 1.1;
    //cell.alphaRange = .4;
    //cell.spin = .6;
    //cell.spinRange = .3;
    //cell.scaleSpeed = 0.3;
    
    emitterLayer.emitterCells = [NSArray arrayWithObjects:cell, nil];
}

@end
