//
//  ContentViewController.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "ContentViewController.h"
#import <QuartzCore/QuartzCore.h>
//#import <Twitter/Twitter.h>

@interface ContentViewController() <UIWebViewDelegate>

@property (strong,nonatomic) UIWebView *webView;
@property (strong,nonatomic) UIPopoverController *popoverController;
@property (weak,nonatomic) id notificationObserver;
@property (nonatomic) UIDeviceOrientation currentOrientation;

@property (weak,nonatomic) UIActivityIndicatorView *spinner;


@end

@implementation ContentViewController

@synthesize filing;
@synthesize webView;
@synthesize popoverController;
@synthesize notificationObserver;
@synthesize spinner;
@synthesize currentOrientation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.autoresizesSubviews=YES;
                
        [self setupWebView];
        [self setupControlBar];
    }
    return self;
}

- (void) showActionPopover:(id)sender{
    if ([self.popoverController isPopoverVisible]){
        [self.popoverController dismissPopoverAnimated:YES];
        return;
    }
    
 //   CGRect contentRect = CGRectMake(0, 0, 200, 40);
 //   UIButton *tweetaction = [UIButton buttonWithType:UIButtonTypeRoundedRect];
 //   [tweetaction addTarget:self action:@selector(tweet:) forControlEvents:UIControlEventTouchUpInside];
  //  tweetaction.frame = contentRect;
 //   [tweetaction setTitle:@"Tweet" forState:UIControlStateNormal];

    CGRect contentRect2 = CGRectMake(0, 50, 200, 40);
    UIButton *safariaction = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [safariaction addTarget:self action:@selector(openInSafari:) forControlEvents:UIControlEventTouchUpInside];
    safariaction.frame = contentRect2;
    [safariaction setTitle:@"Open in Safari" forState:UIControlStateNormal];

    UIViewController* popoverContent = [[UIViewController alloc] init];

  //  [popoverContent.view insertSubview:tweetaction atIndex:0];
    [popoverContent.view insertSubview:safariaction atIndex:0];

    popoverContent.contentSizeForViewInPopover = CGSizeMake(200, 90);
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    [popoverController presentPopoverFromBarButtonItem:sender
                              permittedArrowDirections:UIPopoverArrowDirectionAny 
                                              animated:YES];
    
}

-(NSString *) substr:(NSString*)str ofLen:(int)len{
    // define the range you're interested in
    NSRange stringRange = {0, MIN([str length], len)};
    
    // adjust the range to include dependent chars
    stringRange = [str rangeOfComposedCharacterSequencesForRange:stringRange];
    
    // Now you can create the short string
    return [str substringWithRange:stringRange];
}

- (void) openInSafari:(id)sender{
    [self.popoverController dismissPopoverAnimated:YES];
    NSString* url=[self.filing.content valueForKey:@"page"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

// - (void) tweet:(id)sender{
//    NSString* filingURL= [self.filing.content valueForKey:@"page"];
//    NSString* filingTitle= [self.filing.content valueForKey:@"title"];
//    filingTitle= [[NSString alloc] initWithFormat:@"Currently reading \"%@\"", filingTitle];
    
    //Create the tweet sheet
//    TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
    //Add a link
    //We add the link first to make sure it'll fit
 //   BOOL success= [tweetSheet addURL:[NSURL URLWithString:filingURL]];
    //Customize the tweet sheet here
    //Add a tweet message
//    [tweetSheet setInitialText:[self substr:filingTitle ofLen:120]];
 //   NSLog(@"%i", success);
    
    //Add an image
    //[tweetSheet addImage:[UIImage imageNamed:@"tweetThumb.png"]];
    
    
    //Set a blocking handler for the tweet sheet
 //   tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
 //       [self dismissModalViewControllerAnimated:YES];
 //   };
    
    //Show the tweet sheet!
 //   [self.popoverController dismissPopoverAnimated:YES];

 //   [self presentModalViewController:tweetSheet animated:YES];    
//}

- (void)dealloc {
    self.webView.delegate = nil; // REQUIRED!
}

- (void)cancelAction:(id)sender {
    
    [self.parentViewController dismissModalViewControllerAnimated:YES];
}

- (void)saveAction:(id)sender {
    
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    
    NSData* publicText = [pb dataForPasteboardType:@"public.text"];
    //OPLog(@"public.text: %@",publicText);
    
    NSString *str = [[NSString alloc] initWithBytes:[publicText bytes]
                                             length:[publicText length]
                                           encoding:NSUTF8StringEncoding];
    OPLog(@"public text: %@",str);
    
    [self.parentViewController dismissModalViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //setup notifications for device orientation change
/*
    currentOrientation = UIInterfaceOrientationPortrait;
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector: @selector(didRotateFromInterfaceOrientation:)
                                                 name: UIDeviceOrientationDidChangeNotification
                                               object: nil];
    
    if(currentOrientation != self.interfaceOrientation)
        [self rotateView:self.interfaceOrientation];
*/  
    
    /*
    self.notificationObserver = [[NSNotificationCenter defaultCenter] addObserverForName:UIPasteboardChangedNotification
                                                                                  object:nil
                                                                                   queue:nil
                                                                              usingBlock:^(NSNotification *note) {
                                                                                  //...
                                                                                  self.navigationItem.rightBarButtonItem.enabled = YES;
                                                                                  
                                                                                  UIPasteboard *pb = [UIPasteboard generalPasteboard];
                                                                                  
                                                                                  //OPLog(@"pasteboard: %@",pb.items);
                                                                                  //OPLog(@"pb %i",[pb.items count]);
                                                                                  OPLog(@"%@",pb.pasteboardTypes);
                                                                                  
                                                                                  
                                                                              }];*/
}
    
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.webView.delegate = nil;
    self.webView = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSString* urlString= [[self.filing.content valueForKey:@"page"] stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

#pragma mark - Web View Delegate

-(void)setupControlBar{
    //add "Done" button to the left
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                          target:self
                                                                                          action:@selector(cancelAction:)];
    
    //add spinner to the right
    UIActivityIndicatorView *pv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = pv;
    UIBarButtonItem* spin= [[UIBarButtonItem alloc] initWithCustomView:pv];
    
    //share
    UIBarButtonItem* share=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction 
                                                                         target:self 
                                                                         action:@selector(showActionPopover:)];
    
    
    
    NSArray* buttons= [[NSArray alloc] initWithObjects: share, spin, nil];
    
    //add buttons
    self.navigationItem.rightBarButtonItems=buttons;
}


-(void)setupWebView{
    UIWebView *web = [[UIWebView alloc] initWithFrame:self.view.bounds];
    web.scalesPageToFit = YES;
    web.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

    [self.view addSubview:web];
    self.webView = web;
    web.delegate = self;    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    BOOL shouldLoad = NO;
    
    switch (navigationType) {
        case UIWebViewNavigationTypeOther:
            OPLog(@"UIWebViewNavigationTypeOther");
            shouldLoad = YES;
            break;
        case UIWebViewNavigationTypeLinkClicked:
            OPLog(@"UIWebViewNavigationTypeLinkClicked");
            break;
        case UIWebViewNavigationTypeFormSubmitted:
            OPLog(@"UIWebViewNavigationTypeFormSubmitted");
            break;
        case UIWebViewNavigationTypeBackForward:
            OPLog(@"UIWebViewNavigationTypeBackForward");
            break;
        case UIWebViewNavigationTypeFormResubmitted:
            OPLog(@"UIWebViewNavigationTypeFormResubmitted");
            break;
        case UIWebViewNavigationTypeReload:
            OPLog(@"UIWebViewNavigationTypeReload");
            break;
        default:
            break;
    }
    
    return shouldLoad;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    [self.spinner startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self.spinner stopAnimating];
    
    /*
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                           target:self
                                                                                           action:@selector(saveAction:)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    */
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
 
    OPLog(@"webView:didFailLoadWithError: %@",error);
}

@end
