//
//  PoemWordsLoader.h
//  EnglishDiscovery
//
//  Created by Guillaume on 5/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PoemWordsDatabase.h"

@interface PoemWordsLoader : NSObject <NSXMLParserDelegate>{
    NSXMLParser* parser1;
    NSXMLParser* parser2;

    BOOL addPoemWord;
    BOOL addName;
    NSString* currentEntry;
    PoemWordsDatabase* db;
}

- (id) init;
- (PoemWordsDatabase*) getPoemWordsDB;

@end
