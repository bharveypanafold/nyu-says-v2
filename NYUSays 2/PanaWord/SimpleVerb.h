//
//  SimpleVerb.h
//  ColorPad
//
//  Created by Panafold on 3/24/11.
//  Copyright 2011 Orange Petal, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SimpleVerbCompletionBlock)(id data, NSURLResponse* response, NSError* error);
typedef void (^SimpleVerbUpdateBlock)(NSData *chunk, long long totalLength);

@interface SimpleVerb : NSObject {}

+ (void)setShouldSerializeRequests:(BOOL)serialize;

// NOTE: Completion block is NOT run on main thread.
+ (void)sendGET:(NSURL*)url completion:(SimpleVerbCompletionBlock)completion;
+ (void)sendPOST:(NSURL*)url body:(NSString*)body completion:(SimpleVerbCompletionBlock)completion;

+ (void)sendGET:(NSURL *)url 
         update:(SimpleVerbUpdateBlock)update 
     completion:(SimpleVerbCompletionBlock)completion;

@end
