//
//  RightViewController.h
//  NYUSays
//
//  Created by Vivian Allum on 2/13/14.
//
//

#import <UIKit/UIKit.h>

#import "SnippetViewController.h"
#import "AttractorView.h"

#import "DictionaryManager.h"
#import "AttractorView.h"
#import "LanguageFiling.h"


@interface RightViewController : UITableViewController
@property (strong,nonatomic) SnippetViewController *snippetController;
@property (strong,nonatomic) UIPopoverController *filingPopoverController;
@property (strong, nonatomic) NSString* wordPackStr;
@property (strong,nonatomic) DictionaryManager* dictionarymanager;
@property (strong,nonatomic) AttractorView *currectAttractorView;
@property (strong,nonatomic) LanguageWord *word;
@property (strong, nonatomic) NSString* filingShortText;
@property (strong, nonatomic) AttractorView* attractor;
@property (strong, nonatomic) NSString * filingTextforTable;
@property (strong,nonatomic) LanguageFiling *filing;
@property (strong,nonatomic) NSMutableArray *filingViewsRVC;
@property (strong,nonatomic) NSMutableArray *filingTexts;
@property (strong,nonatomic) NSMutableArray *arrayFilings;

- (void)openAttractor:(AttractorView*)attractor;
- (void)viewWillAppear:(BOOL)animated;


@end
