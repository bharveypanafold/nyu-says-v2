//
//  ContextCardViewController.h
//  PanaWord
//
//  Created by Panafold on 11/29/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageWord.h"

@interface ContextCardViewController : UIViewController

@property (strong,nonatomic) LanguageWord *word;

@end
