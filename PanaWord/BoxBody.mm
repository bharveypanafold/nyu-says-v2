//
//  BoxBody.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "BoxBody.h"
#import "Box2D.h"
#import "BoxWorld.h"

#define bodyPtr ((b2Body*)_bodyRef)
#define bodyDef ((b2BodyDef*)_bodyDef)

@interface BoxFixture(BodyPrivate)
- (b2FixtureDef*)createb2Definition;
- (void)setFixturePtr:(b2Fixture*)fptr;
@end

@interface BoxBody()
@end

@implementation BoxBody

@dynamic position;
@synthesize fixtures;
@synthesize weakContext;
@dynamic mass;
@dynamic angularDamping,linearDamping;

- (id)initWithb2Type:(b2BodyType)type {
    self = [super init];
    
    if (self) {
        _bodyDef = new b2BodyDef;
        self.fixtures = [[NSMutableArray alloc] initWithCapacity:1];
        ((b2BodyDef*)_bodyDef)->type = type;
    }
    
    return self;
}

+ (BoxBody*)dynamicBody {
    return [[self alloc] initWithb2Type:b2_dynamicBody];
}

+ (BoxBody*)staticBody {
    return [[self alloc] initWithb2Type:b2_staticBody];
}

+ (BoxBody*)kinematicBody {
    return [[self alloc] initWithb2Type:b2_kinematicBody];
}


- (void)dealloc {
    NSAssert(NULL == _bodyRef, @"body ptr is NOT NULL. body is probably still in the world.");
    delete bodyDef;
}

- (void)addFixture:(BoxFixture*)fixture {
    [self.fixtures addObject:fixture];
}

- (CGPoint)position {
    b2Vec2 p;
    if (NULL != bodyPtr) {
        p = bodyPtr->GetPosition();
    } else {
        p = bodyDef->position;
    }
    return CGPointMake(p.x*kBoxWorldPixelScale,
                       p.y*kBoxWorldPixelScale);
}

- (void)setPosition:(CGPoint)newPosition {
    if (NULL != bodyPtr) {
        bodyPtr->SetTransform(b2Vec2(newPosition.x/kBoxWorldPixelScale,
                                     newPosition.y/kBoxWorldPixelScale), bodyPtr->GetAngle());
    } else {
        bodyDef->position = b2Vec2(newPosition.x/kBoxWorldPixelScale,
                                   newPosition.y/kBoxWorldPixelScale);
    }
}

- (CGFloat)angle {
    if (NULL != bodyPtr) {
        return bodyPtr->GetAngle();
    } else {
        return bodyDef->angle;
    }
}

- (void)setAngle:(CGFloat)angle {
    if (NULL != bodyPtr) {
        bodyPtr->SetTransform(bodyPtr->GetPosition(), angle);
    } else {
        bodyDef->angle = angle;
    }
}

- (CGFloat)mass {
    return bodyPtr->GetMass();
}

- (void)setLinearDamping:(CGFloat)linearDamping {
    if (NULL != bodyPtr) {
        bodyPtr->SetLinearDamping(linearDamping);
    } else {
        bodyDef->linearDamping = linearDamping;
    }
}

- (CGFloat)linearDamping {
    if (NULL != bodyPtr) {
        return bodyPtr->GetLinearDamping();
    } else {
        return bodyDef->linearDamping;
    }
}

- (void)setAngularDamping:(CGFloat)angularDamping {
    if (NULL != bodyPtr) {
        bodyPtr->SetAngularDamping(angularDamping);
    } else {
        bodyDef->angularDamping = angularDamping;
    }
}

- (CGFloat)angularDamping {
    if (NULL != bodyPtr) {
        return bodyPtr->GetAngularDamping();
    } else {
        return bodyDef->angularDamping;
    }
}

- (void) setActive:(bool)flag{
    bodyPtr->SetActive(flag);
}

// private
- (b2Body*)b2Ptr {
    return bodyPtr;
}

- (void)setBodyPtr:(b2Body*)bptr {
    //NSAssert(_bodyRef == bptr, @"body ptr already set. mayhaps an error hath occurred?");
    _bodyRef = bptr;
    
    if (NULL != bodyPtr)
        bodyPtr->SetUserData((__bridge void*)self);
}

- (b2BodyDef*)b2Definition {
    return bodyDef;
}

- (void)insertFixtures {
    // loop over shapes and add a fixture to the body for each
    
    for (BoxFixture *fixture in self.fixtures) {
        
        b2FixtureDef *fDef = [fixture createb2Definition];
        
        b2Fixture *fix = bodyPtr->CreateFixture(fDef);
        
        [fixture setFixturePtr:fix];
        
        delete fDef;
    }
}

@end
