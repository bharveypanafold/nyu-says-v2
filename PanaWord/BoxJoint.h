//
//  BoxJoint.h
//  PanaWord
//
//  Created by Panafold on 9/12/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//
/*
enum b2JointType
{
	e_unknownJoint,
	e_revoluteJoint,
	e_prismaticJoint,
	e_distanceJoint,
	e_pulleyJoint,
	e_mouseJoint,
	e_gearJoint,
	e_wheelJoint,
    e_weldJoint,
	e_frictionJoint,
	e_ropeJoint
};*/

#import <Foundation/Foundation.h>

@class BoxBody;

@interface BoxJoint : NSObject {
@package
    void *_jointRef;
    void *_jointDef;

}
@property (weak,nonatomic) BoxBody *bodyA;
@property (weak,nonatomic) BoxBody *bodyB;

@property (nonatomic, getter = canSelfCollide) BOOL allowSelfCollision;

@end


@interface BoxMouseJoint : BoxJoint

/// to coincide with the body anchor initially.
@property (nonatomic) CGPoint targetPoint;

/// The maximum constraint force that can be exerted
/// to move the candidate body. Usually you will express
/// as some multiple of the weight (multiplier * mass * gravity).
@property (nonatomic) CGFloat maxForce;

/// The response speed.
@property (nonatomic) CGFloat frequency;

/// The damping ratio. 0 = no damping, 1 = critical damping.
@property (nonatomic) CGFloat dampingRatio;

@end


@interface BoxDistanceJoint : BoxJoint

// by default - the local anchor points are {0,0}
// would need an initializer to change this..

/// The local anchor point relative to body1's origin.
@property (nonatomic,readonly) CGPoint localAnchorA;

/// The local anchor point relative to body2's origin.
@property (nonatomic,readonly) CGPoint localAnchorB;

/// The natural length between the anchor points.
@property (nonatomic) CGFloat length;

/// The mass-spring-damper frequency in Hertz.
@property (nonatomic) CGFloat frequency;

/// The damping ratio. 0 = no damping, 1 = critical damping.
@property (nonatomic) CGFloat dampingRatio;

@end

