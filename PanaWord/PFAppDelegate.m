//
//  PFAppDelegate.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "PFAppDelegate.h"

#import "PFViewController.h"

#import "LanguageSource.h"
#import "LanguageWordPack.h"
#import "LanguageWord.h"
#import "LanguageFiling.h"

#import "SimpleVerb.h"
#import "JSONKit.h"

#import "MMDrawerController.h"
#import "LeftViewController.h"
#import "RightViewController.h"
@implementation PFAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;


@synthesize drawerController = _drawerController;
@synthesize filingTable;
@synthesize arrayFilings;

- (NSString*)uniqueAppIdentifier {
    
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    
    NSString *uid = [df stringForKey:@"uniqueAppIdentifier"];
    
    if (!uid) {
        OPLog(@"uniqueAppIdentifier not found.");
        CFUUIDRef ux = CFUUIDCreate(NULL);
        uid = (__bridge_transfer NSString*)CFUUIDCreateString(NULL,ux);
        
        [df setValue:uid
              forKey:@"uniqueAppIdentifier"];

        ///free(&ux);
    }
    
    
    return uid;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    

    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    OPLogMethod;
//#ifndef TARGET_IPHONE_SIMULATOR
    NSString *teamToken = @"c8e2a7d9aea8f520d909a288801231ae_MjQ0MDYyMDExLTEwLTIwIDEzOjA2OjQyLjEyODY4MA";
    [TestFlight takeOff:teamToken];
    
    [TestFlight addCustomEnvironmentInformation:[self uniqueAppIdentifier]
                                         forKey:@"panafoldUniqueAppIdentifier"];
//#endif
    
    //LanguageSource *shared = [LanguageSource sharedInstance];
    //NSMutableArray *topicList = [LanguageSource topics];
    //shared.packs = topicList;

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[PFViewController alloc] initWithNibName:@"PFViewController" bundle:nil];
    
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    nav.navigationBarHidden = YES;
    nav.toolbarHidden = NO;
    [nav.toolbar setAlpha:0.7f];
    self.window.rootViewController = nav;
    
    [self.window makeKeyAndVisible];
        
    
    OPLog(@"appdidfinish done.");
    return YES;
    } //end ipad
    
    // For iphone
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
       OPLog(@"appdidfinish done. iphone");
        _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        
        LeftViewController * leftDrawer = [[LeftViewController alloc] init];
        leftDrawer.view.backgroundColor = [UIColor blackColor];
        UIViewController * center = [[UIViewController alloc] init];
        center.view.backgroundColor = [UIColor yellowColor];
        RightViewController * rightDrawer = [[RightViewController alloc] init];
        rightDrawer.view.backgroundColor = [UIColor greenColor];
        
        MMDrawerController * drawerController = [[MMDrawerController alloc]
                                                 initWithCenterViewController:center
                                                 leftDrawerViewController:leftDrawer rightDrawerViewController:rightDrawer];
        
        [drawerController setMaximumRightDrawerWidth:200];
        [drawerController setMaximumLeftDrawerWidth:200];
        
        [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
        
        _window.rootViewController = drawerController;
        [_window makeKeyAndVisible];
        
        
        return YES;
        
        
       /*
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        // Override point for customization after application launch.
        self.drawerController = [[MMDrawerController alloc] initWithNibName:@"MMDrawerController" bundle:nil];
        
        
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.drawerController];
        nav.navigationBarHidden = YES;
        nav.toolbarHidden = NO;
        [nav.toolbar setAlpha:0.7f];
        self.window.rootViewController = nav;
        
        [self.window makeKeyAndVisible];
        
        
        OPLog(@"appdidfinish done.");
        return YES;

        */
        /*
        
        UIViewController * leftDrawer = [[UIViewController alloc] init];
        UIViewController * center = [[UIViewController alloc] init];
        UIViewController * rightDrawer = [[UIViewController alloc] init];
        
        MMDrawerController * drawerController = [[MMDrawerController alloc]
                                                 initWithCenterViewController:center
                                                 leftDrawerViewController:leftDrawer
                                                 rightDrawerViewController:rightDrawer];
       
     /////
        
        UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:center];
        [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
        
        self.drawerController = [[MMDrawerController alloc]
                                 initWithCenterViewController:center
                                 leftDrawerViewController:leftDrawer];
        [self.drawerController setRestorationIdentifier:@"MMDrawer"];
        [self.drawerController setMaximumRightDrawerWidth:200.0];
        [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
        
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [self.window setRootViewController:self.drawerController];
        // Override point for customization after application launch.
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
    
        
         return YES;
         */
        
        
        
        /*
   
         UIViewController * leftSideDrawerViewController = [[MMExampleLeftSideDrawerViewController alloc] init];
         
        //UIViewController * centerViewController = [[MMExampleCenterTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
         
         UIViewController * rightSideDrawerViewController = [[MMExampleRightSideDrawerViewController alloc] init];
         
         UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:leftSideDrawerViewController];
         [navigationController setRestorationIdentifier:@"MMExampleCenterNavigationControllerRestorationKey"];
        
         self.drawerController = [[MMDrawerController alloc]
         initWithCenterViewController:navigationController
         leftDrawerViewController:leftSideDrawerViewController
         rightDrawerViewController:rightSideDrawerViewController];
         [self.drawerController setRestorationIdentifier:@"MMDrawer"];
         [self.drawerController setMaximumRightDrawerWidth:200.0];
         [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
         [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
         
         [self.drawerController
         setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         MMDrawerControllerDrawerVisualStateBlock block;
         block = [[MMExampleDrawerVisualStateManager sharedManager]
         drawerVisualStateBlockForDrawerSide:drawerSide];
         if(block){
         block(drawerController, drawerSide, percentVisible);
         }
         }];
         self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
         [self.window setRootViewController:self.drawerController];
         self.window.backgroundColor = [UIColor whiteColor];
         [self.window makeKeyAndVisible];
        
        */ // this was put in last
        
        // Override point for customization after application launch.

        
        
    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    
//    [LanguageSource saveData];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    
    //[[LanguageSource sharedInstance] saveContext];
    
//    [LanguageSource saveData];
}

@end
