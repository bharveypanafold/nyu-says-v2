//
//  SimpleVerb.m
//  ColorPad
//
//  Created by Panafold on 3/24/11.
//  Copyright 2011 Orange Petal, LLC. All rights reserved.
//

#import "SimpleVerb.h"

@interface SimpleVerb()
// place for private @property and method declarations

@property (strong) SimpleVerbCompletionBlock completionBlock;
@property (strong) SimpleVerbUpdateBlock updateBlock;
@property (strong) NSMutableData *buffer;
@property (strong) NSURLResponse *response;
@property (strong) NSURLConnection *conn;

@end

static NSOperationQueue *opQueue; // i'm not dealloc'ing this because i expect it to remain
                                    // for the life of the app
static NSMutableSet *activeConnectionSet;

@implementation SimpleVerb

@synthesize completionBlock,updateBlock, buffer, response, conn;

+ (void)initialize {
    if (self == [SimpleVerb class]) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            // Perform Class initialization here. - dispatch_once just to be damn sure 
            opQueue = [[NSOperationQueue alloc] init];
            [opQueue setName:@"SimpleVerb class-global operation queue."];
            activeConnectionSet = [[NSMutableSet alloc] initWithCapacity:1];
        });
    }
}

+ (void)setShouldSerializeRequests:(BOOL)serialize {
    if (serialize) {
        [opQueue setMaxConcurrentOperationCount:1];
    } else {
        // Note: NSOperationQueueDefaultMaxConcurrentOperationCount is a value determined by the system
        // dynamically. According to the docs. 
        [opQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
    }
}

+ (void)sendGET:(NSURL*)url completion:(SimpleVerbCompletionBlock)completion {
    SimpleVerbCompletionBlock completionBlock = [completion copy];
    
    [opQueue addOperationWithBlock:^{
                
        NSError *error = nil;
        NSURLResponse *response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] 
                                            returningResponse:&response 
                                                        error:&error];
        
        // in order to allow processing on the returned data to run on a second thread,
        // completionBlock is responsible for queueing stuff for the main thread
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
           
        if (error) {
            OPLog(@"SimpleVerb GET error: %@",error);
            completionBlock(nil, nil, error);
        } else {
            completionBlock(data, response, nil);
        }
        
        //        }];
    }];
}

+ (void)sendPOST:(NSURL*)url body:(NSString*)body completion:(SimpleVerbCompletionBlock)completion {
    
    SimpleVerbCompletionBlock completionBlock = [completion copy];
    
    [opQueue addOperationWithBlock:^{
        
        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
        [req setHTTPMethod:@"POST"];
        [req setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSError *error = nil;
        NSURLResponse *response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:req 
                                            returningResponse:&response 
                                                         error:&error];
        
        //        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        if (error) {
            OPLog(@"SimpleVerb POST error: %@",error);
            completionBlock(nil, nil, error);
        } else {
            completionBlock(data, response, nil);
        }
        //        }];
    }];
}

+ (void)sendGET:(NSURL *)url 
         update:(SimpleVerbUpdateBlock)update 
     completion:(SimpleVerbCompletionBlock)completion {
    
    OPLog(@"sendGet:update:completion: %@",url);
    
    SimpleVerb *verb = [[SimpleVerb alloc] init];
    
    verb.completionBlock = completion;
    verb.updateBlock = update;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        [activeConnectionSet addObject:verb];
        
        NSURLRequest *req = [NSURLRequest requestWithURL:url];
                
        verb.conn = [NSURLConnection connectionWithRequest:req delegate:verb];
        
    }];    
}



#pragma mark -
#pragma mark Connection

// 1. Deny redirects. THIS MAY BREAK FORCING CONNECTIONS TO SSL !
/*
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse {
    return nil;
}*/

// 2. deal with authentication challenges
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    
    OPLog(@"Protection Space: %@ %@",[protectionSpace host],[protectionSpace authenticationMethod]);
    
    return YES; // probably not a good default for deployed code.
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    OPLog(@"auth challenge: %@",[[challenge protectionSpace] authenticationMethod]);
    id challenger = [challenge sender];
    
    if ([[[challenge protectionSpace] authenticationMethod] isEqualToString:NSURLAuthenticationMethodClientCertificate]) {
                
        NSString *identPath = [[NSBundle mainBundle] pathForResource:@"JM" ofType:@"p12"];
        NSData *identData = [NSData dataWithContentsOfFile:identPath];
        
        OPLog(@"read %i bytes of p12 file.",[identData length]);
        OSStatus err;
        NSDictionary *opts = [NSDictionary dictionaryWithObjectsAndKeys:@"jason",kSecImportExportPassphrase, nil];
        CFArrayRef itemsRef = NULL;
        err = SecPKCS12Import((__bridge CFDataRef)identData,
                              (__bridge CFDictionaryRef)opts,
                              &itemsRef); // items should be CFArrayRef * (ie, pointer to a pointer)
        
        
        NSArray *items = CFBridgingRelease(itemsRef);
        
        if (err == noErr) {
            if ([items count] > 0) {
                
                NSDictionary *pkcs = [items objectAtIndex:0];
                
                SecIdentityRef ident = (SecIdentityRef)CFBridgingRetain([pkcs valueForKey:(__bridge NSString*)kSecImportItemIdentity]);
                NSArray *certs = [pkcs valueForKey:(__bridge NSString*)kSecImportItemCertChain];
                
                if ([certs count] > 0) {
                    
                    NSURLCredential *credential = [NSURLCredential credentialWithIdentity:ident
                                                                             certificates:nil
                                                                              persistence:NSURLCredentialPersistenceNone];
                    
                    //OPLog(@"credential: %@",credential);
                    //OPLog(@"Authentication Challenge response set: %@",[credential certificates]);
                    [challenger useCredential:credential
                   forAuthenticationChallenge:challenge];
                } else {
                    OPLog(@"no certs found");
                }
                
                free(&ident);
                
            } else {
                OPLog(@"no items returned from PKCS import. :(");
            }
        } else {
            OPLog(@"SecPKCS12Import error: %i",err);
        }
    } else {
        
        [challenger continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
}

// 3. not sent if protocol error is encountered.
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)rsp {
    OPLog(@"recv'd response: %@",rsp);
    self.response = rsp;
}

// 4. get bits
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    //OPLog(@"didReceiveData:");
    if (!self.buffer) {
        self.buffer = [NSMutableData dataWithCapacity:1024];
    }
    
    if (data) {
        [self.buffer appendData:data];
    }
    if (self.updateBlock)
        self.updateBlock(data,self.response.expectedContentLength);
}

// 5. deny caching
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil; // don't cache responses
}

// 6. All done. Or error'd.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    self.completionBlock(self.buffer, self.response, nil);
    
    OPLog(@"removing connection to active set: %@",connection);
    [activeConnectionSet removeObject:self];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    self.completionBlock(nil,self.response,error);
    
    OPLog(@"removing connection to active set: %@",connection);
    [activeConnectionSet removeObject:self];
}

// Monitor uploading
/*
- (void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite {
    
}*/

/*
-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if([challenge previousFailureCount] == 0) {
        NSURLProtectionSpace *protectionSpace = [challenge protectionSpace];
        NSString *authMethod = [protectionSpace authenticationMethod];
        if(authMethod == NSURLAuthenticationMethodServerTrust ) {
            NSLog(@"Verifying The Trust");
            [[challenge sender] useCredential:[NSURLCredential credentialForTrust:[protectionSpace serverTrust]] forAuthenticationChallenge:challenge];
        } else if(authMethod == NSURLAuthenticationMethodClientCertificate ) {
            NSLog(@"Trying Certificate");
            SecIdentityRef identity = [self getClientCertificate]; // Go get a SecIdentityRef
            NSURLCredential *newCredential = [NSURLCredential credentialWithIdentity :identity
                                                                         certificates:nil persistence:NSURLCredentialPersistenceNone];
            [[challenge sender] useCredential:newCredential forAuthenticationChallenge :challenge];
        }
    } else {
        NSLog(@"Auth Challenge Failed");
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
} */

/* // this is from Apple sample code
- (SecIdentityRef)getClientCertificate {
    SecIdentityRef identityApp = nil;
    NSString *thePath = [[NSBundle mainBundle] pathForResource:@"mycert" ofType :@"p12"];
    NSData *PKCS12Data = [[NSData alloc] initWithContentsOfFile:thePath];
    CFDataRef inPKCS12Data = (CFDataRef)PKCS12Data;
    CFStringRef password = CFSTR("<your cert="" pass="">");</your>
    const void *keys[] = { kSecImportExportPassphrase };
    const void *values[] = { password };
    CFDictionaryRef options = CFDictionaryCreate(NULL, keys, values, 1, NULL, NULL);
    CFArrayRef items = CFArrayCreate(NULL, 0, 0, NULL);
    OSStatus securityError = SecPKCS12Import(inPKCS12Data, options, &items);
    CFRelease(options);
    CFRelease(password);
    [PKCS12Data release];
    if(securityError == errSecSuccess) {
        NSLog(@"Success opening p12 certificate. Items: %d", CFArrayGetCount (items));
        CFDictionaryRef identityDict = CFArrayGetValueAtIndex(items, 0);
        identityApp = (SecIdentityRef)CFDictionaryGetValue(identityDict, kSecImportItemIdentity);
    } else {
        NSLog(@"Error opening Certificate.");
    }
    return identityApp;
}
*/


@end