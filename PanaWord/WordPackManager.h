//
//  WordPackManager.h
//  CaliforniaEnglish
//
//  Created by Guillaume on 3/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageWordPack.h"

@class PFViewController;

@interface WordPackManager : NSObject <NSXMLParserDelegate>

@property (nonatomic, retain) PFViewController* parent;
@property (nonatomic, retain) NSXMLParser* parser;
@property (nonatomic, retain) NSMutableDictionary* wordpacks;
@property (nonatomic, retain) NSMutableDictionary* locklist;
@property (nonatomic, retain) NSArray* sortedPackArray;

//these attributes are used by the parser to load the xml file into memory
@property (nonatomic, retain) LanguageWordPack* currentwordpack;

@property (nonatomic) BOOL addword;
@property (nonatomic) BOOL setpurchase;
@property (nonatomic) BOOL setfile;


- (id)initWithParent:(PFViewController*)parent;
- (void) initPacks;
- (LanguageWordPack*) getWordPackForWord:(NSString*)word;
- (NSArray*) getWordpacks;
- (BOOL) getLocked:(NSString*) wordpack;
- (void) unlockWordpack:(NSString*) wordpack;

NSInteger strSort(id str1, id str2, void* context);

@end
