//
//  DictionaryManager.h
//  CaliforniaEnglish
//
//  Created by Guillaume on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DictionaryManager : NSObject  <NSXMLParserDelegate>

@property (strong, nonatomic) NSMutableDictionary* dict;

- (NSString*) getDefinitionForWord:(NSString*)word;
- (void) setDefinition:(NSString*)def ForWord:(NSString*)word;

@end
