//
//  PagedArrayScrollView.h
//  PanaWord
//
//  Created by Panafold on 12/12/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PagedArrayScrollViewDelegate <NSObject>

- (UIView*)viewForIndex:(NSInteger)index;
- (NSUInteger)numberOfPages;
- (void)recycleViewAtIndex:(NSInteger)index;

@end

@interface PagedArrayScrollView : UIScrollView

@property (weak,nonatomic) id<PagedArrayScrollViewDelegate> pageDelegate;

- (void)animateCardFromIndex:(NSInteger)from toIndex:(NSInteger)to;
- (UIView*)viewForIndex:(NSInteger)index;

@end
