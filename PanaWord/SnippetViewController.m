//
//  SnippetViewController.m
//  PanaWord
//
//  Created by Panafold on 9/22/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "SnippetViewController.h"
#import "UIColor-Extras.h"
#import "ContentViewController.h"
#import "LanguageWord.h"
#import "LanguageFiling.h"
#import "RightViewController.h"


@implementation SnippetViewController

@synthesize filing;
@synthesize completion;
@synthesize web;
@synthesize filingShortText;
@synthesize filingText1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)saveAction:(id)sender {
    OPLog(@"saveAction:");
    [TestFlight passCheckpoint:@"SnippetSaved"];
    self.completion();
}

- (void)webAction:(id)sender {
    ContentViewController *vc = [[ContentViewController alloc] initWithNibName:nil bundle:nil];
    vc.filing = self.filing;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentModalViewController:nav
                            animated:YES];
    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    
    UIColor *c = [UIColor colorWithWhite:0.9 alpha:1.];
    UIView *v= [[UIView alloc] initWithFrame:CGRectMake(0., 0., 200., 160.)];

    if ([(NSString*)[filing.content valueForKey:@"type"] isEqualToString:@"image"]){
        //[self setContentSizeForViewInPopover:CGSizeMake(640, 480)];
    } else {
        //[self setContentSizeForViewInPopover:CGSizeMake(300, 400)];
    }
    self.view = v;
    self.view.backgroundColor = c;
    
    UIWebView *wv = [[UIWebView alloc] initWithFrame:CGRectInset(self.view.bounds, 10., 10.)];
    wv.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    wv.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.];
    wv.dataDetectorTypes = UIDataDetectorTypeNone;
    ((UIScrollView*)[[wv subviews] objectAtIndex:0]).bounces= NO;
    self.web = wv;
    
    [self.view addSubview:self.web];
}

//#define kLeadingStringLength 100
//#define kTrailingStringLength 200
#define kLeadingStringLength 10
#define kTrailingStringLength 20

- (NSString*)subString:(NSString*)string centeredAround:(NSString*)center {
    
    NSString *str = string;
 
    NSRange foundRange = [string rangeOfString:center
                                       options:NSCaseInsensitiveSearch
                                         range:NSMakeRange(0, 4) // (0, [string length])
                                        locale:nil];
    
    if (NSNotFound != foundRange.location) {
        
        //OPLog(@"substring found");
        NSUInteger start = 0;
        NSUInteger stop = kLeadingStringLength + kTrailingStringLength;
        
        if (foundRange.location > kLeadingStringLength) {
            start = foundRange.location - kLeadingStringLength;
        }
        
        if (start+stop > [string length]-1) {
            stop = ([string length]-1) - start;
        }
        
        //OPLog(@"%i [%i %i] %i",0,start,stop,[string length]);
        
        str = [string substringWithRange:NSMakeRange(start, stop)];
       
    } else {
        OPLog(@"substring NOT found");
    }
    
    
    return str;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.filing.content valueForKey:@"page"]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow20x20"]
                                                                                  style:UIBarButtonItemStyleBordered
                                                                                 target:self
                                                                                 action:@selector(webAction:)];
   
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    NSString *term = [self.filing.word.term stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    self.title = term;
    
    NSString *filingText = [self.filing.content valueForKey:@"description"];
    
    
    filingText = [self subString:filingText centeredAround:term];
     NSLog(@"FilingText: %@", filingText);
    filingText1 = filingText;
   
    filingShortText = filingText;
    filingText = [filingText stringByReplacingOccurrencesOfString:term
                                                       withString:[NSString stringWithFormat:@"<span style='background-color:#ffb6c1;'>%@</span> ",term]];
    
    filingText = [filingText stringByReplacingOccurrencesOfString:[term uppercaseString]
                                                       withString:[NSString stringWithFormat:@"<span style='background-color:#ffb6c1;'>%@</span> ",[term uppercaseString]]];
    NSString* html;
    if ([(NSString*)[filing.content valueForKey:@"type"] isEqualToString:@"image"]){
        html = [NSString stringWithFormat:@"<html><head></head><body style='background-color:#e5e5e5;'><img style='width: 480px;' src='%@' /></body></html>", [self.filing.content valueForKey:@"page"]];   
        NSLog(@"Image: %@",  [self.filing.content valueForKey:@"page"]);
        
    } else {
        html = [NSString stringWithFormat:@"<html><head></head><body style='background-color:#e5e5e5; font-size: 14pt; font-family: arial;'>%@</body></html>",filingText];        
    }
    
    [self.web loadHTMLString:html
                     baseURL:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}


- (void)getSnippet {
    if ([self.filing.content valueForKey:@"page"]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow20x20"]
                                                                                  style:UIBarButtonItemStyleBordered
                                                                                 target:self
                                                                                 action:@selector(webAction:)];
        
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    NSString *term = [self.filing.word.term stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    self.title = term;
    
    NSString *filingText = [self.filing.content valueForKey:@"description"];
        NSLog(@"THISSFIRST........: %@", filingText);
    
    filingText = [self subString:filingText centeredAround:term];
    NSLog(@"THISS........: %@", filingText);
    filingText1 = filingText;
    
    filingShortText = filingText;
    filingText = [filingText stringByReplacingOccurrencesOfString:term
                                                       withString:[NSString stringWithFormat:@"<span style='background-color:#ffb6c1;'>%@</span> ",term]];
    
    filingText = [filingText stringByReplacingOccurrencesOfString:[term uppercaseString]
                                                       withString:[NSString stringWithFormat:@"<span style='background-color:#ffb6c1;'>%@</span> ",[term uppercaseString]]];
    NSString* html;
    if ([(NSString*)[filing.content valueForKey:@"type"] isEqualToString:@"image"]){
        html = [NSString stringWithFormat:@"<html><head></head><body style='background-color:#e5e5e5;'><img style='width: 480px;' src='%@' /></body></html>", [self.filing.content valueForKey:@"page"]];
        NSLog(@"Image: %@",  [self.filing.content valueForKey:@"page"]);
        
    } else {
        html = [NSString stringWithFormat:@"<html><head></head><body style='background-color:#e5e5e5; font-size: 14pt; font-family: arial;'>%@</body></html>",filingText];
    }
    
    [self.web loadHTMLString:html
                     baseURL:nil];
     
}


@end
