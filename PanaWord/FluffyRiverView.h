//
//  FluffyRiverView.h
//  PanaWord
//
//  Created by Panafold on 9/18/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FluffyRiverView : UIView

@property (strong,nonatomic,readonly) CAEmitterLayer *emitterLayer;

@end
