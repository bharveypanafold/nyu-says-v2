//
//  PFAppDelegate.h
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MMDrawerController/MMDrawerController.h>

#import "MMDrawerController.h"

//#import "MMExampleDrawerVisualStateManager.h"

@class PFViewController;

@interface PFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) PFViewController *viewController;
@property (nonatomic,strong) MMDrawerController * drawerController;
@property (strong, nonatomic) NSMutableArray *filingTable; 
@property (strong, nonatomic) NSMutableArray *arrayFilings;

@end
