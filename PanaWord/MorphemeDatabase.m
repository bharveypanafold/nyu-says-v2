//
//  MorphemeDatabase.m
//  EnglishDiscovery
//
//  Created by Guillaume on 5/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MorphemeDatabase.h"

@implementation MorphemeDatabase
@synthesize morphemes;

- (id) init{
    self=[super init];
    
    if (self){
        self.morphemes= [[NSMutableArray alloc] initWithCapacity:50];
    }
    
    return self;
}

- (BOOL) morphemeInDatabase:(NSString*)m{
    LanguageWord* morpheme;
    for (morpheme in self.morphemes){
        if ([morpheme.name isEqualToString:m])
            return true;
    }

    return false;
}

- (LanguageWord*) getMorpheme:(NSString*)m{
    LanguageWord* l;

    for (l in self.morphemes){
        if ([l.name isEqualToString:m])
            return l;
    }
    
    return nil;
}

- (void) addMorpheme:(LanguageWord*)l{
    if (![self morphemeInDatabase:l.name]){
        [self.morphemes addObject:l];
    }
}

- (NSString*) getTypeForMorpheme:(NSString*)m{
    return [self getMorpheme:m].type;
}

- (NSString*) getDefinitionForMorpheme:(NSString*)m{
    return [self getMorpheme:m].def;
}

@end
