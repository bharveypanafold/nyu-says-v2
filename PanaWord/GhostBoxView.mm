//
//  GhostBoxView.m
//  PanaFlow
//
//  Created by Panafold on 9/13/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "GhostBoxView.h"
#import "Box2D.h"
#import "BoxWorld.h"
#import "UIColor-Extras.h"

@interface BoxWorld(Private)
- (b2World*)worldPtr;
@end

@implementation GhostBoxView

@synthesize world;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return self;
}

- (void)dealloc {
    OPLog(@"dealloc");
}


#define kForceNormalizationFactor 60.0

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    /*// mark the center of the view
    UIGraphicsPushContext(context);
    CGContextTranslateCTM(context,
                          rect.size.width/2.0,
                          rect.size.height/2.0);
    
    CGContextStrokeEllipseInRect(context, CGRectMake(-2., -2., 4., 4.));
    [[UIColor orangeColor] set];
    CGContextFillEllipseInRect(context, CGRectMake(-2., -2., 4., 4.));
    UIGraphicsPopContext();
    
    CGContextTranslateCTM(context,
                          -rect.size.width/2.0,
                          -rect.size.height/2.0);
     */
    
    NSAssert(self.world,@"You forgot to set the world pointer.");
    
    b2World *bWorld = [self.world worldPtr];
    b2Body *body = bWorld->GetBodyList();
    CGFloat s = kBoxWorldPixelScale;
    
    [[UIColor blackColor] set];
    
    while (NULL != body) {
        UIGraphicsPushContext(context);
        
        
        if (b2_dynamicBody == body->GetType()) {
            [[UIColor redColor] set];
        } else {
            [[UIColor blueColor] set];
        }
        
        b2Vec2 p = body->GetPosition();
        b2Vec2 v = body->GetLinearVelocity();
        //CGFloat a = body->GetAngle();
        
        CGFloat x = p.x*s;
        CGFloat y = p.y*s;
        
        
        CGContextTranslateCTM(context, x, y);
        CGContextFillEllipseInRect(context, CGRectMake(-2., -2., 4., 4.));
        
        b2Fixture *fixture = body->GetFixtureList();
        CGMutablePathRef path = CGPathCreateMutable();
        while (fixture != NULL) {
            
            //b2Shape *shape = fixture->GetShape();
            //b2PolygonShape *shape = (b2PolygonShape*)fixture->GetShape();
            const b2Transform& xf = fixture->GetBody()->GetTransform();
            
            switch (fixture->GetType())
            {
                case b2Shape::e_circle:
                {
                    b2CircleShape* circle = (b2CircleShape*)fixture->GetShape();
                    
                    b2Vec2 center = b2Mul(xf, circle->m_p);
                    float32 radius = circle->m_radius;
                    
                    CGRect c = CGRectMake(center.x - radius,
                                          center.y - radius,
                                          radius*2.0,
                                          radius*2.0);
                    
                    CGPathAddEllipseInRect(path, NULL, c);
                    //m_debugDraw->DrawCircle(center, radius, color);
                }
                    break;
                    
                case b2Shape::e_polygon:
                {
                    b2PolygonShape* poly = (b2PolygonShape*)fixture->GetShape();
                    int32 vertexCount = poly->m_vertexCount;
                    b2Assert(vertexCount <= b2_maxPolygonVertices);
                    
                    for (int k = 0; k < vertexCount; k++) {
                        b2Vec2 vv = b2Mul(xf, poly->m_vertices[k]);
                        //b2Vec2 vv = body->GetWorldVector(shape->GetVertex(k));
                        if (0==k) {
                            CGPathMoveToPoint(path, NULL, vv.x*s, vv.y*s);
                        } else {
                            CGPathAddLineToPoint(path, NULL, vv.x*s, vv.y*s);
                        }
                    }
                    
                    //m_debugDraw->DrawPolygon(vertices, vertexCount, color);
                }
                    break;
                    
                default:
                    break;
            }
            
            fixture = fixture->GetNext();
            CGPathCloseSubpath(path);
        }
        
        CGContextAddPath(context, path);
        CGPathRelease(path);
        
        CGContextStrokePath(context);
        CGContextTranslateCTM(context, -x, -y);
        
        body = body->GetNext();
        
        UIGraphicsPopContext();
    }


    b2Joint *joint = bWorld->GetJointList();
    [[UIColor lightGrayColor] set];
    
    //CGMutablePathRef path = CGPathCreateMutable();
    while (NULL != joint) {
        UIGraphicsPushContext(context);
        
        CGContextSetLineWidth(context, 2.0);
        
        if (e_mouseJoint != joint->GetType()) {
            b2Vec2 anchorA = joint->GetAnchorA();
            b2Vec2 anchorB = joint->GetAnchorB();
            
            CGFloat force = joint->GetReactionForce(60.0).Normalize() / kForceNormalizationFactor;
            
            CGPoint points[2];
            
            points[0].x = s*anchorA.x;
            points[0].y = s*anchorA.y;
            points[1].x = s*anchorB.x;
            points[1].y = s*anchorB.y;
            
            // Set the line with to represent the reaction force - fun, but not our goal
            //CGFloat sWidth = fmaxf(1.0, 5.0*force);
            //CGContextSetLineWidth(context, sWidth);
            [[UIColor colorWithWhite:(force) alpha:1.0] set];
            CGContextStrokeLineSegments(context, &points[0], 2);
            
            //CGPathMoveToPoint(path, NULL, s*anchorA.x, s*anchorA.y);
            //CGPathAddLineToPoint(path, NULL, s*anchorB.x, s*anchorB.y);
        }
        joint = joint->GetNext();
    }
    //CGContextAddPath(context, path);
    //CGPathRelease(path);
    
    //CGContextStrokePath(context);
}


@end
