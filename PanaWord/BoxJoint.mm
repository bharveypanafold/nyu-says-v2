//
//  BoxJoint.m
//  PanaWord
//
//  Created by Panafold on 9/12/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "BoxJoint.h"
#import "Box2D.h"
#import "BoxBody.h"
#import "BoxWorld.h"

@interface BoxBody(PrivateWorldMethods)
- (void)setBodyPtr:(b2Body*)bptr;
- (b2BodyDef*)b2Definition;
- (void)insertFixtures;
- (b2Body*)b2Ptr;
@end

@implementation BoxJoint

@synthesize bodyA,bodyB,allowSelfCollision;

- (b2JointDef*)b2Definition {
    return NULL;
}

- (void)setJointPtr:(b2Joint*)bptr {
    _jointRef = bptr;
}

- (b2Joint*)b2Ptr {
    return (b2Joint*)_jointRef;
}


@end


@implementation BoxMouseJoint

@dynamic frequency,maxForce,targetPoint,dampingRatio;

- (id)init {
    self = [super init];
    
    if (self) {
     
        _jointDef = new b2MouseJointDef;
    }
    return self;
}

- (void)dealloc {
    NSAssert(NULL == _jointRef, @"joint ptr is NOT NULL. body is probably still in the world.");
    delete (b2MouseJoint*)_jointRef;
}

- (b2MouseJointDef*)b2Definition {
    
    // update BoxJoint properties in the definition structure
    b2MouseJointDef* def = (b2MouseJointDef*)_jointDef;
    
    def->bodyA = [self.bodyA b2Ptr];
    def->bodyB = [self.bodyB b2Ptr];
    def->collideConnected = self.allowSelfCollision;
    
    return (b2MouseJointDef*)_jointDef;
}

- (CGFloat)frequency {
    if (NULL != _jointRef) {
        return ((b2MouseJoint*)_jointRef)->GetFrequency();
    } else {
        return ((b2MouseJointDef*)_jointDef)->frequencyHz;
    }
}

- (void)setFrequency:(CGFloat)frequency {
    if (NULL != _jointRef) {
        ((b2MouseJoint*)_jointRef)->SetFrequency(frequency);
    } else {
        ((b2MouseJointDef*)_jointDef)->frequencyHz = frequency;
    }
}

- (CGFloat)dampingRatio {
    if (NULL != _jointRef) {
        return ((b2MouseJoint*)_jointRef)->GetDampingRatio();
    } else {
        return ((b2MouseJointDef*)_jointDef)->dampingRatio;
    }
}

- (void)setDampingRatio:(CGFloat)dampingRatio {
    if (NULL != _jointRef) {
        ((b2MouseJoint*)_jointRef)->SetDampingRatio(dampingRatio);
    } else {
        ((b2MouseJointDef*)_jointDef)->dampingRatio = dampingRatio;
    }
}

- (CGFloat)maxForce {
    if (NULL != _jointRef) {
        return ((b2MouseJoint*)_jointRef)->GetMaxForce();
    } else {
        return ((b2MouseJointDef*)_jointDef)->maxForce;
    }
}

- (void)setMaxForce:(CGFloat)maxForce {
    if (NULL != _jointRef) {
        ((b2MouseJoint*)_jointRef)->SetMaxForce(maxForce);
    } else {
        ((b2MouseJointDef*)_jointDef)->maxForce = maxForce;
    }
}

- (CGPoint)targetPoint {
    b2Vec2 bp;
    
    if (NULL != _jointRef) {
        bp = ((b2MouseJoint*)_jointRef)->GetTarget();
    } else {
        bp = ((b2MouseJointDef*)_jointDef)->target;
    }
    
    return CGPointMake(bp.x*kBoxWorldPixelScale,
                       bp.y*kBoxWorldPixelScale);
}

- (void)setTargetPoint:(CGPoint)targetPoint {
    if (NULL != _jointRef) {
        ((b2MouseJoint*)_jointRef)->SetTarget(b2Vec2(targetPoint.x/kBoxWorldPixelScale,
                                                     targetPoint.y/kBoxWorldPixelScale));
    } else {
        ((b2MouseJointDef*)_jointDef)->target = b2Vec2(targetPoint.x/kBoxWorldPixelScale,
                                                       targetPoint.y/kBoxWorldPixelScale);
    }
}

@end


@implementation BoxDistanceJoint

@dynamic localAnchorA,localAnchorB,frequency,dampingRatio,length;

- (id)init {
    self = [super init];
    
    if (self) {
        
        _jointDef = new b2DistanceJointDef;
        //OPLog(@"+ distanceJoint");
    }
    return self;
}

- (void)dealloc {
    NSAssert(NULL == _jointRef, @"joint ptr is NOT NULL. body is probably still in the world.");
    delete (b2DistanceJointDef*)_jointRef;
    
    //OPLog(@"- distanceJoint");
}

- (b2DistanceJointDef*)b2Definition {
    
    // update BoxJoint properties in the definition structure
    b2DistanceJointDef* def = (b2DistanceJointDef*)_jointDef;
    
    def->bodyA = [self.bodyA b2Ptr];
    def->bodyB = [self.bodyB b2Ptr];
    def->collideConnected = self.allowSelfCollision;
    
    return (b2DistanceJointDef*)_jointDef;
}

- (void)setLength:(CGFloat)length {
    if (NULL != _jointRef) {
        ((b2DistanceJoint*)_jointRef)->SetLength(length/kBoxWorldPixelScale);
    } else {
        ((b2DistanceJointDef*)_jointDef)->length = length/kBoxWorldPixelScale;
    }
}

- (CGFloat)length {
    if (NULL != _jointRef) {
        return ((b2DistanceJoint*)_jointRef)->GetLength()*kBoxWorldPixelScale;
    } else {
        return ((b2DistanceJointDef*)_jointDef)->length*kBoxWorldPixelScale;
    }
}
/*
- (void)setLocalAnchorA:(CGPoint)localAnchorA {
    if (NULL != _jointRef) {
        ((b2DistanceJoint*)_jointRef)->;
    } else {
        ((b2DistanceJointDef*)_jointDef)->length = length;
    }
}
*/
- (CGPoint)localAnchorA {
    b2Vec2 p;
    if (NULL != _jointRef) {
        p = ((b2DistanceJoint*)_jointRef)->GetAnchorA();
    } else {
        p = ((b2DistanceJointDef*)_jointDef)->localAnchorA;
    }
    return CGPointMake(p.x*kBoxWorldPixelScale, p.y*kBoxWorldPixelScale);
}
/*
- (void)setLocalAnchorB:(CGPoint)localAnchorB {
    
}
*/
- (CGPoint)localAnchorB {
    b2Vec2 p;
    if (NULL != _jointRef) {
        p = ((b2DistanceJoint*)_jointRef)->GetAnchorB();
    } else {
        p = ((b2DistanceJointDef*)_jointDef)->localAnchorB;
    }
    return CGPointMake(p.x*kBoxWorldPixelScale, p.y*kBoxWorldPixelScale);
}

- (void)setFrequency:(CGFloat)frequency {
    if (NULL != _jointRef) {
        ((b2DistanceJoint*)_jointRef)->SetFrequency(frequency);
    } else {
        ((b2DistanceJointDef*)_jointDef)->frequencyHz = frequency;
    }
}

- (CGFloat)frequency {
    if (NULL != _jointRef) {
        return ((b2DistanceJoint*)_jointRef)->GetFrequency();
    } else {
        return ((b2DistanceJointDef*)_jointDef)->frequencyHz;
    }
}

- (void)setDampingRatio:(CGFloat)dampingRatio {
    if (NULL != _jointRef) {
        ((b2DistanceJoint*)_jointRef)->SetDampingRatio(dampingRatio);
    } else {
        ((b2DistanceJointDef*)_jointDef)->dampingRatio = dampingRatio;
    }
}

- (CGFloat)dampingRatio {
    if (NULL != _jointRef) {
        return ((b2DistanceJoint*)_jointRef)->GetDampingRatio();
    } else {
        return ((b2DistanceJointDef*)_jointDef)->dampingRatio;
    }
}

@end
