//
//  PoemWordsDatabase.m
//  PowerOfWords
//
//  Created by Guillaume on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PoemWordsDatabase.h"

@implementation PoemWordsDatabase
@synthesize poemwords;
@synthesize lowfreqpoemwords, highfreqpoemwords;

- (id) init{
    self=[super init];
    
    if (self){
        self.poemwords= [[NSMutableArray alloc] initWithCapacity:2000];
        self.lowfreqpoemwords= [[NSMutableArray alloc] initWithCapacity:2000];
        self.highfreqpoemwords= [[NSMutableArray alloc] initWithCapacity:2000];
    }
    
    return self;
}

- (NSMutableArray*) getRandomPoemWordsNumLowFreq:(int)low andNumHighFreq:(int)high{
    //fixed array for now
    
    NSMutableArray* results= [[NSMutableArray alloc] initWithCapacity:high+low];
    
    for (int i=0; i<low;i++){
        int randIndex= arc4random() % [self.lowfreqpoemwords count];
        [results addObject:[self.lowfreqpoemwords objectAtIndex:randIndex]];
    }

    for (int i=0; i<high;i++){
        int randIndex= arc4random() % [self.highfreqpoemwords count];
        [results addObject:[self.highfreqpoemwords objectAtIndex:randIndex]];
    }
    
    return results;
}

- (void) addWord:(NSString*)w forFrequency:(NSString*)freq{
    [self.poemwords addObject:w];
    
    if ([freq isEqualToString:@"low"]){
        [self.lowfreqpoemwords addObject:w];
    } else if ([freq isEqualToString:@"high"]){
        [self.highfreqpoemwords addObject:w]; 
    }
}
@end
