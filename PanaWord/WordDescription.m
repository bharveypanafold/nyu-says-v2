//
//  WordDescription.m
//  EnglishDiscovery
//
//  Created by Guillaume on 6/4/12.
//  Copyright (c) 2012 Panafold. All rights reserved.
//

#import "WordDescription.h"

@implementation WordDescription
@synthesize affixes, rewriteRules, fullword, def;

- (id) init{
    self=[super init];
    if (self){
        self.rewriteRules= [[NSMutableArray alloc] initWithCapacity:3];
        self.affixes= [[NSMutableArray alloc] initWithCapacity:3];
    }
    return self;
}

- (void) addAffix:(NSString*)affix{
  //  NSLog(@"affixxxxxxxxxxxxxxx: %@", affix);
    [self.affixes addObject:affix];
}

- (void) addDef:(NSString*)def{
    [self.def addObject:def];
}


- (void) addRewriteRuleForAffixA:(NSString*)affixA andAffixB:(NSString*)affixB withResult:(NSString*)finalStr{
    RewriteRule* rule= [[RewriteRule alloc] initWithAffix1:affixA andAffix2:affixB forResult:finalStr];
    [self.rewriteRules addObject:rule];
}

- (void) addRewriteRule:(RewriteRule*)r{
    [self.rewriteRules addObject:r];
//    NSLog(@"Added rewrite rule for %@ + %@ = %@", r.affix1, r.affix2, r.result);
//    NSLog(@"%u rules total for %@", [self.rewriteRules count], self.fullword);

}

- (BOOL) existsRewriteRuleFor:(NSString*)affix1 and:(NSString*)affix2{
    for (RewriteRule* r in self.rewriteRules){
        if ([r.affix1 isEqualToString:affix1] && [r.affix2 isEqualToString:affix2])
            return YES;
        if ([r.affix2 isEqualToString:affix1] && [r.affix1 isEqualToString:affix2])
            return YES;
    }
    
    return NO;
}

- (RewriteRule*) rewriteRuleFor:(NSString*)affix1 and:(NSString*)affix2{
    for (RewriteRule* r in self.rewriteRules){
        if ([r.affix1 isEqualToString:affix1] && [r.affix2 isEqualToString:affix2])
            return r;
        if ([r.affix2 isEqualToString:affix1] && [r.affix1 isEqualToString:affix2])
            return r;
    }
    
    return nil;    
}

- (BOOL) hasAffix:(NSString*)a{
    for (NSString* s in self.affixes){
        if ([s isEqualToString:a])
            return YES;
    }
    return NO;
}

- (BOOL) stringIsRewriteResult:(NSString*)str{
    for (RewriteRule* r in self.rewriteRules){
        if ([r.result isEqualToString:str])
            return YES;
    }
    return NO;

}

@end
 
 
