//
//  PFViewController.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "PFViewController.h"
#import "PackPickerController.h"
#import "ContentViewController.h"
#import "UIColor-Extras.h"

#import "LanguageSource.h"
#import "LanguageWordPack.h"
#import "LanguageWord.h"
#import "LanguageFiling.h"

#import <QuartzCore/QuartzCore.h>
//#import <MessageUI/MessageUI.h>

#import "BoxWorld.h"
#import "BoxShape.h"
#import "BoxBody.h"

#import "FluffyRiverView.h"
#import "SnippetViewController.h"
#import "EmitterDebugView.h"
#import "SimpleVerb.h"
#import "JSONKit.h"
#import "NSData+HMAC.h"
#import "GTMNSDictionary+URLArguments.h"

#import "AttractorView.h"
#import "FilingView.h"

#import "AboutViewController.h"
#import "SettingsViewController.h"
#import "WordpackPickerViewController.h"

#import "PackPickerTableController.h"
#import "MenuController.h"

#import "ContextCardViewController.h"
#import "CardView.h"

#import "TestFlight.h"

// Attractor states: closed, waiting on network, open

@implementation PFViewController

@synthesize wordPack;
@synthesize displayLink;
@synthesize world;
@synthesize documentOptionsPanel;
@synthesize wallBodies;
@synthesize mouseAnchorBody;
@synthesize filingPopoverController;
@synthesize fluffyView;
@synthesize menuView;
@synthesize attractorViews;
@synthesize addAttractorView;
@synthesize selectedAttractor;
@synthesize embeddedViewControllerBackdropView;
@synthesize workQueue;
@synthesize snippetController;
@synthesize wordPackStr;

@synthesize wordpacks, settings, info, plusButton, mailScreenshot;
@synthesize shouldShowPlusButton;
@synthesize popoverController;
@synthesize settingsDict;

@synthesize background;
@synthesize dictionarymanager;

@synthesize tableviewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        OPLogMethod;
        
        //Initiate content database from xml files
        //  morphemeLoader= [[MorphemeLoader alloc] init];
        //  morphemeDB=[morphemeLoader getMorphemeDB];
        self.dictionarymanager= [[DictionaryManager alloc] init];
    
        wordpackLoader= [[WordpackLoader alloc]initWithDictManager:dictionarymanager];
        wordpackDB= [wordpackLoader getWordpackDB];
        
        //  poemwordsLoader= [[PoemWordsLoader alloc] init];
        //  poemwordsDB= [poemwordsLoader getPoemWordsDB];
        
        //init audio resource
        //   NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"magnet-merge" withExtension:@"aiff"];
        //   player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:NULL];
        //  [player prepareToPlay];
    }
    
    return self;
}

//This method sets up the image background for the app
- (void) initBackground{
    self.background= [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.view insertSubview:self.background atIndex:0];
}

//This method sets the background to a given image
- (void) setBackgroundToImage:(NSString*)path {
    [self.background setImage:[UIImage imageNamed:path]];
    self.background.contentMode= UIViewContentModeTopLeft;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setup functions

//This method setups the menu bar at the bottom of the main view
- (void) setupMenuBar{
    //Defining elements for the menu bar
    self.wordpacks = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.wordpacks setImage:[UIImage imageNamed:@"wordpack-icon"] forState:UIControlStateNormal];
    self.wordpacks.frame = CGRectMake(0., 0., 40., 40.);
    self.wordpacks.tag= WORDPACK_BUTTON_MENUBAR;
    [self.wordpacks addTarget:self
                       action:@selector(showPopover:)
             forControlEvents:UIControlEventTouchUpInside];
    
    self.settings = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.settings setImage:[UIImage imageNamed:@"settings-icon"] forState:UIControlStateNormal];
    self.settings.frame = CGRectMake(0., 0., 40., 40.);
    self.settings.tag= SETTINGS_BUTTON_MENUBAR;
    [self.settings addTarget:self
                      action:@selector(showPopover:)
            forControlEvents:UIControlEventTouchUpInside];
    
    self.info = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.info setImage:[UIImage imageNamed:@"info-icon"] forState:UIControlStateNormal];
    self.info.frame = CGRectMake(0., 0., 40., 40.);
    self.info.tag= INFO_BUTTON_MENUBAR;
    [self.info addTarget:self
                  action:@selector(showPopover:)
        forControlEvents:UIControlEventTouchUpInside];
    
    //   self.tweetScreenshot = [UIButton buttonWithType:UIButtonTypeCustom];
    //   [self.tweetScreenshot setImage:[UIImage imageNamed:@"tweet-icon"] forState:UIControlStateNormal];
    //   self.tweetScreenshot.frame = CGRectMake(0., 0., 40., 40.);
    //   self.tweetScreenshot.tag= TWEET_BUTTON_MENUBAR;
    //   [self.tweetScreenshot addTarget:self
    //                            action:@selector(tweetsheetScreenshot)
    //                  forControlEvents:UIControlEventTouchUpInside];
    
    /* self.mailScreenshot = [UIButton buttonWithType:UIButtonTypeCustom];
     [self.mailScreenshot setImage:[UIImage imageNamed:@"email-icon"] forState:UIControlStateNormal];
     self.mailScreenshot.frame = CGRectMake(0., 0., 40., 40.);
     //    self.mailScreenshot.tag= TWEET_BUTTON_MENUBAR;
     [self.mailScreenshot addTarget:self
     action:@selector(mailsheetScreenshot)
     forControlEvents:UIControlEventTouchUpInside];
     */
    
    if (self.shouldShowPlusButton){
        self.plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.plusButton setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
        self.plusButton.frame = CGRectMake(0., 0., 40., 40.);
        self.plusButton.tag= ADD_NEW_ATTRACTOR;
        [self.plusButton addTarget:self
                            action:@selector(showNewAttractor)
                  forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:self
                                                                           action:nil];
    
    UIBarButtonItem *wordpacksB = [[UIBarButtonItem alloc] initWithCustomView:self.wordpacks];
    UIBarButtonItem *infoB = [[UIBarButtonItem alloc] initWithCustomView:self.info];
    //   UIBarButtonItem *tweetScreenshotB = [[UIBarButtonItem alloc] initWithCustomView:self.tweetScreenshot];
    UIBarButtonItem *mailScreenshotB = [[UIBarButtonItem alloc] initWithCustomView:self.mailScreenshot];
    UIBarButtonItem *plus= nil;
    
    if (self.shouldShowPlusButton)
        plus=[[UIBarButtonItem alloc] initWithCustomView:plusButton];
    
    NSMutableArray* buttons= [[NSMutableArray alloc] initWithCapacity:5];
    
    [buttons addObject:wordpacksB];
    [buttons addObject:infoB];
    
    //    if ([TWTweetComposeViewController canSendTweet])
    //        [buttons addObject:tweetScreenshotB];
    if ([MFMailComposeViewController canSendMail])
        [buttons addObject:mailScreenshotB];
    
    if (self.shouldShowPlusButton)
        [buttons addObject:plusButton];
    
    self.toolbarItems = buttons;
}

- (void) showAddAttractorButton{
    self.shouldShowPlusButton=true;
    [self setupMenuBar];
}

- (NSArray*) getWordpackList{
    return [wordpackDB wordpackTitles];
}

- (void)writeToSettingsValue:(NSObject*)value forKey:(NSString*)key{
    [self.settingsDict setObject:value forKey:key];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:SETTINGS_FILE_NAME];
    
    [self.settingsDict writeToFile:appFile atomically:YES];
}

- (void)readSettingsFile{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:SETTINGS_FILE_NAME];
    
    NSMutableDictionary* settingsDictFromFile= [[NSMutableDictionary alloc] initWithContentsOfFile:appFile];
    
    self.settingsDict= settingsDictFromFile;
    if (self.settingsDict==nil){
        self.settingsDict= [[NSMutableDictionary alloc] initWithCapacity:2];
        [self.settingsDict setObject:[[NSNumber alloc] initWithInt:0] forKey:NUM_LAUNCHES_SETTINGS_KEY];
        [self.settingsDict setObject:[[NSNumber alloc] initWithInt:1] forKey:REMIND_TO_REVIEW_SETTINGS_KEY];
    }
    
    NSLog(@"Read settings.");
    NSLog(@"%@", self.settingsDict);
}

//<--
//- (void) ratePromptCheck{
//    int numLaunches=[[self.settingsDict valueForKey:NUM_LAUNCHES_SETTINGS_KEY] intValue];
//    NSLog(@"Num times launched so far: %i \n", numLaunches);

//    if ([self.settingsDict valueForKey:REMIND_TO_REVIEW_SETTINGS_KEY]==[NSNumber numberWithInt:1] && (numLaunches==RATE_FIRST_REMINDER || numLaunches==RATE_SECOND_REMINDER)){
//        NSString* title;
//        if (numLaunches==RATE_SECOND_REMINDER){
//            title=@"Never bug me again!";
//        } else {
//           title=@"Later!";
//        }

//        UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"Love from Panafold"
//                                                       message:@"Thanks for using our app! If you like it, please rate us 5 stars on the app store!"
//                                                      delegate:self
//                                             cancelButtonTitle:title
//                                             otherButtonTitles:@"Rate it!", nil];
//        [alert setTag:UIALERT_RATE_US];
//        [alert show];
//    }

//    numLaunches++;
//    [self writeToSettingsValue:[NSNumber numberWithInt:numLaunches] forKey:NUM_LAUNCHES_SETTINGS_KEY];
//}

//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if ([alertView tag]==UIALERT_RATE_US && buttonIndex==1){
//        [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=541302696"]];
//the user will likely rate us since we opened the link, so we never remind again
//        [self writeToSettingsValue:[[NSNumber alloc] initWithInt:0] forKey:REMIND_TO_REVIEW_SETTINGS_KEY];
//    } else if ([alertView tag]==UIALERT_FULL_VERSION && buttonIndex==1){
//        [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:@"itms-apps://itunes.apple.com/us/app/power-words-advanced-english/id541302696?mt=8"]];
//   }

//}

#pragma mark - C Functions

static inline CGPoint cartesianPointFromPolar(CGFloat radius, CGFloat angle) {
    return CGPointMake(cosf(angle)*radius, sinf(angle)*radius);
}

static inline CGAffineTransform transformForPositionRotation(CGPoint pos, CGFloat rotation) {
    CGAffineTransform t = CGAffineTransformMakeTranslation(pos.x, pos.y);
    CGAffineTransform r = CGAffineTransformMakeRotation(rotation);
    return CGAffineTransformConcat(r, t);
}

static inline CGFloat CGPointDistanceToPoint(CGPoint p1, CGPoint p2) {
    return sqrtf( (p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y) );
}

#pragma mark - Snippet

//Called when one attractor's filing is tapped.
//Opens the preview snippet
- (void)showSnippetForFiling:(FilingView*)filingView {
    [TestFlight passCheckpoint:@"ShowSnippetForFiling"];
    
    self.snippetController = [[SnippetViewController alloc] initWithNibName:nil bundle:nil];
    //We need a nav controller to show the top bar with "expand" button
    UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:self.snippetController];
    navc.modalPresentationStyle = UIModalPresentationPageSheet;
    UIPopoverController *pop = [[UIPopoverController alloc] initWithContentViewController:navc];
    pop.delegate = self;
    self.filingPopoverController = pop;
    
    self.snippetController.filing = filingView.filing;
    CGSize suggestedSize;
    
    if ([[filingView.filing.content objectForKey:@"type"] isEqualToString:@"image"]){
        suggestedSize= CGSizeMake(500., 400);
    } else {
        suggestedSize= CGSizeMake(350., 250.);
    }
    [pop setPopoverContentSize:suggestedSize
                      animated:NO];
    
    
    if (filingView.filing.isDef){
        //Use apple built in dict
        
        // UIReferenceLibraryViewController *reference = [[UIReferenceLibraryViewController alloc] initWithTerm:filingView.filing.word.term];
        //[self presentModalViewController:reference animated:YES];
        
        //[self stopBoxWorld];
        /*
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
         NSString *def =[self.dictionarymanager getDefinitionForWord:filingView.filing.word.term];
         NSMutableDictionary* content = [[NSMutableDictionary alloc] init];
         [content setObject:def forKey: @"description"];
         [self.snippetController.filing setContent:content];
         NSLog(@"content, %@", content);
         return;
         } else if ([[filingView.filing.content objectForKey:@"type"] isEqualToString:@"image"]){
         */
        
     
        //Use apple dict in wildcard, internal one otherwise
        if ([self.wordPackStr isEqualToString:@"Wildcard"]){
            UIReferenceLibraryViewController *reference = [[UIReferenceLibraryViewController alloc] initWithTerm:filingView.filing.word.term];
            [self presentModalViewController:reference animated:YES];
            [self stopBoxWorld];
            return;
        } else {
            NSString* def= [self.dictionarymanager getDefinitionForWord:filingView.filing.word.term];
            NSMutableDictionary* content= [[NSMutableDictionary alloc] init];
            [content setObject:def forKey:@"description"];
            NSLog(@"Set DEFINITIONN: term: %@, def: %@",filingView.filing.word.term, def);
            [self.snippetController.filing setContent:content];
        }
        //end of adding in
    } else {
        [filingView markAsRead];
    }
    
    [self.filingPopoverController presentPopoverFromRect:filingView.frame
                                                  inView:self.view
                                permittedArrowDirections:UIPopoverArrowDirectionAny
                                                animated:YES];
    
    
    [self stopBoxWorld];
}

- (void)flashAttractor:(AttractorView*)newAttractor {
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.4];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [CATransaction setCompletionBlock:^{
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        newAttractor.shapeLayer.fillColor = [UIColor grayColor].CGColor;
        [CATransaction commit];
    }];
    newAttractor.shapeLayer.fillColor = [UIColor randomColor].CGColor;
    [CATransaction commit];
}

#pragma mark - Custom ViewController Presentation

- (void)showViewController:(UIViewController*)controller {
    
    OPLogMethod;
    
    // if self.embeddedViewControllerBackdropView has a superview, we are alreay presenting a view controller
    if (nil != self.embeddedViewControllerBackdropView.superview) {
        return;
    }
    
    
    controller.view.alpha = 0.;
    CGSize S = controller.contentSizeForViewInPopover;
    
    controller.view.frame = CGRectMake(0., 0., S.width, S.height);
    controller.view.center = CGPointMake(self.view.bounds.size.width/2., self.view.bounds.size.height/2.);
    
    controller.view.clipsToBounds = YES;
    controller.view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    controller.view.layer.borderWidth = 2.;
    controller.view.layer.cornerRadius = 10.;
    controller.view.backgroundColor = [UIColor whiteColor];
    
    self.embeddedViewControllerBackdropView.alpha = 0.;
    self.embeddedViewControllerBackdropView.frame = self.view.bounds;
    self.embeddedViewControllerBackdropView.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.8];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(hideViewController)];
    tap.delegate = self;
    [self.embeddedViewControllerBackdropView addGestureRecognizer:tap];
    [self.view addSubview:self.embeddedViewControllerBackdropView];
    
    [self.embeddedViewControllerBackdropView addSubview:controller.view];
    
    controller.view.layer.shouldRasterize = YES;
    
    // this is to move between two sibling view controllers.
    [self addChildViewController:controller];
    
    [UIView animateWithDuration:.1
                     animations:^{
                         
                         self.embeddedViewControllerBackdropView.alpha = 1.;
                         
                     } completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.2
                                             options:0
                                          animations:^{
                                              
                                              controller.view.alpha = 1.;
                                              
                                          } completion:^(BOOL finished) {
                                              
                                              [controller didMoveToParentViewController:self];
                                          }];
                         
                         
                     }];
}

- (void)hideViewControllerCompletion:(void(^)(void))completion {
    
    UIViewController *controller = [self.childViewControllers lastObject];
    
    [controller willMoveToParentViewController:nil];
    
    controller.view.layer.shouldRasterize = YES;
    
    [UIView animateWithDuration:.5
                     animations:^{
                         controller.view.alpha = 0.;
                         
                     } completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.1
                                             options:0
                                          animations:^{
                                              self.embeddedViewControllerBackdropView.alpha = 0.0;
                                          } completion:^(BOOL finished) {
                                              [self.embeddedViewControllerBackdropView removeFromSuperview];
                                              
                                              self.embeddedViewControllerBackdropView.gestureRecognizers = nil;
                                              
                                              [controller.view removeFromSuperview];
                                              [controller removeFromParentViewController];
                                              
                                              if (completion)
                                                  completion();
                                          }];
                     }];
    
}

- (void)hideViewController {
    [self hideViewControllerCompletion:nil];
}

#pragma mark - Reverse Pinch Controller Fun

- (void)showCardController:(ContextCardViewController*)controller completion:(void(^)(void))completion {
    
    [self addChildViewController:controller];
    [controller willMoveToParentViewController:self];
    [self.embeddedViewControllerBackdropView addSubview:controller.view];
    controller.view.center = CGPointMake(self.view.bounds.size.width/2.,
                                         self.view.bounds.size.height/2.);
    
    [UIView animateWithDuration:.3
                     animations:^{
                         self.embeddedViewControllerBackdropView.alpha = 1.;
                         
                     } completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.2
                                             options:0
                                          animations:^{
                                              controller.view.alpha = 1.;
                                          } completion:^(BOOL finished) {
                                              
                                              completion();
                                              
                                              [controller didMoveToParentViewController:self];
                                          }];
                         
                         
                     }];
}

#pragma mark - +Attractor

- (void)hideNewAttractor:(NSString*)term {
    
    AttractorView *view = self.addAttractorView;
    NSValue *Tv = [view.layer valueForKey:@"originalCenter"];
    UIImageView *iv = [view.layer valueForKey:@"imageView"];
    UITextField *tf = [view.layer valueForKey:@"textField"];
    
    AttractorView *newAttractor = nil;
    
    if (term) {
        LanguageWord *newWord = [[LanguageWord alloc] init];
        newWord.term = term;
        newWord.gloss = [[LanguageSource gloss] valueForKey:[term lowercaseString]];
        
        
        [self.wordPack.words addObject:newWord];
        
        newAttractor = [self insertAttractorForWord:newWord];
        [self.view addSubview:newAttractor];
        [self.attractorViews addObject:newAttractor];
        newAttractor.alpha = 0.;
        
        tf.text = nil;
    } else {
        [tf resignFirstResponder];
    }
    
    
    [self removePlusAttractor]; //we remove the + attractor
    NSLog(@"Removed the + attractor");
    
    [UIView animateWithDuration:.6
                          delay:0.1
                        options:0
                     animations:^{
                         //view.transform = T;
                         view.center = [Tv CGPointValue];
                         tf.alpha = 0.;
                         self.embeddedViewControllerBackdropView.alpha = 0.;
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3
                                          animations:^{
                                              for (AttractorView *av in self.attractorViews) {
                                                  av.userInteractionEnabled = YES;
                                                  av.alpha = 1.;
                                              }
                                              
                                              iv.alpha = 1.;
                                              self.fluffyView.alpha = 1.;
                                          }];
                         
                         [self startBoxWorld];
                         
                         UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                               action:@selector(handleAttractorTapFrom:)];
                         [view addGestureRecognizer:tap];
                         view.tapRecognizer = tap;
                         
                         self.embeddedViewControllerBackdropView.gestureRecognizers = nil;
                         [self.embeddedViewControllerBackdropView removeFromSuperview];
                         
                         if (term) {
                             [self performSelector:@selector(flashAttractor:) withObject:newAttractor afterDelay:.5];
                         }
                         
                         
                     }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (![textField.text isEqualToString:@""]){
        [self hideNewAttractor:textField.text];
    } else {
        [self hideNewAttractor:nil];
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void) createPlusAttractor {
    AttractorView *aView = [self insertAttractorForWord:nil];
    [self.view addSubview:aView];
    self.addAttractorView = aView;
    
    // toss the long press recognizer, since the add attractor doesn't need it
    [aView removeGestureRecognizer:aView.longPressRecognizer];
    // also the pinch
    [aView removeGestureRecognizer:aView.pinchRecognizer];
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"plus_attractor"]];
    [aView.rotatingView addSubview:iv];
    iv.center = CGPointMake(aView.bounds.size.width/2., aView.bounds.size.height/2.);
    iv.userInteractionEnabled = YES;
    [aView.layer setValue:iv forKey:@"imageView"];
    
    UITextField *field = [[UITextField alloc] initWithFrame:CGRectMake(0., 0., aView.bounds.size.width-10., 42.)];
    field.center = CGPointMake(aView.bounds.size.width/2., aView.bounds.size.height/2.);
    field.font = [UIFont boldSystemFontOfSize:28.];
    field.adjustsFontSizeToFitWidth = YES;
    field.borderStyle = UITextBorderStyleBezel;
    field.alpha = 0.;
    [aView addSubview:field];
    [aView.layer setValue:field forKey:@"textField"];
    field.backgroundColor = [UIColor whiteColor];
    field.delegate = self;
}

- (void) removePlusAttractor{
    // remove + attractor
    [self.world removeBody:self.addAttractorView.body];
    [self.addAttractorView removeFromSuperview];
    self.addAttractorView = nil;
}

- (void)showNewAttractor {
    
    [self stopBoxWorld];
    
    [self createPlusAttractor];
    AttractorView *view = self.addAttractorView;
    
    NSValue *Tv = [NSValue valueWithCGPoint:view.center];
    
    UITextField *tf = [view.layer valueForKey:@"textField"];
    UIImageView *iv = [view.layer valueForKey:@"imageView"];
    
    [view.layer setValue:Tv forKey:@"originalCenter"];
    
    tf.placeholder = @"new word";
    
    [self.view addSubview:self.embeddedViewControllerBackdropView];
    self.embeddedViewControllerBackdropView.backgroundColor = [UIColor colorWithWhite:1. alpha:0.8];
    self.embeddedViewControllerBackdropView.alpha = 0.;
    
    [self.view bringSubviewToFront:view];
    
    [view removeGestureRecognizer:view.tapRecognizer];
    
    [UIView animateWithDuration:0.4
                          delay:0.
                        options:0
                     animations:^{
                         
                         view.center = CGPointMake(self.view.bounds.size.width/2.,
                                                   self.view.bounds.size.height/2.);
                         
                         iv.alpha = 0.;
                         tf.alpha = 1.;
                         self.fluffyView.alpha = 0.1;
                         for (AttractorView *av in self.attractorViews) {
                             av.userInteractionEnabled = NO;
                             av.alpha = 0.2;
                         }
                         self.embeddedViewControllerBackdropView.alpha = 1.;
                         
                     }
                     completion:^(BOOL finished) {
                         
                         [tf becomeFirstResponder];
                         
                         UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                               action:@selector(handleCanvasTapFrom:)];
                         tap.delegate = self;
                         [self.embeddedViewControllerBackdropView addGestureRecognizer:tap];
                     }];
}

#pragma mark -

- (void)renderImageOfController:(UIViewController*)controller withFrame:(CGRect)frame completion:(void(^)(UIImage* img))completion {
    
    NSAssert(controller, @"missing controller");
    
    // render the image in a secondary thread to avoid locking up the main thread.
    [self.workQueue addOperationWithBlock:^{
        //OPLogMethod;
        UIView *view = controller.view;
        view.frame = frame;
        [view layoutIfNeeded];
        UIGraphicsBeginImageContext(controller.view.bounds.size);
        [controller.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            completion(img);
        }];
        
    }];
}

#pragma mark - Gesture Recognizer Callbacks

- (void)handleCanvasTapFrom:(UITapGestureRecognizer*)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [self hideNewAttractor:nil];
    }
}

// filing gestures
- (void)handleFilingTapFrom:(UITapGestureRecognizer*)recognizer {
    FilingView *view = (FilingView*)recognizer.view;
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        // ...
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        [self showSnippetForFiling:view];
	}
}

- (void)removeFiling:(FilingView*)filing fromAttractor:(AttractorView*)attractor {
    [self.world removeBody:filing.body];
    [attractor.word.filings removeObject:filing];
    [filing removeFromSuperview];
    [attractor.filingViews removeObject:filing];
}

- (void)handleFilingPanFrom:(UIPanGestureRecognizer*)recognizer {
    FilingView *view = (FilingView*)recognizer.view;
    BoxBody *theBody = view.body;
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        // should create joint to pin attractor down here.
        BoxJoint* attractorJoint=[[BoxJoint alloc] init];
        attractorJoint.bodyA=view.attractorView.body;
        attractorJoint.bodyB=[wallBodies lastObject];
        //[self.world addJoint:attractorJoint];
        [view.layer setValue:attractorJoint
                      forKey:@"pinJoint"];
        //
        
        BoxMouseJoint *mJoint = [[BoxMouseJoint alloc] init];
        mJoint.bodyA = self.mouseAnchorBody;//[self.wallBodies lastObject];
        mJoint.bodyB = theBody;
        mJoint.targetPoint = [recognizer locationInView:self.view];
        mJoint.maxForce = 1000.0*theBody.mass;
        mJoint.dampingRatio = 0.9;
        [self.world addJoint:mJoint];
        [view.layer setValue:mJoint
                      forKey:@"mouseJoint"];
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        mJoint.targetPoint = [recognizer locationInView:self.view];
        
        BoxDistanceJoint *dJoint = [view.layer valueForKey:@"distanceJoint"];
        
        if (dJoint) {
            CGPoint p1 = dJoint.localAnchorA;
            CGPoint p2 = dJoint.localAnchorB;
            CGFloat distance = CGPointDistanceToPoint(p1, p2);
            
            if (distance <= 100.) {
                
                [UIView animateWithDuration:.2
                                 animations:^{
                                     view.alpha = 0.5;
                                 }];
                
            } else {
                view.alpha = 1.;
            }
            
        }
        
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        //remove "pin"joint here
        BoxJoint* pinJoint = [view.layer valueForKey:@"attractorPinJoint"];
        [self.world removeJoint:pinJoint];
        [view.layer setValue:nil forKey:@"attractorPinJoint"];
        //
        
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        [self.world removeJoint:mJoint];
        [view.layer setValue:nil forKey:@"mouseJoint"];
        
        BoxDistanceJoint *dJoint = [view.layer valueForKey:@"distanceJoint"];
        
        if (dJoint) {
            CGPoint p1 = dJoint.localAnchorA;
            CGPoint p2 = dJoint.localAnchorB;
            CGFloat distance = CGPointDistanceToPoint(p1, p2);
            
            if (distance >= 200.0) { //remove filing
                //OPLog(@"removing distance joint: %f",distance);
                [view.layer setValue:nil forKey:@"distanceJoint"];
                [self.world removeJoint:dJoint];
                
                // remove gesture recognizers - so you can't interact with the filing anymore
                NSArray *gr = view.gestureRecognizers;
                for (UIGestureRecognizer *recog in gr) {
                    [view removeGestureRecognizer:recog];
                }
                
                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options:0
                                 animations:^{
                                     view.alpha = 0.0;
                                     
                                 } completion:^(BOOL finished) {
                                     [self removeFiling:view fromAttractor:view.attractorView];
                                 }];
            }
            
        }
        
	}
    
}

- (void)highlightAttractor:(AttractorView*)attractor {
    
    if (attractor) {
        [UIView animateWithDuration:0.4
                         animations:^{
                             for (AttractorView *av in self.attractorViews) {
                                 if (av != attractor) {
                                     av.alpha = 0.7;
                                 } else {
                                     av.alpha = 1.;
                                 }
                             }
                         }];
    } else {
        [UIView animateWithDuration:0.4
                         animations:^{
                             for (AttractorView *av in self.attractorViews) {
                                 av.alpha = 1.;
                             }
                         }];
    }
    
}

// attractor gestures
- (void)handleAttractorTapFrom:(UITapGestureRecognizer*)recognizer {
    AttractorView *tappedAttractor = (AttractorView*)recognizer.view;
    
    // set to true to read tapped attractors
    tappedAttractor.isCompleted=true;
    
    OPLog(@"tap attractor");
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        // ...
        
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        OPLog(@"gotinfirststep");
        self.selectedAttractor = tappedAttractor;
        
        //close all attractors but the tapped one
        for (AttractorView *av in self.attractorViews) {
            if (av != tappedAttractor) {
                if (kAttractorViewStateClosed != av.state) {
                    [self closeAttractor:av];
                }
            }
        }
        
        if (tappedAttractor == self.addAttractorView) {
            [self showNewAttractor];
            [TestFlight passCheckpoint:@"NewAttractorTap"];
        } else if ([tappedAttractor.word.term isEqualToString:@"Welcome"]){

            [self showPopover:self.wordpacks];
            
            //don't show filings if attractor isn't completed
        } else if (![tappedAttractor isCompleted]){
            //  OPLog(@"in this");
            return ;
        } else{
            // normal attractor
            if (kAttractorViewStateOpen == tappedAttractor.state) {
                [TestFlight passCheckpoint:@"AttractorTapClose"];
                [self closeAttractor:tappedAttractor];
                
                [self highlightAttractor:nil];
                
            } else if (kAttractorViewStateClosed == tappedAttractor.state) {
                OPLog(@"Got into new attract");
                [TestFlight passCheckpoint:@"AttractorTapOpen"];
                [self loadAttractor:tappedAttractor];
                [self highlightAttractor:tappedAttractor];
                
            }
        }
    }
}

- (void) showWelcomeAttractors{
    AttractorView* newAttractor;
    LanguageWord* w= [LanguageWord alloc];
    
    w.term=@"Welcome";
    newAttractor=[self insertAttractorForWord:w]; //TODO: pull up menu when this attractor is tapped
    
}


- (void)handleAttractorPanFrom:(UIPanGestureRecognizer*)recognizer {
    
    AttractorView *view = (AttractorView*)recognizer.view;
    
    BoxBody *theBody = view.body;
    NSAssert(theBody,@"wtf");
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        BoxMouseJoint *mJoint = [[BoxMouseJoint alloc] init];
        mJoint.bodyA = self.mouseAnchorBody;//[self.wallBodies lastObject];
        mJoint.bodyB = theBody;
        mJoint.targetPoint = [recognizer locationInView:self.view];
        mJoint.maxForce = 1000.0*theBody.mass;
        mJoint.dampingRatio = 0.9;
        
        [self.world addJoint:mJoint];
        
        [view.layer setValue:mJoint
                      forKey:@"mouseJoint"];
        view.touched=YES;
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        mJoint.targetPoint = [recognizer locationInView:self.view];
        
        //check if we need to merge with an other attractor
        AttractorView* attractorToMerge=nil;
        
        for (AttractorView* a in [self attractorViews]){
            float dist=CGPointDistanceToPoint(a.frame.origin, view.frame.origin);
            // Took out to not read merging
            /*if (a!=view && a.touched && dist<ATTRACTOR_MERGE_THRESHOLD){
             BOOL shouldMerge=[self checkMergeMatch:a.word.term with:view.word.term];
             if (shouldMerge)
             attractorToMerge=a;
             }*/
            
            if ([a.type isEqualToString:@"welcome_magnet"]){
                NSLog(@"%f", dist);
                NSLog(@"Ax: %f, Ay: %f", a.frame.origin.x, a.frame.origin.y);
                NSLog(@"Fx: %f, Fy: %f", view.frame.origin.x, view.frame.origin.y);
                
                /*   if (a!=view && a.touched && dist < 410){
                 [self mergeWelcomeAttractorPieces];
                 return;
                 }*/
            }
        }
        
        if (attractorToMerge!=nil){
            [self mergeAttractor:view and:attractorToMerge];
            
            //check if we have merged all words in current pack
            //if yes, poetry!
            NSLog(@"Currently, %i words are completed out of %i", words_completed_current_wordpack, words_in_current_wordpack);
            if (words_completed_current_wordpack==words_in_current_wordpack){
                [self loadPoemWords];
                [self notifyFullVersion];
            }
        }
        
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        
        [self.world removeJoint:mJoint];
        [view.layer setValue:nil
                      forKey:@"mouseJoint"];
        
        view.touched=NO;
        
	}
}
/*
 - (BOOL) checkMergeMatch:(NSString*)affix1 with:(NSString*)affix2{
 NSArray* words=[wordpackDB getWordsWithAffix:affix1];
 
 for (WordDescription* w in words){
 if ([w existsRewriteRuleFor:affix1 and:affix2])
 return YES;
 //check whether affix is in word and matches already completed rule
 if ([w stringIsRewriteResult:affix1] || [w stringIsRewriteResult:affix2])
 return YES;
 }
 
 return NO;
 }
 
 
 - (void) mergeWelcomeAttractorPieces{
 CGPoint pos= CGPointMake(0, 0);
 NSLog(@"Merge welcome attractors");
 
 for (AttractorView* v in self.attractorViews){
 pos.x+=v.body.position.x;
 pos.y+=v.body.position.y;
 [self.world removeBody:v.body];
 [v removeFromSuperview];
 }
 pos.x = pos.x/2;
 pos.y = pos.y/2;
 [self.attractorViews removeAllObjects];
 
 LanguageWord* w = [[LanguageWord alloc] init];
 w.term=@"WELCOME";
 
 AttractorView* v=[self insertAttractorForWord:w];
 v.type=@"welcome";
 
 [v setCenter:pos];
 [v.body setPosition:pos];
 }
 */
//OUTDATED
- (bool)isSuffix:(NSString*)string{
    if ([[morphemeDB getTypeForMorpheme:string] isEqualToString:@"suffix"])
        return true;
    return false;
    
    //OUTDATED
    if ([string characterAtIndex:0]=='-')
        return true;
    return false;
}

//OUTDATED
- (bool)isPrefix:(NSString*)string{
    if ([[morphemeDB getTypeForMorpheme:string] isEqualToString:@"prefix"])
        return true;
    return false;
    
    //
    if ([string characterAtIndex:[string length]-1]=='-')
        return true;
    return false;
}

- (void)mergeAttractor:(AttractorView*)a and:(AttractorView*)b{
    //play sound
    [self playMergeSound];
    
    CGPoint coordA= a.center;
    CGPoint coordB= b.center;
    CGPoint pos= CGPointMake((coordA.x+coordB.x)/2, (coordA.y+coordB.y)/2);
    
    LanguageWord* word= [LanguageWord alloc];
    word.term=[wordpackDB getRewriteRuleResultFor:a.word.term and:b.word.term];
    
    //add attractor
    AttractorView* aView=[self insertAttractorForWord:word];
    [self.view addSubview:aView];
    [self.attractorViews addObject:aView];
    
    //check if the word just formed is a finished word
    if ([wordpackDB isFullWord:word.term]){
        words_completed_current_wordpack+=1;
        aView.isCompleted=YES;
        [aView setImage];
        //attractors open up upon completion of a full word
        [self bloomAttractor:aView];
    }
    
    
    //store submorphemes in attractor to be able to break it apart
    NSMutableArray* morphemeArray= [[NSMutableArray alloc] initWithCapacity:3];
    if ([a.morphemeComponents count]==0)
        [morphemeArray addObject:a.word];
    else
        [morphemeArray addObjectsFromArray:a.morphemeComponents];
    
    if ([b.morphemeComponents count]==0)
        [morphemeArray addObject:b.word];
    else
        [morphemeArray addObjectsFromArray:b.morphemeComponents];
    
    aView.morphemeComponents=morphemeArray;
    
    //remove 2 old attractors
    [self removeSingleAttractor:a];
    [self removeSingleAttractor:b];
    
    //put attractor right under finger
    [aView setCenter:pos];
    [aView.body setPosition:pos];
}
- (void)handleAttractorPinchFrom:(UIPinchGestureRecognizer*)recognizer {
    //only perform gesture recognizer once
    //THIS IS NOT IDEAL!
    //We want to do as follows:
    //NSUInteger index=[self.attractorViews indexOfObject:att];
    //NSLog(@"Position for attractor %@ is %d", att.word.term, index);
    //if index==NSNotFound
    //this doesn't work because of (i think) multithreading issues. Need to look into it.
    
    NSLog(@"unglom: %f", recognizer.scale);
    
    if (recognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    //can't unglom atomic words
    if ([((AttractorView*)recognizer.view).morphemeComponents count]==0)
        return;
    
    
    
    if (recognizer.scale>1.0){
        int i=-10;
        for (LanguageWord* l in ((AttractorView*)recognizer.view).morphemeComponents){
            NSLog(@"%@", l.term);
            AttractorView* newAttractor= [self insertAttractorForWord:l];
            
            //make it small if morpheme
            if ([self isSuffix:l.term] || [self isPrefix:l.term])
                [newAttractor makeMorpheme];
            
            //set position under fingers
            CGPoint pos= recognizer.view.center;
            pos.x+=-10*i;
            pos.y+=-10*i;
            newAttractor.center= pos;
            newAttractor.body.position= pos;
            
            i+=10;
        }
        
        //one less word completed if what we unglommed was full word
        if ([wordpackDB isFullWord:((AttractorView*)recognizer.view).word.term])
            words_completed_current_wordpack-=1;
        
        
        
        [self removeSingleAttractor:(AttractorView*)recognizer.view];
        
    }
}

- (void)handleAttractorLongPressFrom:(UILongPressGestureRecognizer*)recognizer {
    AttractorView *v = (AttractorView*)recognizer.view;
    if (UIGestureRecognizerStateBegan == recognizer.state) {
        UIImageView *iv = [v.layer valueForKey:@"cardImageView"];
        
        [iv removeFromSuperview];
        
        CGRect targetRect = CGRectMake(0., 0., 0.8*768., 0.8*1004.);
        
        self.embeddedViewControllerBackdropView.alpha = 0.;
        self.embeddedViewControllerBackdropView.frame = self.view.bounds;
        [self.view addSubview:self.embeddedViewControllerBackdropView];
        [self.view addSubview:iv];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(hideViewController)];
        tap.delegate = self;
        [self.embeddedViewControllerBackdropView addGestureRecognizer:tap];
        
        iv.center = v.center;
        [UIView animateWithDuration:0.5
                              delay:0.
                            options:0
                         animations:^{
                             
                             self.embeddedViewControllerBackdropView.alpha = 1.;
                             iv.bounds = targetRect;
                             iv.center = CGPointMake(self.view.bounds.size.width/2., self.view.bounds.size.height/2.);
                             iv.alpha = 1.;
                             v.rotatingView.alpha = 1.;
                             
                         } completion:^(BOOL finished) {
                             
                             ContextCardViewController *vc = [v.layer valueForKey:@"contextCardController"];
                             vc.view.bounds = targetRect;
                             [self showCardController:vc completion:^{
                                 [iv removeFromSuperview];
                                 //[v.layer setValue:nil forKey:@"cardImageView"];
                             }];
                             
                         }];
        
    }
    
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer*)recognizer {
    
    CGPoint p = [recognizer locationInView:self.view];
    
    UIView *v = [self.view hitTest:p withEvent:nil];
    //OPLog(@"self.view %p v %p",self.view,v);
    if (v != self.fluffyView) {
        return; // if there is a view under the touch, and it's not the main view, ignore swipe
    }
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
    }
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        [TestFlight passCheckpoint:@"SwipeRightForWordPacks"];
        
    }
}

// Ignore touch events on UIControl classes. Thanks Apple. :/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if (touch.view == gestureRecognizer.view) {
        return YES;
    }
    return NO;
    
}

#pragma mark - UI Actions

- (CGFloat)randomFloatSeededWithString:(NSString*)string {
    NSData *wd = [[string dataUsingEncoding:NSUTF8StringEncoding] SHA1];
    
    long *d = (long*)[wd bytes]; // pointer to a long
    
    long seedling = *(d);
    
    d+= 1;
    seedling += *(d);
    
    srandom(seedling);
    
    return RANDOM_FLOAT();
}

- (UIColor*) colorFromPaletteWithIndex:(int)i{
    //Color palette: Iberian Sun from Kuler
    UIColor* c1=[UIColor colorWithRed:0.572f green:0.933f blue:0.098f alpha:0.8f];
    UIColor* c2=[UIColor colorWithRed:1.000f green:0.349f blue:0.125f alpha:0.8f];
    UIColor* c3=[UIColor colorWithRed:0.988f green:0.000f blue:0.254f alpha:0.8f];
    UIColor* c4=[UIColor colorWithRed:0.078f green:0.611f blue:0.788f alpha:0.8f];
    UIColor* c5=[UIColor colorWithRed:0.956f green:0.890f blue:0.380f alpha:0.8f];
    
    NSArray* colors= [[NSArray alloc] initWithObjects:c1, c2, c3, c4, c5, nil];
    
    if (i<0 || i>[colors count]){
        i=rand()%5;
    }
    
    return [colors objectAtIndex:i];
}

- (void)removeAttractors {
    // get rid of existing attractors
    for (AttractorView *av in self.attractorViews) {
        [self.world removeBody:av.body];
        [av removeFromSuperview];
        
        // remove any filings that the attractor has
        for (FilingView *fv in av.filingViews) {
            [self.world removeBody:fv.body];
            [fv removeFromSuperview];
        }
    }
    [self.attractorViews removeAllObjects];
    
    [self removePlusAttractor];
    
}

- (void)deleteAttractor:(UIButton*)sender {
    AttractorView *av = (AttractorView*)sender.superview;
    
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:0
                     animations:^{
                         
                         av.alpha = 0.0;
                         
                     } completion:^(BOOL finished) {
                         
                         [av removeFromSuperview];
                         
                         if (av == self.selectedAttractor) {
                             [self highlightAttractor:nil];
                         }
                         
                         for (FilingView *fv in av.filingViews) {
                             
                             BoxJoint *joint = [fv.layer valueForKey:@"distanceJoint"];
                             if (joint) {
                                 [self.world removeJoint:joint];
                             }
                             
                             [self.world removeBody:fv.body];
                             [fv removeFromSuperview];
                         }
                         [self.world removeBody:av.body];
                         
                         
                         
                     }];
    
    OPLog(@"delete: %@",av);
}

- (FilingView*)insertFilingViewForFiling:(LanguageFiling*)aFiling atPosition:(CGPoint)position {
    
    FilingView *fv = [[FilingView alloc] initWithFrame:CGRectMake(0., 0., 60., 60.)];
    NSString *filingURL = nil;
    filingURL = [aFiling.content valueForKey:@"filing_id"];
    
    // View
    
    fv.shapeLayer.frame = CGRectMake(0., 0., fv.bounds.size.width, fv.bounds.size.height);
    fv.shapeLayer.path = [UIBezierPath bezierPathWithOvalInRect:fv.shapeLayer.bounds].CGPath;
    
    fv.shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    fv.shapeLayer.fillColor = [UIColor clearColor].CGColor;//[self colorFromPaletteWithIndex:fColor].CGColor;
    
    // add Gesture Recognizers
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleFilingTapFrom:)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    [fv addGestureRecognizer:recognizer];
    fv.tapRecogznier = recognizer;
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(handleFilingPanFrom:)];
    pan.minimumNumberOfTouches = 1;
    pan.maximumNumberOfTouches = 1;
    [fv addGestureRecognizer:pan];
    fv.panRecognizer = pan;
    
    // Configure
    
    CGFloat a = fv.bounds.size.width;
    fv.alpha = 0.;
    
    BoxFixture *fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:a/2.0 Position:CGPointZero]];
    fixture.friction = 0.5;
    fixture.density = 0.3;
    
    BoxBody *body = [BoxBody dynamicBody];
    body.linearDamping = 0.9;
    body.angularDamping = 0.5;
    [body addFixture:fixture];
    [self.world addBody:body];
    
    CGPoint pp = cartesianPointFromPolar(20.0*RANDOM_FLOAT(), 2.0*M_PI*RANDOM_FLOAT());
    body.position = CGPointMake(pp.x + position.x,
                                pp.y + position.y);
    
    fv.center = body.position;
    
    body.weakContext = fv;
    
    fv.body = body;
    fv.filing = aFiling;
    
    //if filing is dictionary access
    if (aFiling.isDef){
        [fv addDLabel];
    } else if ([[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
        [fv addImageLabel];
        
   /* } else if (aFiling.hasRead && ![[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
        [fv markAsRead];
    } else if (aFiling.hasRead && [[aFiling.content objectForKey:@"type"] isEqualToString:@"image"]) {
        NSLog(@"read and image");
        [fv addImageLabel];
     */
    } else {
        [fv markAsUnread];
    }
    
    return fv;
}

- (AttractorView*)insertAttractorForWord:(LanguageWord*)aWord {
    OPLog(@"New attractor for %@",aWord.term);
    NSLog(@"type: %@",[morphemeDB getTypeForMorpheme:aWord.term]);
    
    CGFloat rr = 0.0;
    if (aWord)
        rr = [self randomFloatSeededWithString:aWord.term];
    
    // View
    AttractorView *av = [[AttractorView alloc] initWithFrame:CGRectMake(0., 0., MAGNET_WIDTH, MAGNET_HEIGHT)];
    
    if ([self isSuffix:aWord.term] || [self isPrefix:aWord.term]){
        [av makeMorpheme];
        
        if ([[morphemeDB getTypeForMorpheme:aWord.term] isEqualToString:@"suffix"])
            // set Defintion for NYU
            [av.defLabel setHidden:NO];
    }
    
    
    
    [av.layer setValue:[NSNumber numberWithFloat:rr]
                forKey:@"hue"];
    
    av.shapeLayer.fillColor = [UIColor grayColor].CGColor;
    
    av.radius = av.bounds.size.width/2.;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(handleAttractorTapFrom:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    tap.delegate = av;
    [av addGestureRecognizer:tap];
    av.tapRecognizer = tap;
    
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(handleAttractorPanFrom:)];
    pan.minimumNumberOfTouches = 1;
    pan.maximumNumberOfTouches = 1;
    [av addGestureRecognizer:pan];
    av.panRecognizer = pan;
    NSLog(@"pan %d", av.panRecognizer.enabled);
    
    //reverse pinch action
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(handleAttractorPinchFrom:)];
    [av addGestureRecognizer:pinch];
    av.pinchRecognizer=pinch;
    
    
    // Delete action
    [av.deleteButton addTarget:self
                        action:@selector(deleteAttractor:)
              forControlEvents:UIControlEventTouchUpInside];
    av.deleteButton.alpha = 0.;
    
    
    // Box2D
    /*
     BoxFixture* fixture;
     if (av.isMorpheme){
     int width=SMALL_MAGNET_WIDTH;
     int height=SMALL_MAGNET_HEIGHT;
     
     PolygonShape* shape= [PolygonShape rectangleWithSize:CGSizeMake(width,height)];
     shape.centroid=CGPointMake(-1*width/2, height/2);
     fixture= [BoxFixture fixtureWithShape:shape];
     } else{
     int width=MAGNET_WIDTH;
     int height=MAGNET_HEIGHT;
     
     PolygonShape* shape=[PolygonShape rectangleWithSize:CGSizeMake(width,height)];
     shape.centroid=CGPointMake(-1*width/2, height/2);
     fixture= [BoxFixture fixtureWithShape:shape];
     }
     */
    
    BoxFixture *fixture;
    if (av.isMorpheme){
        fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:SMALL_MAGNET_HEIGHT/1.5 Position:CGPointZero]];
    } else {
        fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:MAGNET_HEIGHT/1.5 Position:CGPointZero]];
    }
    
    fixture.friction = 0.5;
    fixture.density = 10.0;
    fixture.restitution = 0.5;
    
    BoxBody *body = [BoxBody dynamicBody];
    body.linearDamping = 60;
    body.angularDamping = 60;
    [body addFixture:fixture];
    [self.world addBody:body];
    body.weakContext = av;
    
    // Set up position
    av.word = aWord;
    av.body = body;
    
    //pin body if not morpheme
    if (!av.isMorpheme){
        
    }
    
    RANDOM_SEED();
   CGFloat rx = 0.1 + RANDOM_FLOAT()*(0.7*self.view.bounds.size.width);
    CGFloat ry = 0.1 + RANDOM_FLOAT()*(0.7*self.view.bounds.size.height);

    body.position = CGPointMake(rx,ry);
    
    av.label.text = aWord.term;
    
   NSString* def=[morphemeDB getDefinitionForMorpheme:aWord.term];
   // NSString* def=@"THIS IS DEF";
   // NSLog(@"Def: %@", def);
    av.defLabel.text=def;
    
    //populate morpheme array with the word we initiated the attractor with
    [av.morphemeComponents addObject:aWord.term];
    
    
    if (aWord) {
        // pre-cache the card image and controller
        ContextCardViewController *ctxvc = [[ContextCardViewController alloc] initWithNibName:nil bundle:nil];
        ctxvc.word = av.word;
        [av.layer setValue:ctxvc
                    forKey:@"contextCardController"];
        
        [self renderImageOfController:ctxvc
                            withFrame:CGRectMake(0., 0., 600., 800.)
                           completion:^(UIImage *img) {
                               
                               UIImageView *iv = [[UIImageView alloc] initWithImage:img];
                               iv.frame = av.bounds;
                               iv.contentMode = UIViewContentModeScaleAspectFit;
                               iv.alpha = 0.;
                               
                               iv.center = CGPointMake(av.bounds.size.width/2.,
                                                       av.bounds.size.height/2.);
                               [av addSubview:iv];
                               [av.layer setValue:iv forKey:@"cardImageView"];
                           }];
        
    }
    
    //add the attractor view to the view
    [self.view addSubview:av];
    //keep track of all attractors being displayed
    [self.attractorViews addObject:av];
    
    return av;
}
// loadWords
- (void)loadAttractorsForWordPack:(LanguageWordPack*)chosenPack {
    OPLog(@"wordPickerDone");
    
    self.wordPack = chosenPack;
    
    // Build attractors for words in the chosen pack
    OPLog(@"Loading %i words.",[self.wordPack.words count]);
    for (LanguageWord *aWord in self.wordPack.words) {
        AttractorView *aView;
        
        aView = [self insertAttractorForWord:aWord];
        
        
    }
    
    
}

- (void)packPickerDidNotify:(NSNotification*)note {
    NSDictionary *userInfo = note.userInfo;
    
    LanguageWordPack *pack = [userInfo valueForKey:@"wordPack"];
    
    [self hideViewControllerCompletion:^{
        if (pack != self.wordPack) {
            
            [UIView animateWithDuration:0.3
                                  delay:0
                                options:0
                             animations:^{
                                 
                                 self.addAttractorView.alpha = 0.0;
                                 for (AttractorView *av in self.attractorViews) {
                                     av.alpha = 0.0;
                                     
                                     for (FilingView *fv in av.filingViews) {
                                         fv.alpha = 0.;
                                     }
                                 }
                                 
                             } completion:^(BOOL finished) {
                                 [self removeAttractors];
                                 [self loadAttractorsForWordPack:pack];
                             }];
        }
    }];
    
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    [self startBoxWorld];
}

#pragma mark - Attractor Utilities

- (void) bloomAttractor:(AttractorView*)a{
    self.selectedAttractor = a;
    [self loadAttractor:a];
    [self highlightAttractor:a];
}

- (void)openAttractor:(AttractorView*)attractor {
    NSLog(@"trying to open attractor");
    LanguageWord *word = attractor.word;
    BoxBody *attractorBody = attractor.body;
    
    //this happens when attractor is unglommed while pulling data
    if (attractorBody==nil){
        [self highlightAttractor:nil];
        return;
    }
      // add filing views for the filings in the word
    for (LanguageFiling *filing in word.filings) {
          NSLog(@"HEREHERE");
        FilingView *fv = [self insertFilingViewForFiling:filing atPosition:attractorBody.position];
        //fv.shapeLayer.fillColor= [self getFilingColor:word.];
        [self.view addSubview:fv];
        fv.attractorView = attractor;
        [attractor.filingViews addObject:fv];
    }
    
    //TODO: CLEANUP
    //    if (word.gloss) {
    LanguageFiling *glossFiling = [[LanguageFiling alloc] init];
    glossFiling.isDef=YES;
    glossFiling.word = word;
    glossFiling.content = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                           word.term,@"filing_id",
                           word.gloss,@"description", nil];
    
    FilingView *glossFilingView = [self insertFilingViewForFiling:glossFiling
                                                       atPosition:attractorBody.position];
    [self.view addSubview:glossFilingView];
    glossFilingView.attractorView = attractor;
    [attractor.filingViews addObject:glossFilingView];
    
    [glossFilingView.layer setValue:glossFiling
                             forKey:@"glossFiling"];
    
    //    }
    
    // make sure the attractor is above the filing views
    [self.view bringSubviewToFront:attractor];
    
    [UIView animateWithDuration:0.0
                          delay:0.0
                        options:0
                     animations:^{
                         
                         // animate the filings from the center of the attractor out
                         for (FilingView *fv in attractor.filingViews) {
                             BoxBody *body = fv.body;
                             fv.alpha = 1.0;
                             CGPoint p = body.position;
                             fv.center = p;
                         }
                         
                     } completion:^(BOOL finished) {
                         
                         for (FilingView *fv in attractor.filingViews) {
                             
                             
                             BoxBody *body = fv.body;
                             BoxDistanceJoint *joint = [[BoxDistanceJoint alloc] init];
                             joint.bodyA = attractorBody;
                             joint.bodyB = body;
                             joint.length = 120.; //defines how far filings are from attractor
                             joint.frequency = 3.0;
                             joint.dampingRatio = 0.1;
                             [self.world addJoint:joint];
                             
                             [fv.layer setValue:joint
                                         forKey:@"distanceJoint"];
                         }
                         
                     }];
    
    attractor.state = kAttractorViewStateOpen;
}

- (void)closeAttractor:(AttractorView*)attractor {
    
    BoxBody *body = attractor.body;
    [self.view bringSubviewToFront:attractor];
    
    for (FilingView *fv in attractor.filingViews) {
        
        // remove distance joint that connects the filing with the attractor
        BoxJoint *joint = [fv.layer valueForKey:@"distanceJoint"];
        if (joint) {
            [self.world removeJoint:joint];
        }
        
        // remove the gesture recognizers so you can't accidently interact with a disappearing filing
        [fv removeGestureRecognizer:fv.panRecognizer];
        [fv removeGestureRecognizer:fv.tapRecogznier];
        
        [self.world removeBody:fv.body];
        
        // animate the view into the attractor
        [UIView animateWithDuration:1.0
                              delay:0.0
                            options:0
                         animations:^{
                             
                             fv.alpha = 0.0;
                             fv.center = body.position;
                             //fv.transform = transformForPositionRotation(body.position, 0.0);
                             
                         } completion:^(BOOL finished) {
                             [fv removeFromSuperview];
                         }];
        
    }
    
    [attractor.filingViews removeAllObjects];
    
    // animate the attractor in a 'suck in' kind of motion
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:0
                     animations:^{
                         
                         attractor.shapeLayer.transform = CATransform3DMakeScale(0.8, 0.8, 1.);
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.2
                                          animations:^{
                                              attractor.shapeLayer.transform = CATransform3DIdentity;
                                          }];
                         
                     }];
    
    attractor.state = kAttractorViewStateClosed;
}

- (void)loadAttractor:(AttractorView *)attractor {
    
    CAShapeLayer *l = attractor.shapeLayer;
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.15];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [CATransaction setCompletionBlock:^{
        [CATransaction begin];
        [CATransaction setAnimationDuration:0.15];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        l.transform = CATransform3DIdentity;
        [CATransaction commit];
    }];
    if (0 == [attractor.word.filings count])
        l.fillColor = [UIColor blackColor].CGColor;
    l.transform = CATransform3DMakeScale(1.2, 1.2, 1.0);
    [CATransaction commit];
    
    attractor.state = kAttractorViewStateWaitingOnNetwork;
    
    if ([attractor.word.filings count] == 0) {
        [attractor startFluffies];
        CGFloat delay = 0; //kAttractorFluffyDelay + RANDOM_FLOAT()*kAttractorFluffyDelay;
        [self performSelector:@selector(tloadAttractor:) withObject:attractor afterDelay:delay];
    } else {
        if (self.selectedAttractor == attractor) {
            [self openAttractor:attractor];
        }
    }
    
    
}


- (void)tloadAttractor:(AttractorView*)attractor {
    
    OPLog(@"loading: %@",attractor.word.term);
    

    attractor.word.wordpackStr= self.wordPackStr;
           // Blekko
    [attractor.word loadFilingsCompletion:^(BOOL success, LanguageWord *word) {
                 [attractor stopFluffies];
        
        if (0 < [word.filings count]) {
            [UIView animateWithDuration:0.2
                             animations:^{
                                 CGFloat hue = [[attractor.layer valueForKey:@"hue"] floatValue];
                                 attractor.shapeLayer.fillColor = [UIColor colorWithHue:hue
                                                                             saturation:0.9
                                                                             brightness:0.6 alpha:1.].CGColor;
                             }];
        } else {
              
            [UIView animateWithDuration:0.2
                             animations:^{
                                 attractor.shapeLayer.fillColor = [UIColor grayColor].CGColor;
                             }];
        }
        
        if (self.selectedAttractor == attractor) {
            if (success) {
                OPLog(@"attractor loaded: %@ (%i filings)",attractor.word.term,[word.filings count]);
                [self openAttractor:attractor];
            } else {
                [self openAttractor:attractor];
            }
            
        }
    }];
}

- (void)removeSingleAttractor:(AttractorView*)attractor{
    OPLog(@"Removing attractor %@", attractor.word.term);
    //remove body
    [self.world removeBody:attractor.body];
    //remove view
    [attractor removeFromSuperview];
    // remove any filings that the attractor has
    for (FilingView *fv in attractor.filingViews) {
        [self.world removeBody:fv.body];
        [fv removeFromSuperview];
    }
    [self.attractorViews removeObject:attractor];
}

/*- (void) showWelcomeAttractors{
 IntroMagnetView* leftMagnet= [[IntroMagnetView alloc] initWithFrame:CGRectMake(0, 0, WELCOME_MAGNET_WIDTH, WELCOME_MAGNET_HEIGHT) andSide:WELCOME_LEFT];
 IntroMagnetView* rightMagnet= [[IntroMagnetView alloc] initWithFrame:CGRectMake(0, 0, WELCOME_MAGNET_WIDTH, WELCOME_MAGNET_HEIGHT) andSide:WELCOME_RIGHT];
 
 
 //add the two welcome magnets to the view
 [self.view addSubview:leftMagnet];
 [self.view addSubview:rightMagnet];
 
 //keep track of all welcome magnets being displayed
 [self.attractorViews addObject:leftMagnet];
 [self.attractorViews addObject:rightMagnet];
 
 //Box2D setup
 BoxFixture *fixture;
 fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:WELCOME_MAGNET_HEIGHT Position:CGPointZero]];
 fixture.friction = 0.2;
 fixture.density = 5.0;
 fixture.restitution = 0.5;
 
 BoxBody *body = [BoxBody dynamicBody];
 body.linearDamping = 60;
 body.angularDamping = 60;
 [body addFixture:fixture];
 [self.world addBody:body];
 body.weakContext = leftMagnet;
 leftMagnet.body = body;
 
 BoxFixture *fixture2;
 fixture2 = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:WELCOME_MAGNET_HEIGHT Position:CGPointZero]];
 fixture2.friction = 0.2;
 fixture2.density = 5.0;
 fixture2.restitution = 0.5;
 
 
 BoxBody *body2 = [BoxBody dynamicBody];
 body2.linearDamping = 60;
 body2.angularDamping = 60;
 [body2 addFixture:fixture2];
 [self.world addBody:body2];
 body2.weakContext = rightMagnet;
 rightMagnet.body = body2;
 
 
 //Gesture recognizers here
 UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
 action:@selector(handleAttractorPanFrom:)];
 pan.minimumNumberOfTouches = 1;
 pan.maximumNumberOfTouches = 1;
 [leftMagnet addGestureRecognizer:pan];
 leftMagnet.panRecognizer = pan;
 
 //Gesture recognizers here
 UIPanGestureRecognizer *pan2 = [[UIPanGestureRecognizer alloc] initWithTarget:self
 action:@selector(handleAttractorPanFrom:)];
 pan2.minimumNumberOfTouches = 1;
 pan2.maximumNumberOfTouches = 1;
 [rightMagnet addGestureRecognizer:pan2];
 rightMagnet.panRecognizer = pan2;
 
 //set positions
 [leftMagnet setCenter:CGPointMake(200, 300)];
 [leftMagnet.body setPosition:CGPointMake(200, 300)];
 [rightMagnet setCenter:CGPointMake(600, 300)];
 [rightMagnet.body setPosition:CGPointMake(700, 300)];
 }
 */
- (void) pinBody:(AttractorView*)a{
    NSLog(@"Pinning: %@", a.word.term);
}


//- (void) playMergeSound{
// [player play];
//}

#pragma mark - Box

#define kBoxWallThickness 200.0
#define kBoxWallWidth 1000.0

- (void)repositionWalls {
    
    for (NSInteger j = 0; j < 4; j++) {
        BoxBody *wall = [self.wallBodies objectAtIndex:j];
        
        switch (j) {
            case 0:
                wall.position = CGPointMake(self.view.bounds.size.width/2.0,
                                            -kBoxWallThickness/2.0);
                
                break;
            case 1:
                wall.position = CGPointMake(self.view.bounds.size.width/2.0,
                                            self.view.bounds.size.height+kBoxWallThickness/2.0);
                break;
            case 2:
                wall.position = CGPointMake(-kBoxWallThickness/2.0,
                                            self.view.bounds.size.height/2.0);
                break;
            case 3:
                wall.position = CGPointMake(self.view.bounds.size.width+kBoxWallThickness/2.0,
                                            self.view.bounds.size.height/2.0);
                break;
            default:
                break;
        }
    }
}

- (void)buildBoxWalls {
    self.wallBodies = [NSMutableArray arrayWithCapacity:4];
    
    BoxBody *bb = [BoxBody staticBody];
    [bb addFixture:[BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:CGSizeMake(10., 10.)]]];
    bb.position = CGPointMake(-200., -200.); // offscreen
    [self.world addBody:bb];
    self.mouseAnchorBody = bb;
    
    // top
    BoxBody *wall = [BoxBody staticBody];
    BoxFixture *fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                                        CGSizeMake(kBoxWallWidth, kBoxWallThickness)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(self.view.bounds.size.width/2.0,
                                -kBoxWallThickness/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    // bottom
    wall = [BoxBody staticBody];
    fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                            CGSizeMake(kBoxWallWidth, kBoxWallThickness)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(self.view.bounds.size.width/2.0,
                                self.view.bounds.size.height+kBoxWallThickness/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    // left
    wall = [BoxBody staticBody];
    fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                            CGSizeMake(kBoxWallThickness, kBoxWallWidth)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(-kBoxWallThickness/2.0,
                                self.view.bounds.size.height/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    // right
    wall = [BoxBody staticBody];
    fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                            CGSizeMake(kBoxWallThickness, kBoxWallWidth)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(self.view.bounds.size.width+kBoxWallThickness/2.0,
                                self.view.bounds.size.height/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    
}

- (void)positionViews {
    
    [self.world enumerateBodies:^(BoxBody *body) {
        
        UIView *view = body.weakContext;
        
        if (view) {
            CGPoint p = body.position;
            CGFloat a = body.angle;
            CGAffineTransform r = CGAffineTransformMakeRotation(a);
            
            if ([view isKindOfClass:[AttractorView class]]) {
                AttractorView *av = (AttractorView*)view;
                
                view.center = CGPointMake(p.x, p.y);
                //av.rotatingView.transform = r; //CATransform3DMakeRotation(a, 0., 0., 1.);
                
            } else {
                view.center = CGPointMake(p.x, p.y);
            }
        }
    }];
}

- (void)containmentCheck {
    CGRect bounds = self.view.bounds;
    
    for (AttractorView *av in self.attractorViews) {
        BoxBody *body = av.body;
        CGPoint p = body.position;
        
        if (!CGRectContainsPoint(bounds, p)) {
            OPLog(@"%@ not in %@",NSStringFromCGPoint(p),NSStringFromCGRect(bounds));
            // put it back in the box
            CGFloat rx = 0.2 + RANDOM_FLOAT()*(0.8*bounds.size.width);
            CGFloat ry = 0.2 + RANDOM_FLOAT()*(0.8*bounds.size.height);
            
            body.position = CGPointMake(rx,ry);
        }
    }
    
    
}

- (void)boxTick {
    [self containmentCheck];
    [self.world step];
    [self positionViews];
}

- (void)startBoxWorld {
    OPLog(@"start box world");
    [self.displayLink invalidate];
    [self.world setAccelerometerEnabled:NO];
    self.displayLink = [CADisplayLink displayLinkWithTarget:self
                                                   selector:@selector(boxTick)];
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
}

- (void)stopBoxWorld {
    OPLog(@"stop box world");
    [self.displayLink invalidate];
    self.displayLink = nil;
}

-(void) setupFluffyRiver{
    FluffyRiverView *frv = [[FluffyRiverView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:frv];
    frv.backgroundColor = [UIColor clearColor];
    frv.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth );
    self.fluffyView = frv;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    // iphone vs. ipad view
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    OPLogMethod;
    [super viewDidLoad];
    
    [self positionViews];
    [self startBoxWorld];
    
    self.attractorViews = [[NSMutableArray alloc] initWithCapacity:1];
    self.world = [BoxWorld worldWithGravity:CGPointZero];
    
    if (self.wallBodies) {
        [self repositionWalls];
    } else {
        [self buildBoxWalls];
    }
    
    [self readSettingsFile];
    
    [self initBackground];
    
    // unused variable userDefaults?
    NSUserDefaults* userDefaults=[NSUserDefaults standardUserDefaults];
    //NSString* bgImage= [userDefaults stringForKey:@"bg"];
    // [self setBackgroundToImage:@"lockerRed.png"];
    [self setBackgroundToImage:@"Window.png"];
    // [self setBackgroundToImage:@"Default-Landscape~ipad.png"];
    //
    
    [self setupMenuBar];
    //    [self ratePromptCheck];
    //[self setupFluffyRiver];
    // [self showWelcomeAttractors];
    
    self.shouldShowPlusButton=false;
    
    // set up for custom dictionary
    [self showWelcomeAttractors];

    
    // Dark view
    UIView *v = [[UIView alloc] initWithFrame:self.view.bounds];
    v.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.8];
    v.alpha = 0.;
    v.autoresizesSubviews = NO;
    self.embeddedViewControllerBackdropView = v;
    
    
    if (0 < [self.attractorViews count]) {
        for (AttractorView *av in self.attractorViews) {
            [self.view addSubview:av];
            
            if (kAttractorViewStateOpen == av.state) {
                for (FilingView *fv in av.filingViews) {
                    [self.view addSubview:fv];
                }
            }
        }
    }
    }
    
    // iphone view
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        
    }
    
    
    NSLog(@"viewdidload");
    
}

- (void)viewDidUnload {
    OPLogMethod;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    OPLogMethod;
    
    [self recenterEmbeddedViewController];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    OPLogMethod;
    [super viewDidAppear:animated];
    [self repositionWalls];
}

- (void)viewWillDisappear:(BOOL)animated {
    OPLogMethod;
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    OPLogMethod;
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)recenterEmbeddedViewController {
    self.embeddedViewControllerBackdropView.bounds = self.view.bounds;
    self.embeddedViewControllerBackdropView.center = CGPointMake(self.view.bounds.size.width/2.,
                                                                 self.view.bounds.size.height/2.);
    UIView *cView = [[self.embeddedViewControllerBackdropView subviews] lastObject];
    cView.center = self.embeddedViewControllerBackdropView.center;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    
    //OPLogMethod;
    //OPLog(@"%@",NSStringFromCGRect(self.view.bounds));
    
    [UIView animateWithDuration:duration
                          delay:0.
                        options:0
                     animations:^{
                         
                         [self recenterEmbeddedViewController];
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    [self repositionWalls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark — Popover

- (void) showPopover:(UIButton*)sender{
    // display popover
    if (self.popoverController == nil) {
    } else {
        [self.popoverController dismissPopoverAnimated:YES];
    }
    
    PopOverViewController* uiviewcontroller;
    
    if (sender.tag==SETTINGS_BUTTON_MENUBAR)
        uiviewcontroller= [self settingsPopover];
    else if (sender.tag==INFO_BUTTON_MENUBAR)
        uiviewcontroller= [self infoPopover];
    else if (sender.tag==WORDPACK_BUTTON_MENUBAR)
        uiviewcontroller= [self wordpacksPopover];
    else
        return;
    
    uiviewcontroller.parent= self;
    
    UIPopoverController* popover= [[UIPopoverController alloc] initWithContentViewController:uiviewcontroller];
    popover.delegate = self;
    self.popoverController = popover;
    
    UIButton* buttonPressed= (UIButton*)sender;
    CGRect popoverRect= [self.view convertRect:[buttonPressed frame] fromView:[buttonPressed superview]];
    
    popoverRect.size.width = MIN(popoverRect.size.width, 100);
    [self.popoverController presentPopoverFromRect:popoverRect
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];

    
  
    
}


- (PopOverViewController*) infoPopover{
    AboutViewController* tips= [[AboutViewController alloc] initWithNibName:@"AboutViewController"
                                                                     bundle:[NSBundle mainBundle]];
    
    return tips;
}

- (PopOverViewController*) wordpacksPopover{
    WordpackPickerViewController* wordpackpicker= [[WordpackPickerViewController alloc] initWithNibName:@"WordpackPickerViewController"
                                                                                                 bundle:[NSBundle mainBundle]];
    
    return wordpackpicker;
}
- (UITableViewController*) wordpacksTable{
    WordpackPickerViewController* wordpackpicker= [[WordpackPickerViewController alloc] initWithNibName:@"WordpackPickerViewController"
                                                                                                 bundle:[NSBundle mainBundle]];
    UITableViewController *tablePicker = wordpackpicker;
    //return wordpackpicker;
    return tablePicker;
}


- (PopOverViewController*) settingsPopover{
    SettingsViewController* settingsVC= [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"
                                                                                 bundle:[NSBundle mainBundle]];
    return settingsVC;
}

- (void) loadWordPack:(NSString*)wordpack{
    //change bg color
    NSMutableArray* colors=[[NSMutableArray alloc] initWithObjects:
                            @"Castle.png",
                            @"Balcony.png",
                            @"Moonlight.png",
                            @"River.png",
                            @"Window.png",
                            @"Regina.png",
                            @"Dock.png",
                            @"Waterfront.png",
                            nil];
    
    NSString* path= [colors objectAtIndex:(arc4random()%[colors count])];
    [self setBackgroundToImage:path];
    
    OPLog(@"Loading wordpack: %@", wordpack);
    //    self.wordPackStr= [wordpack stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    
    //state variables
    self.wordPackStr= wordpack;
    words_completed_current_wordpack=0;
    words_in_current_wordpack=[wordpackDB numberOfWordsInWordPack:wordpack];
    NSLog(@"Current wordpack has %i words", words_in_current_wordpack);
    
    
    NSArray* words= [[wordpackDB getWordPackForWord:wordpack] words];
    [self removeAttractors];
    
    OPLog(@"Creating attractors:");
    for (WordDescription* word in words){
        NSLog(@"%@ ", word.fullword);
        
        for (NSString* item in word.affixes){
            LanguageWord* l= [LanguageWord alloc];
            l.term=item;
            l.wordpackStr= wordpack;
            AttractorView* aView=[self insertAttractorForWord:l];
            
            if ([[morphemeDB getTypeForMorpheme:item] isEqualToString:@"suffix"] || [[morphemeDB getTypeForMorpheme:item] isEqualToString:@"prefix"]){
                [aView makeMorpheme];
                
            } else {
                [self pinBody:aView];
            }
            
        }
    }
    
    if ([wordpack isEqualToString:@"Wildcard"]){
        [self showAddAttractorButton];
        //dismiss popover && create new attractor
        [self.popoverController dismissPopoverAnimated:TRUE];
        [self showNewAttractor];
    } else {
        self.shouldShowPlusButton=false;
        [self setupMenuBar];
    }
    
    
}


//Picks a random wordpack from the wordpack database
//Input: None
//Returns: None
- (void) loadRandomWordPack{
    int randomIndex= arc4random() % [wordpackDB.wordpackTitles count];
    NSString* randomWordpack= [wordpackDB.wordpackTitles objectAtIndex:randomIndex];
    NSLog(@"Loading random wordpack... wordpack: %@", randomWordpack);
    [self loadWordPack:randomWordpack];
}

//Captures a view of the entire screen and returns it
//Input: None
//Returns: UIImage* containing the screenshot
- (UIImage*) captureScreen{
    UIGraphicsBeginImageContext(self.view.frame.size);
	[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage* viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    return viewImage;
}

//Opens a tweetsheet preloaded with a screenshot of the current view
//Input: None
//Returns: None
//- (void) tweetsheetScreenshot{
//    UIImage* screenshot= [self captureScreen];

//    TWTweetComposeViewController* tweetSheet = [[TWTweetComposeViewController alloc] init];
//    [tweetSheet setInitialText:@"Just made a poem with Power of Words!"];
//    [tweetSheet addImage:screenshot];
//    tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result){
//        [self dismissModalViewControllerAnimated:YES];
//    };
//    [self presentModalViewController:tweetSheet animated:YES];
//}

//Opens a sheet enabling the user to send an email with a screenshot of the current view
//Input: None
//Returns: None
- (void) mailsheetScreenshot{
    UIImage* screenshot= [self captureScreen];
    
    MFMailComposeViewController* mailComposer= [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate=self;
    [mailComposer setToRecipients:[NSArray arrayWithObjects:@"hipnisse@panafold.com",nil]];
    [mailComposer setSubject:@"Vocabulary"];
    [mailComposer setMessageBody:@"<b>My iPad screenshot </b><br /><br /> <p style='color: gray; font-size: 12px;'>Self-test: <br /> What new words do I want to remember? <br />The challenge I answered:</p> <br />"
                          isHTML:YES];
    [mailComposer addAttachmentData:UIImagePNGRepresentation(screenshot) mimeType:@"image/png" fileName:@"poem.png"];
    
    [self presentModalViewController:mailComposer animated:YES];
}

//Called as part of MFMailDelegate protocol when mail view ends
//Input: None
//Output: None
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}

//Brings a random number (as defined by constant) of words from poem XML file into scene
//Input: None
//Returns: None
- (void) loadPoemWords{
    NSMutableArray* poemwords= [poemwordsDB getRandomPoemWordsNumLowFreq:NUM_RANDOM_POEMWORDS_LOW andNumHighFreq:NUM_RANDOM_POEMWORDS_HIGH];
    
    for (NSString* w in poemwords){
        LanguageWord* lw= [[LanguageWord alloc] init];
        lw.term=w;
        AttractorView* av=[self insertAttractorForWord:lw];
        [av fadeIn];
        
    }
}

//Shows an alert view notifying the user of the full version. Called when a wordpack is completed.
//Input: None
//Returns: None
- (void) notifyFullVersion{
    UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"Nice Work!"
                                                   message:@"Try a challenge. You can email the result."
                                                  delegate:self
                                         cancelButtonTitle:@"Onward!"
                                         otherButtonTitles:nil];
    
    alert.tag=UIALERT_FULL_VERSION;
    [alert show];
    
}

@end
