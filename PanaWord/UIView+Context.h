//
//  UIView+Context.h
//  PanaWord
//
//  Created by Panafold on 9/19/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView(Context)

@property (weak,nonatomic) id weakContext;

@end
