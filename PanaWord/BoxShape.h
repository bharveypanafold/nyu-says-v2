//
//  BoxShape.h
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BoxShape : NSObject

@property (nonatomic) CGFloat radius;

@end


@interface CircleShape : BoxShape

@property (nonatomic) CGPoint position;

+ (CircleShape*)circleWithRadius:(CGFloat)theRadius Position:(CGPoint)thePosition;

@end

@interface PolygonShape : BoxShape

@property (strong,nonatomic) NSArray *verticies;
@property (strong,nonatomic,readonly) NSArray *normals;
@property (nonatomic) CGPoint centroid;

+ (PolygonShape*)rectangleWithSize:(CGSize)size;

@end

@interface BoxFixture : NSObject {
    @private
    void* _b2Shape;
    void* _b2Fixture;
}

@property (strong,nonatomic) BoxShape *shape;
@property (nonatomic) CGFloat friction;
@property (nonatomic) CGFloat restitution;
@property (nonatomic) CGFloat density;

@property (nonatomic,getter = isSensor) BOOL sensor;

// collision filter

+ (BoxFixture*)fixtureWithShape:(BoxShape*)shape;

- (void)setShapeRadius:(CGFloat)radius;

@end