//
//  WordpackPickerViewController.h
//  CaliforniaEnglish
//
//  Created by Guillaume on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverViewController.h"

#define CELL_NAME_TAG 1

@interface WordpackPickerViewController :  PopOverViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray* listOfWordpacks;
@property (strong, nonatomic) IBOutlet UITableView* table;

- (void) buyPressed:(id)sender;

@end
