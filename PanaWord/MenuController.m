//
//  MenuController.m
//  PanaWord
//
//  Created by Panafold on 11/22/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "MenuController.h"
#import <QuartzCore/QuartzCore.h>

#import "LanguageWordPack.h"
#import "LanguageSource.h"
#import "LanguageTopic.h"

#import "PackPickerTableController.h"
#import "CardArrayViewController.h"

#import "AboutViewController.h"

@implementation MenuController

@synthesize imageView, pfView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Welcome";
        //self.contentSizeForViewInPopover = CGSizeMake(400., 316.);
        self.contentSizeForViewInPopover = CGSizeMake(768., 768.);
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)showController:(UIViewController*)vc showNavBar:(BOOL)showBar {
    vc.view.alpha = 0.;
    
    UIViewController *parent = self.parentViewController;
    CGRect b = vc.view.bounds;
    b.size.width = vc.contentSizeForViewInPopover.width;
    b.size.height = vc.contentSizeForViewInPopover.height;
    
    [UIView animateWithDuration:0.15
                          delay:0
                        options:0
                     animations:^{
                         self.view.alpha = 0.;
                     } completion:^(BOOL finished) {
                         
                         [self.navigationController setNavigationBarHidden:!showBar animated:YES];
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.
                                             options:0
                                          animations:^{
                                              
                                              parent.view.bounds = b;
                                              
                                          } completion:^(BOOL finished) {
                                              
                                              [self.navigationController pushViewController:vc
                                                                                   animated:NO];
                                              
                                              [UIView animateWithDuration:0.1
                                                               animations:^{
                                                                   vc.view.alpha = 1.;
                                                               }];
                                              
                                          }];
                     }];

}

- (void)showPackPicker {
    PackPickerTableController *vc = [[PackPickerTableController alloc] initWithNibName:nil bundle:nil];
    
    [self showController:vc showNavBar:YES];
}

- (void)showTips {
    
}

- (void)showCards {
    CardArrayViewController *vc = [[CardArrayViewController alloc] initWithNibName:nil bundle:nil];
        
    [self presentModalViewController:[[UINavigationController alloc] initWithRootViewController:vc]
                            animated:YES];//*/
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES
                                             animated:animated];
    
    CGRect R = CGRectMake(0., 0., self.contentSizeForViewInPopover.width, self.contentSizeForViewInPopover.height);
    
    if (!CGRectEqualToRect(self.parentViewController.view.bounds, R)) {
        self.view.alpha = 0.;
        
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    if (self.view.alpha == 0.) {
        
        UIViewController *parent = self.parentViewController;
        
        CGRect b = self.view.bounds;
        //OPLog(@"%@",NSStringFromCGRect(b));
        b.size.width = self.contentSizeForViewInPopover.width;
        b.size.height = self.contentSizeForViewInPopover.height;
        
        [UIView animateWithDuration:0.3
                              delay:0
                            options:0
                         animations:^{
                             
                             parent.view.bounds = b;
                             
                         } completion:^(BOOL finished) {
                             
                             [UIView animateWithDuration:0.2
                                              animations:^{
                                                  self.view.alpha = 1.;
                                              }];
                             
                         }];

        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    /*[self.navigationController setNavigationBarHidden:NO
                                       animated:animated];*/
    
    //OPLogMethod;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //OPLogMethod;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	//return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIDeviceOrientationPortraitUpsideDown);
}

#pragma mark - Actions

- (IBAction)wordPackButtonAction:(id)sender {
    
    [self showPackPicker];
}

- (IBAction)contextCardsButtonAction:(id)sender {
    
    [self showCards];
}

- (IBAction)aboutButtonAction:(id)sender {
    
    AboutViewController *vc = [[AboutViewController alloc] initWithNibName:nil bundle:nil];
    
    //[self.navigationController pushViewController:vc animated:YES];
    [self showController:vc showNavBar:NO];
}


@end
