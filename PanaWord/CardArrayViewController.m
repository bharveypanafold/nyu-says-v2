//
//  CardArrayViewController.m
//  PanaWord
//
//  Created by Panafold on 12/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "CardArrayViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>

#import "LanguageSource.h"
#import "LanguageTopic.h"
#import "LanguageWord.h"
#import "LanguageWordPack.h"
#import "LanguageFiling.h"

#import "CardView.h"
#import "UIColor-Extras.h"

#import "PagedArrayScrollView.h"

#define kCardButtonSize 120.


@interface CardArrayViewController() <UIScrollViewDelegate,MFMailComposeViewControllerDelegate,PagedArrayScrollViewDelegate> {
    
    CGRect cardBounds;
}


@property (strong,nonatomic) PagedArrayScrollView *arrayView;

@property (strong,nonatomic) NSMutableArray *sortedWords;

@property (strong,nonatomic) UILabel *pageCountLabel;
@property (strong,nonatomic) UIButton *checkButton;
@property (strong,nonatomic) UIButton *emailButton;

//- (void)composeEmail;


@end


@implementation CardArrayViewController

@synthesize sortedWords;
@synthesize pageCountLabel;
@synthesize arrayView;
@synthesize checkButton,emailButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.title = @"Context Cards";
        
        self.sortedWords = nil;
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                               target:self
                                                                                               action:@selector(doneAction:)];
        
        
        self.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)doneAction:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}


#pragma mark - 

- (void)sortWords {
    
    self.sortedWords = [[NSMutableArray alloc] initWithCapacity:1];
    
    NSMutableArray *checkedWords = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *workingWords = [NSMutableArray arrayWithCapacity:1];
    
    NSMutableArray *allWords = [NSMutableArray arrayWithCapacity:1];

    for (LanguageTopic *topic in [LanguageSource sharedInstance].packs) {
        for (LanguageWordPack *pack in topic.subTopics) {
            [allWords addObjectsFromArray:pack.words];
        }
    }
    
    for (LanguageWord *word in allWords) {
        if (word.hasMemorized) {
            [checkedWords addObject:word];
        } else {
            [workingWords addObject:word];
        }
    }
    
    // should sort memorized words and randomize working words
    [checkedWords sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        LanguageWord *word1 = obj1;
        LanguageWord *word2 = obj2;
        
        return [word1.term compare:word2.term];
    }];
    
    [self.sortedWords addObjectsFromArray:workingWords];
    [self.sortedWords addObjectsFromArray:checkedWords];
}


- (void)hideCardUI {
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.checkButton.alpha = 0.;
                         self.emailButton.alpha = 0.;
                         self.pageCountLabel.alpha = 1.;
                     }];
}

- (void)showCardUI {
    
    NSInteger currentPage = (self.arrayView.contentOffset.x+self.arrayView.bounds.size.width/2.) / self.arrayView.bounds.size.width;
    
    CardView *cv = (CardView*)[self.arrayView viewForIndex:currentPage];
    //CardView *cv = [self.cardViewMap objectForKey:[NSNumber numberWithInteger:currentPage]];
    
    if (cv) {
        if (cv.word.hasMemorized) {
            self.checkButton.selected = YES;
        } else {
            self.checkButton.selected = NO;
        }
    }
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.checkButton.alpha = 1.;
                         self.emailButton.alpha = 1.;
                         self.pageCountLabel.alpha = 0.;
                     }];
}

- (void)cardCheckAction:(UIButton*)sender {
        
    NSInteger currentPage = (self.arrayView.contentOffset.x+self.arrayView.bounds.size.width/2.) / self.arrayView.bounds.size.width;
    
    CardView *cv = (CardView*)[self.arrayView viewForIndex:currentPage];
    
    BOOL isChecked = cv.word.hasMemorized;
    
    if (isChecked) {
        cv.word.memorized = NO;
        sender.selected = NO;
    } else {
        cv.word.memorized = YES;
        sender.selected = YES;
        
        [self hideCardUI];
        
        CGPoint c = cv.center;
        
        [UIView animateWithDuration:0.3
                              delay:0.
                            options:0
                         animations:^{
                             
                             cv.transform = CGAffineTransformMakeScale(0.7, 0.7);
                             
                         } completion:^(BOOL finished) {
                             
                             [UIView animateWithDuration:0.4
                                                   delay:0.
                                                 options:0
                                              animations:^{
                                                  
                                                  CGPoint newCenter = c;
                                                  newCenter.x += self.view.bounds.size.width*1.5;
                                                  cv.center = newCenter;
                                                  
                                                  [self.sortedWords removeObjectAtIndex:currentPage];
                                                  
                                                  [self.arrayView animateCardFromIndex:currentPage toIndex:0];
                                                  
                                                  
                                              } completion:^(BOOL finished) {
                                                  
                                                  
                                                  [self showCardUI];
                                              }];
                             
                         }];//*/
    }
}

- (void)cardEmailAction:(id)sender {
    
}

#pragma mark - PagedArrayScrollView Delegate

- (UIView*)viewForIndex:(NSInteger)index {
    
    LanguageWord *word = [self.sortedWords objectAtIndex:index];
    
    //OPLog(@"viewForIndex: %i %@",index,word.term);
    
    CardView *cv = [[CardView alloc] initWithFrame:cardBounds];
    
    cv.word = word;
    cv.backgroundColor = [UIColor whiteColor];
    cv.viewController = self;
    cv.autoresizingMask = UIViewAutoresizingNone;
        
    [cv.layer setValue:[NSNumber numberWithInteger:index]
                forKey:@"card number"];
        
    return cv;
}

- (NSUInteger)numberOfPages {
    return [self.sortedWords count];
}

- (void)recycleViewAtIndex:(NSInteger)index {
    
    //[self.cardViewMap removeObjectForKey:[NSNumber numberWithInteger:index]];
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    
    OPLogMethod;        
    UIView *v = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    self.view = v;
    v.backgroundColor = [UIColor whiteColor];
    
    //v.layer.borderColor = [UIColor redColor].CGColor;
    //v.layer.borderWidth = 3.;
    
    CGRect f = self.view.bounds;
    f.size.height -= (124.);
    
    cardBounds = CGRectMake(0., 0., f.size.width, f.size.height-44.);
    
    PagedArrayScrollView *sv = [[PagedArrayScrollView alloc] initWithFrame:f];
    [self.view addSubview:sv];
    sv.pageDelegate = self;
    sv.delegate = self;
    sv.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    sv.autoresizesSubviews = NO;
    sv.scrollsToTop = NO;
    self.arrayView = sv;
        
    sv.layer.borderWidth = 2.;
    sv.layer.borderColor = [UIColor whiteColor].CGColor;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., 300., 60.)];
    self.pageCountLabel = label;
    label.font = [UIFont boldSystemFontOfSize:32.];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor darkGrayColor];
    label.backgroundColor = [UIColor colorWithWhite:1. alpha:0.8];
    label.text = @"-/-";
    //label.layer.borderColor = [UIColor darkGrayColor].CGColor;
    //label.layer.borderWidth = 2.;
    label.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    [self.view addSubview:label];
    
    label.center = CGPointMake(self.view.bounds.size.width/2.,
                               self.view.bounds.size.height-label.bounds.size.height-8.);
    
    UIButton *button = nil;
    // CHECKMARK Button
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0., 0., kCardButtonSize, kCardButtonSize);
    [self.view addSubview:button];
    self.checkButton = button;
    
    //button.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //button.layer.borderWidth = 1.;
    button.alpha = 0.;
    button.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin);
    
    [button setImage:[UIImage imageNamed:@"check_light"]
            forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"check_dark"]
            forState:UIControlStateSelected];
    [button addTarget:self
               action:@selector(cardCheckAction:)
     forControlEvents:UIControlEventTouchUpInside];
    
    // EMAIL Button
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0., 0., kCardButtonSize, kCardButtonSize);
    [self.view addSubview:button];
    self.emailButton = button;
    [button addTarget:self
               action:@selector(composeEmail)
     forControlEvents:UIControlEventTouchUpInside];
    
    //button.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //button.layer.borderWidth = 1.;
    button.alpha = 0.;
    button.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin);
    
    [button setImage:[UIImage imageNamed:@"email_dark"]
            forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"email_blue"]
            forState:UIControlStateHighlighted];
    [button addTarget:self
               action:@selector(cardEmailAction:)
     forControlEvents:UIControlEventTouchUpInside];

    self.checkButton.frame = CGRectMake(2.,
                                        self.view.bounds.size.height-kCardButtonSize-2.,
                                        kCardButtonSize,
                                        kCardButtonSize);
    
    self.emailButton.frame = CGRectMake(self.view.bounds.size.width-kCardButtonSize-2.,
                                        self.view.bounds.size.height-kCardButtonSize-2.,
                                        kCardButtonSize,
                                        kCardButtonSize);
    
    [self sortWords];    


}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self sortWords];    
    OPLogMethod;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];   
    
    OPLogMethod;
    [self.arrayView setNeedsLayout];
    
    [self showCardUI];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    OPLogMethod;

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    OPLogMethod;

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	//return YES;
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIDeviceOrientationPortraitUpsideDown);
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    
    [UIView animateWithDuration:duration
                          delay:0.
                        options:0
                     animations:^{
                         
                         self.arrayView.frame = CGRectMake(self.view.bounds.size.width/2.-self.arrayView.bounds.size.width/2.,
                                                           0.,
                                                           self.arrayView.bounds.size.width,
                                                           self.arrayView.bounds.size.height);
                         
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
}


#pragma mark - UIScrollView delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [self hideCardUI];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    NSInteger numberOfPages = scrollView.contentSize.width / scrollView.bounds.size.width;
    NSInteger currentPage = (scrollView.contentOffset.x+scrollView.bounds.size.width/2.) / scrollView.bounds.size.width + 1;
    
    self.pageCountLabel.text = [NSString stringWithFormat:@"%i/%i",currentPage,numberOfPages];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    [self showCardUI];
}


#pragma mark - Email

- (void)composeEmail {
    
    NSInteger currentPage = (self.arrayView.contentOffset.x+self.arrayView.bounds.size.width/2.) / self.arrayView.bounds.size.width;
    CardView *cv = (CardView*)[self.arrayView viewForIndex:currentPage];
    
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"PanaWord Document Email!"];
    
    // Fill out the email body text
    NSMutableString *emailBody = [NSMutableString stringWithCapacity:1024];
        
    NSString *rawDataPath = [[NSBundle mainBundle] pathForResource:@"cardtemplate" ofType:@"html"];
    NSData *rawData = [NSData dataWithContentsOfFile:rawDataPath];
    NSString *template = [[NSString alloc] initWithBytes:[rawData bytes]
                                                   length:[rawData length]
                                                 encoding:NSUTF8StringEncoding];
    
    NSString *replacement = @"";
    if (cv.word.term)
        replacement = cv.word.term;
    template = [template stringByReplacingOccurrencesOfString:@"%%word%%"
                                                   withString:replacement];
    
    if (cv.word.note) {
        replacement = cv.word.note;        
    } else {
        replacement = @"";
    }
    template = [template stringByReplacingOccurrencesOfString:@"%%note%%"
                                                   withString:replacement];
    
    if (cv.word.definition) {
        replacement = cv.word.definition;
    } else {
        replacement = @"";
    }
    template = [template stringByReplacingOccurrencesOfString:@"%%definition%%"
                                                   withString:replacement];
    
    if (cv.word.gloss) {
        replacement = cv.word.gloss;
    } else {
        replacement = @"";
    }
    template = [template stringByReplacingOccurrencesOfString:@"%%gloss%%"
                                                   withString:replacement];
    
    // this is seriously fragile!
    if ([cv.filingViews count] >= 3) {
        
        UIView *v = [cv.filingViews objectAtIndex:0];
        UILabel *label = [v.layer valueForKey:@"back"];
        NSString *string = label.text;
        
        if (!string)
            string = @"(none)";
        
        template = [template stringByReplacingOccurrencesOfString:@"%%filing1%%"
                                                       withString:string];
        
        v = [cv.filingViews objectAtIndex:1];
        label = [v.layer valueForKey:@"back"];
        
        string = label.text;
        if (!string)
            string = @"(none)";

        template = [template stringByReplacingOccurrencesOfString:@"%%filing2%%"
                                                       withString:string];

        v = [cv.filingViews objectAtIndex:2];
        label = [v.layer valueForKey:@"back"];
        
        string = label.text;
        if (!string)
            string = @"(none)";
        
        template = [template stringByReplacingOccurrencesOfString:@"%%filing3%%"
                                                       withString:string];
        
    } else {
        
        template = [template stringByReplacingOccurrencesOfString:@"%%filing1%%"
                                                       withString:@"(no filing)"];
        
        template = [template stringByReplacingOccurrencesOfString:@"%%filing2%%"
                                                       withString:@"(no filing)"];

        template = [template stringByReplacingOccurrencesOfString:@"%%filing3%%"
                                                       withString:@"(no filing)"];

    }
    
    
    
    
    
    [emailBody appendFormat:template];
    
    [picker setMessageBody:emailBody 
                    isHTML:YES];
    
    
    [self presentModalViewController:picker 
                            animated:YES];
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message 
// field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller 
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError*)error {    
	
    [self dismissModalViewControllerAnimated:YES];
}

@end
