//
//  MorphemeDatabase.h
//  EnglishDiscovery
//
//  Created by Guillaume on 5/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LanguageWord.h"

@interface MorphemeDatabase : NSObject
@property (strong, nonatomic) NSMutableArray* morphemes;

- (BOOL) morphemeInDatabase:(NSString*)m;
- (void) addMorpheme:(LanguageWord*)l;
- (LanguageWord*) getMorpheme:(NSString*)m;
- (NSString*) getTypeForMorpheme:(NSString*)m;
- (NSString*) getDefinitionForMorpheme:(NSString*)m;

@end
