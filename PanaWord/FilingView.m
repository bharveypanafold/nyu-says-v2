//
//  FilingView.m
//  PanaWord
//
//  Created by Panafold on 10/25/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "FilingView.h"
#import "LanguageFiling.h"
#import <QuartzCore/QuartzCore.h>


@implementation FilingView

@synthesize tapRecogznier,panRecognizer,longPressRecognizer,filing,shapeLayer,body,attractorView, imageLayer, image, colorLayer;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CAShapeLayer *l = [CAShapeLayer layer];
        self.shapeLayer = l;
        [self.layer addSublayer:l];
        l.frame = self.bounds;
        
        //[self markAsUnread];
        //l.strokeColor = [UIColor lightGrayColor].CGColor;
        //l.lineWidth = 1.0;
        
        NSMutableArray* colors= [[NSMutableArray alloc] initWithCapacity:10];
        [colors addObject:[UIColor greenColor]];
        [colors addObject:[UIColor redColor]];
        [colors addObject:[UIColor blueColor]];
        [colors addObject:[UIColor purpleColor]];
        [colors addObject:[UIColor cyanColor]];
        [colors addObject:[UIColor magentaColor]];
        [colors addObject:[UIColor brownColor]];
        
        float radius=30.0;
        self.colorLayer= [CALayer layer];
        [self.colorLayer setMasksToBounds:YES];
        UIColor* c= [colors objectAtIndex:arc4random()%[colors count]];
        [self.colorLayer setBackgroundColor:[c CGColor]];
        [self.colorLayer setCornerRadius:radius];
        [self.colorLayer setFrame:CGRectMake(0, 0, radius*2, radius*2)];
        //[self.layer addSublayer:self.colorLayer];
    }
    return self;
}

- (void)setImageForFiling:(NSString*)path
{
    [self.imageLayer removeFromSuperlayer];

    self.imageLayer= [CALayer layer];
    image=[UIImage imageNamed:path];
    
    self.imageLayer.contents= (id) image.CGImage;
    self.imageLayer.frame = CGRectMake(0, 0, 60, 60);
    
    [self.layer addSublayer:self.imageLayer];  
}

- (void)addDLabel{   
    [self setImageForFiling:@"filing-magnet-dictionary.png"];
}

- (void)addImageLabel{   
    [self setImageForFiling:@"filing-magnet-image.png"];
}


- (void) markAsRead{
    [self setImageForFiling:@"filing-magnet.png"];
  //  self.filing.read = YES;
}

- (void) markAsUnread{
    [self setImageForFiling:@"filing-magnet.png"];
//    self.filing.read = NO;
}

@end
