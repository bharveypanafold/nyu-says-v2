//
//  ContextCardViewController.m
//  PanaWord
//
//  Created by Panafold on 11/29/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "ContextCardViewController.h"
#import "UIColor-Extras.h"
#import "CardView.h"
#import <QuartzCore/QuartzCore.h>

@interface ContextCardViewController()

@property (strong,nonatomic) CardView *cardView;

@end

@implementation ContextCardViewController

@synthesize word;
@synthesize cardView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    OPLogMethod;
    CGRect R = [[UIScreen mainScreen] bounds];
    
    UIScrollView *v = [[UIScrollView alloc] initWithFrame:R];
    self.view = v;
    
    v.layer.borderColor = [UIColor randomColor].CGColor;
    v.layer.borderWidth = 1.;
    
    CardView *cv = [[CardView alloc] initWithFrame:self.view.bounds];
    cv.word = self.word;
    cv.backgroundColor = [UIColor whiteColor];
    //[cv layoutData];
    [v addSubview:cv];
    self.cardView = cv;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    OPLogMethod;
    UIScrollView *sv = (UIScrollView*)self.view;
    sv.contentSize = self.cardView.bounds.size;
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
