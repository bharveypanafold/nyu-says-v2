//
//  BoxBody.h
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoxShape.h"

@interface BoxBody : NSObject {
@private
    void *_bodyRef;
    void *_bodyDef;
}

@property (weak,nonatomic) id weakContext;
@property (nonatomic) CGPoint position;
@property (nonatomic) CGFloat angle;
@property (nonatomic,readonly) CGFloat mass;

@property (nonatomic) CGFloat linearDamping;
@property (nonatomic) CGFloat angularDamping;

@property (strong,nonatomic) NSMutableArray *fixtures;


+ (BoxBody*)dynamicBody;
+ (BoxBody*)staticBody;
+ (BoxBody*)kinematicBody;

- (void)addFixture:(BoxFixture*)fixture;
- (void)setActive:(bool)flag;
// - enumerate fixtures

@end
