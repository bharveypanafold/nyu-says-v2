//
//  LanguageWord.m
//  PanaWord
//
//  Created by Panafold on 10/10/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "LanguageWord.h"
#import "LanguageFiling.h"
#import "SimpleVerb.h"
#import "LanguageWordPack.h"

@implementation LanguageWord

@synthesize filings;
@synthesize note;
@synthesize definition;
@synthesize gloss;
@synthesize wordPack;
@synthesize term;
@synthesize wordpackStr;
@synthesize memorized;
@synthesize type;
@synthesize def;
@synthesize name;

- (id)initWithCoder:(NSCoder *)coder {
    
    self = [super init];
    if (self) {
        self.filings = [coder decodeObjectForKey:@"filings"];
        self.note = [coder decodeObjectForKey:@"note"];
        self.definition = [coder decodeObjectForKey:@"definition"];
        self.gloss = [coder decodeObjectForKey:@"gloss"];
        self.term = [coder decodeObjectForKey:@"term"];
        self.memorized = [coder decodeBoolForKey:@"memorized"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.filings
                 forKey:@"filings"];
    
    [coder encodeObject:self.note
                 forKey:@"note"];
    
    [coder encodeObject:self.definition
                 forKey:@"definition"];
    
    [coder encodeObject:self.gloss
                 forKey:@"gloss"];
    
    [coder encodeObject:self.term
                 forKey:@"term"];
    
    [coder encodeBool:self.memorized
               forKey:@"memorized"];
    
}


+ (LanguageWord*)wordFromDictionary:(NSDictionary*)dict {
    LanguageWord *word = [[self alloc] init];
    word.term = [dict valueForKey:@"term"];
    word.definition = [dict valueForKey:@"definition"];
    word.gloss = [dict valueForKey:@"gloss"];
   //The Filing Dictionary
    word.filings = [NSMutableArray arrayWithCapacity:1];
    for (NSDictionary *def in [dict valueForKey:@"filings"]) {
    //Add filings to dictionary
        [word.filings addObject:[LanguageFiling filingFromDictionary:def]];
    }
    
    return word;
}

- (void)loadFilingsCompletion:(LanguageWordFilingCompletionBlock)completion {
 
    NSString *uid = [[[UIApplication sharedApplication] delegate] performSelector:@selector(uniqueAppIdentifier)];
    NSString* slashtag=@"nyu";
    
    if (self.term==NULL){
        self.term=@"";
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"http://107.22.253.19:8080/%@?lang=en&slashtag=%@&wordpack=%@&uid=%@",self.term, slashtag, self.wordpackStr,uid];
    urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSLog(@"%@", urlStr);
    
    [SimpleVerb sendGET:[NSURL URLWithString:urlStr]
             completion:^(id data, NSURLResponse *response, NSError *error) {
                 
                 BOOL success = NO;
                 if (data) {
                     NSError *jsonError = nil;
                     id rs = [NSJSONSerialization JSONObjectWithData:data
                                                             options:0
                                                               error:&jsonError];
                     
                     if (jsonError) {
                         OPLog(@"jsonError: %@",jsonError);
                         OPLog(@"------");
                         OPLog(@"%@",[NSString stringWithCString:[data bytes] encoding:NSUTF8StringEncoding]);
                         OPLog(@"------");
                     } else {
                         success = YES;
                         //OPLog(@"%@",data);
                         //OPLog(@"%@",[NSString stringWithCString:[data bytes] encoding:NSUTF8StringEncoding]);
                         NSMutableDictionary *uidFilingMap = [NSMutableDictionary dictionaryWithCapacity:1];
                         for (LanguageFiling *f in self.filings) {
                             NSString *uid = [f.content valueForKey:@"filing_id"];
                             if (uid)
                                 [uidFilingMap setValue:f
                                                 forKey:uid];
                         }
                         
                         if (!self.filings)
                             self.filings = [NSMutableArray arrayWithCapacity:1];
                         
                         for (NSDictionary *dict in rs) {
                             NSString *uid = [dict valueForKey:@"filing_id"];
                             if (uid) {
                                 if ([uidFilingMap valueForKey:uid]) {
                                     // we already have this one
                                     // maybe update it? or just ignore for now.
                                 } else {
                                     // new filing!
                                     LanguageFiling *aFiling = [LanguageFiling filingFromDictionary:dict];
                                     aFiling.word = self;
                                     [self.filings addObject:aFiling];
                                 }
                                 
                             }
                         }
                     }
                 }
                 
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                     OPLog(@"load complete.");
                     completion(success, self);
                 }];
             }];
}

@end
