//
//  NSData+HMAC.h
//  ColorPad
//
//  Created by Panafold on 4/7/11.
//  Copyright 2011 OrangePetal, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(GTMBase64)
+ (NSData *)GTMBase64encodeData:(NSData *)data;
+ (NSData *)GTMBase64decodeData:(NSData *)data;

+ (NSData *)GTMBase64encodeBytes:(const void *)bytes length:(NSUInteger)length;
+ (NSData *)GTMBase64decodeBytes:(const void *)bytes length:(NSUInteger)length;

+ (NSString *)GTMBase64stringByEncodingData:(NSData *)data;
+ (NSString *)GTMBase64stringByEncodingBytes:(const void *)bytes length:(NSUInteger)length;
+ (NSData *)GTMBase64decodeString:(NSString *)string;
@end

@interface NSMutableData(OPHMACAdditions)
+ (id)dataWithCapacity:(NSUInteger)aNumItems HMACSHA1data:(NSMutableData*)hmac withKey:(NSString*)key;
- (void)appendData:(NSData *)otherData updatingHMAC:(NSMutableData*)hmac;
- (NSData*)finalizeHMACSHA1:(NSMutableData*)hmac;
@end


@interface NSData(OPHMACAdditions)
- (NSData*)HMACSHA1ValueWithKey:(NSString*)key;
- (NSData*)SHA1;
- (NSString *)gtm_hexString;
@end
