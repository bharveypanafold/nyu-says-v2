//
//  FilingView.h
//  PanaWord
//
//  Created by Panafold on 10/25/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AttractorView;
@class LanguageFiling;
@class BoxBody;
@class CAShapeLayer;
@class CATextLayer;

@interface FilingView : UIView

@property (weak,nonatomic) UITapGestureRecognizer *tapRecogznier;
@property (weak,nonatomic) UIPanGestureRecognizer *panRecognizer;
@property (weak,nonatomic) UILongPressGestureRecognizer *longPressRecognizer;

@property (weak,nonatomic) AttractorView *attractorView;
@property (weak,nonatomic) LanguageFiling *filing;
@property (weak,nonatomic) BoxBody *body;

@property (strong,nonatomic) CAShapeLayer *shapeLayer;
@property (strong,nonatomic) CALayer *imageLayer;
@property (strong,nonatomic) CALayer *colorLayer;
@property (strong,nonatomic) UIImage *image;

- (void)addDLabel;
- (void)addImageLabel;

- (void)setImageForFiling:(NSString*)path;
- (void)markAsRead;
- (void)markAsUnread;
@end
