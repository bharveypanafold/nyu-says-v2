//
//  BoxWorld.h
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoxShape.h"
#import "BoxBody.h"
#import "BoxJoint.h"

#define kBoxWorldPixelScale 82.0

#ifdef __cplusplus
class b2World;
class b2Body;
#endif

typedef void(^BoxBodyEnumerationBlock)(BoxBody* body);

@interface BoxWorld : NSObject {
@private
    UIAccelerationValue	g[3];
    CFMutableDictionaryRef mouseJointMap;
#ifdef __cplusplus
    b2World *world;
    b2Body *walls[4];
#endif
    BOOL accelerometerEnabled;
}


+ (BoxWorld*)worldWithGravity:(CGPoint)gravity;

- (void)step;

- (void)setAccelerometerEnabled:(BOOL)accelerometerEnabled;
- (BOOL)isAccelerometerEnabled;

- (void)addBody:(BoxBody*)body;
- (void)removeBody:(BoxBody*)body;
// enumerate bodies
- (void)enumerateBodies:(BoxBodyEnumerationBlock)block;

- (void)addJoint:(BoxJoint*)joint;
- (void)removeJoint:(BoxJoint*)joint;

@end
