//
//  LanguageTopic.h
//  PanaWord
//
//  Created by Panafold on 11/28/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageTopic : NSObject

@property (strong,nonatomic) NSMutableArray *subTopics;
@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSString *englishTitle;

@end
