//
//  SettingsViewController.h
//  CaliforniaEnglish
//
//  Created by Guillaume on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverViewController.h"

@interface SettingsViewController : PopOverViewController

-(IBAction)toggleBG1:(id)sender;
-(IBAction)toggleBG2:(id)sender;
-(IBAction)toggleBG3:(id)sender;
-(IBAction)toggleBG4:(id)sender;

@end
