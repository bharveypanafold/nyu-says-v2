//
//  LanguageWordPack.m
//  PanaWord
//
//  Created by Panafold on 10/10/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "LanguageWordPack.h"

@implementation LanguageWordPack

@synthesize dictionaryManager;

@synthesize words;
@synthesize title;
@synthesize xmlSourceFile;
@synthesize unlocked;

@synthesize parser;
@synthesize setword;
@synthesize setdefinition;

- (id) init{
    self = [super init];
    if (self) {
        self.words = [[NSMutableArray alloc] initWithCapacity:10];
    }
    return self;
    
}

- (id)initWithDictManager:(DictionaryManager*)dict{
    
    self = [super init];
    if (self) {
        self.dictionaryManager=dict;
        self.words = [[NSMutableArray alloc] initWithCapacity:10];
    }
    return self;
}

- (void) loadWords{
    NSLog(@"Parsing file %@ into wordpack %@", self.title, self.xmlSourceFile);
    /* NSURL* url= [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"http://107.22.253.19/stanford-xml/%@", self.xmlSourceFile]];
     self.parser= [[NSXMLParser alloc] initWithContentsOfURL:url];
     */
    
    //reading wordpack locally
    self.parser=[[NSXMLParser alloc] initWithData:[NSData dataWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"wordpacks.xml"]]];
    [self.parser setDelegate:self];
    [self.parser parse];
    
    NSLog(@"%i total words loaded from remote wordpack xml file.", [self.words count]);
    
    
}

- (id)initWithCoder:(NSCoder *)coder {
    
    self = [super init];
    if (self) {
        self.words = [coder decodeObjectForKey:@"words"];
        self.title = [coder decodeObjectForKey:@"title"];
        self.unlocked = [coder decodeBoolForKey:@"unlocked"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.words
                 forKey:@"words"];
    
    [coder encodeObject:self.title
                 forKey:@"title"];
    
    [coder encodeBool:self.unlocked
               forKey:@"unlocked"];
    
}

#pragma mark — NSXMLParser delegate

- (void)parserDidStartDocument:(NSXMLParser *)parser{
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
}

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict {
    
    //NSLog(@"began element %@", elementName);
    
    if ([elementName isEqualToString:@"word"]){
        self.setword=YES;
    } else if ([elementName isEqualToString:@"definition"]){
        self.setdefinition=YES;
    }
    
}


- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"word"]){
        self.setword=NO;
    } else if ([elementName isEqualToString:@"definition"]){
        self.setdefinition=NO;
    }
    
    
    //NSLog(@"ended element %@", elementName);
    
}

- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string{
    NSLog(@"Added dWE'RE IN");
    if (self.setword){
        [self.words addObject:string];
        NSLog(@"Added word: %@", string);
    } else if (self.setdefinition){
        NSLog(@"Added DEFin word: %@", [self.words lastObject]);
//if(!lastob)
        [self.dictionaryManager setDefinition:string ForWord:[self.words lastObject]];
        NSLog(@"Added definition LANGWORD: %@ for word: %@", string, [self.words lastObject]);
    }
    
    
}

- (int) length{
    return [self.words count];
}
@end
