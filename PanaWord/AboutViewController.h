//
//  AboutViewController.h
//  PanaWord
//
//  Created by Panafold on Summer 2013.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "PFViewController.h"
#import "PopOverViewController.h"

@interface AboutViewController : PopOverViewController  <UIScrollViewDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic) BOOL animatingOffset;

- (IBAction)pageControlAction:(id)sender;
- (void)scrollViewDidScroll:(UIScrollView *)ascrollView;
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

- (IBAction)composeEmail:(id)sender;
- (IBAction)twitterButtonAction:(id)sender;
- (IBAction)supportButtonAction:(id)sender;
- (IBAction)creditButtonAction:(id)sender;
- (IBAction)facebookWebsite:(id)sender;
- (IBAction)panafoldWebsite:(id)sender;

- (void)openLinkInSafari:(NSString*) url;
@end
