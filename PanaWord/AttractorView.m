//
//  AttractorView.m
//  PanaWord
//
//  Created by Panafold on 10/21/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "AttractorView.h"
#import <QuartzCore/QuartzCore.h>
#import "FilingView.h"

#import "LanguageWord.h"

#import "BoxBody.h"
#import "BoxShape.h"

@implementation AttractorView

@synthesize tapRecognizer,panRecognizer,longPressRecognizer,word,filingViews,body,label, defLabel;
@synthesize type;
@synthesize shapeLayer;
@synthesize state;
@synthesize fluffiesRunning;
@synthesize deleteButton;
@synthesize radius;
@synthesize rotatingView;
@synthesize pinchRecognizer;
@synthesize glossFiling;
@synthesize touched;
@synthesize isMorpheme;
@synthesize imgView;
@synthesize morphemeComponents;
@synthesize isCompleted;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.touched=NO;
        self.isMorpheme=NO;
        self.isCompleted=NO;
        self.type=@"normal";
        
        self.filingViews = [NSMutableArray arrayWithCapacity:1];
        
        self.rotatingView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., frame.size.width, frame.size.height)];
        [self addSubview:self.rotatingView];
        
        //self.rotatingView.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
        
        //CAShapeLayer *l = [CAShapeLayer layer];
        //l.frame = self.bounds;
        //l.borderColor = [UIColor blackColor].CGColor;
        //l.borderWidth = 2.;
        //self.rotatingView.layer.borderWidth = 1.;
        //self.rotatingView.layer.borderColor = [UIColor redColor].CGColor;
        
        //self.shapeLayer = l;
        //[self.rotatingView.layer addSublayer:l];
        
        [self setImage];
        [self setColorLayer];
        
        //name label
        //self.label = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., frame.size.width, frame.size.height)];
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., frame.size.width-20, 64.)];

       // self.label = [[UILabel alloc] initWithFrame:CGRectMake(-20., 100., frame.size.width, frame.size.height)];
        self.label.center = CGPointMake(frame.size.width/2.0, frame.size.height/2.0);
        //self.label.center = CGPointMake(-20, 30 );
        self.label.center = CGPointMake(frame.size.width/2.0, frame.size.height/2.0);

        self.label.font = [UIFont systemFontOfSize:19.];
        self.label.font = [UIFont fontWithName:@"Calibri" size:23];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.textColor = [UIColor whiteColor];
        self.label.textAlignment = UITextAlignmentCenter;
        self.label.adjustsFontSizeToFitWidth = YES;
        self.label.minimumFontSize = 12.;
        self.label.numberOfLines = 2;
        self.label.lineBreakMode = UILineBreakModeWordWrap;
        [self.rotatingView addSubview:self.label];
        
        
        //definition label
        self.defLabel = [[UILabel alloc] initWithFrame:CGRectMake(0., 0., frame.size.width, frame.size.height)];
        self.defLabel.center = CGPointMake(frame.size.width/2.0, frame.size.height/2.0 + 20);
        self.defLabel.font = [UIFont systemFontOfSize:12.];
        self.defLabel.backgroundColor = [UIColor clearColor];
        self.defLabel.textColor = [UIColor grayColor];
        self.defLabel.textAlignment = UITextAlignmentCenter;
        self.defLabel.adjustsFontSizeToFitWidth = YES;
        self.defLabel.minimumFontSize = 10;
        self.defLabel.numberOfLines = 1;
        self.defLabel.lineBreakMode = UILineBreakModeWordWrap;
        [self.rotatingView addSubview:self.defLabel];
        
        self.state = kAttractorViewStateClosed;
        
        self.shapeLayer.strokeColor = [UIColor darkGrayColor].CGColor;
        
        self.fluffiesRunning = NO;
        
        self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.deleteButton setImage:[UIImage imageNamed:@"DeleteIcon"]
                           forState:UIControlStateNormal];
        self.deleteButton.frame = CGRectMake(-10., -10., 50., 50.);
        //[self addSubview:self.deleteButton];
        
    }
    return self;
}

-(void)setColorLayer{
    CALayer* colorlayer= [CALayer layer];
    colorlayer.opacity=0.0f;
    colorlayer.opaque=NO;
    colorlayer.backgroundColor= [UIColor blueColor].CGColor;
    
    [self.rotatingView.layer addSublayer:colorlayer];
}

-(void)setImage{
    NSString* attractorImage;
    
    if (self.isCompleted){
        attractorImage=@"magnet-completed.png";    
    } else if (self.isMorpheme){
       // attractorImage=@"medium-magnet.png";
    } else {
        attractorImage=@"magnet-large.png";
    }
    
    [imgView removeFromSuperview];
    imgView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:attractorImage]];
    
    imgView.alpha= 0.75f;
    imgView.opaque= NO;
    
    [self.rotatingView insertSubview:imgView atIndex:0];
    
}

-(void)setSizeWithWidth:(int)width andHeight:(int)height{
    self.frame=CGRectMake(0., 0., width, height);
    self.rotatingView.frame= self.frame;
    
    self.label.frame= self.frame;
    self.label.center= CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0);
    
    self.defLabel.frame= self.frame;
    self.defLabel.center= CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0+15);
}

-(void)makeMorpheme{
    self.isMorpheme=YES;
    [self setImage];
    [self setSizeWithWidth:80 andHeight:80];
    
}

- (void)layoutSubviews {
    
    CGPoint c = CGPointMake(self.bounds.size.width/2.,
                            self.bounds.size.height/2.);
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    self.shapeLayer.bounds = self.bounds;
    self.shapeLayer.position = c;
    [CATransaction commit];
    
    self.rotatingView.bounds = self.bounds;
    self.rotatingView.center = c;
    
    self.radius = self.bounds.size.width/2.;
    
    self.radius = self.bounds.size.width/2.;
    
    CGRect labelFrame = self.label.frame;
    labelFrame.size.width = self.bounds.size.width;
    self.label.frame = labelFrame;
    self.label.center = c;
    
}

static inline CGPoint cartesianPointFromPolar(CGFloat radius, CGFloat angle) {
    return CGPointMake(cosf(angle)*radius, sinf(angle)*radius);
}

- (void)showLayer:(CALayer*)layer {
    
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:.3];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    [CATransaction setCompletionBlock:^{
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.+RANDOM_FLOAT()];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [CATransaction setCompletionBlock:^{
            [layer removeFromSuperlayer];
        }];
        layer.position = CGPointMake(self.bounds.size.width/2.,
                                     self.bounds.size.height/2.);
        [CATransaction commit];
    }];
    layer.opacity = 0;
    [CATransaction commit];
    
}

- (void)startFluffies {
    
    if (self.fluffiesRunning) {
        //OPLog(@"new fluff");
        for (NSInteger j = 0; j < 6; j++) {
            CALayer *fluff = [CALayer layer];
            [self.layer insertSublayer:fluff
                                 below:self.rotatingView.layer];
            
            UIImage *img = [UIImage imageNamed:@"filing"];
            
            [CATransaction begin];
            [CATransaction disableActions];
            fluff.contents = (__bridge id)img.CGImage;
            fluff.opacity = 0.;
            fluff.frame = CGRectMake(0., 0., img.size.width, img.size.height);
            CGPoint p = cartesianPointFromPolar(300, RANDOM_FLOAT()*M_PI*2);
            p.x += self.bounds.size.width/2.;
            p.y += self.bounds.size.height/2.;
            fluff.position = p;
            
            //fluff.borderWidth = 1.;
            //fluff.borderColor = [UIColor redColor].CGColor;
            
            [CATransaction commit];
            
            // seems CoreAnimation needs some time to get the layer into the frame buffer
            [self performSelector:@selector(showLayer:) withObject:fluff afterDelay:0.0];
        }
        CGFloat nextRoundDelay = RANDOM_FLOAT();
        if (nextRoundDelay < 0.1)
            nextRoundDelay += 0.1;
        
        [self performSelector:@selector(startFluffies)
                   withObject:nil
                   afterDelay:nextRoundDelay];
        
    } else {
        //OPLog(@"starting fluffies");
        self.fluffiesRunning = YES;
        [self performSelector:@selector(startFluffies)
                   withObject:nil
                   afterDelay:RANDOM_FLOAT()];
    }
}

- (void)stopFluffies {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startFluffies) object:nil];
    //OPLog(@"stopping fluffies");
    self.fluffiesRunning = NO;    
}

- (void)setRadius:(CGFloat)aradius {
    if (radius != aradius) {
        radius = aradius;
        
        // resize the Box2D body
        BoxBody *fbody = self.body;
        BoxFixture *fix = [fbody.fixtures lastObject];
        [fix setShapeRadius:radius];
        
        // resize the CAShapeLayer
        
        self.shapeLayer.path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(self.bounds.size.width/2.-radius,
                                                                                 self.bounds.size.height/2.-radius, 
                                                                                 2.*radius,
                                                                                 2.*radius)].CGPath;
        /*
         self.shapeLayer.path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0,
         0.0, 
         2.*radius,
         2.*radius)].CGPath;//*/
    }
}

// Ignore touch events on UIControl classes. Thanks Apple. :/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIControl class]]) {
        // we touched a button, slider, or other UIControl
        return NO; // ignore the touch
    }
    return YES; // handle the touch
}

- (void) fadeIn{
    //self.rotatingView.alpha=0.0;
    CGRect oldFrame=self.frame;
    CGRect newFrame=CGRectMake(-200, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    self.frame=newFrame;
    
    [UIView animateWithDuration:4.0 
                     animations:^{
                         self.frame=oldFrame;
                         //self.rotatingView.alpha=1.0;
                     }];
}

@end
