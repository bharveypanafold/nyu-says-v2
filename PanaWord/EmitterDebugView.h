//
//  EmitterDebugView.h
//  PanaWord
//
//  Created by Panafold on 9/27/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface EmitterDebugView : UIView {
    CFMutableDictionaryRef _blockDict;
}
@property (weak,nonatomic) CAEmitterLayer *emitter;
@end
