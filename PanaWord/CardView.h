//
//  CardView.h
//  PanaWord
//
//  Created by Panafold on 10/11/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageWord.h"

@interface CardView : UIView

@property (strong,nonatomic) LanguageWord *word;
@property (weak,nonatomic) UIViewController *viewController;
@property (strong,nonatomic) NSMutableArray *filingViews;

- (void)layoutData;

@end
