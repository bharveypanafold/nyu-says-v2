//
//  WordPackManager.m
//  CaliforniaEnglish
//
//  Created by Guillaume on 3/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WordPackManager.h"
#import "PFViewController.h"

@implementation WordPackManager

@synthesize parent;
@synthesize parser;
@synthesize wordpacks;
@synthesize locklist;
@synthesize sortedPackArray;

@synthesize currentwordpack;

//switches
@synthesize addword;
@synthesize setpurchase;
@synthesize setfile;

- (id)initWithParent:(PFViewController*)p {
    self = [super init];
    
    if (self){
        self.parent=p;
        [self initPacks];
    }
    
    return self;
}

- (void) initPacks{
    self.wordpacks= [[NSMutableDictionary alloc] init];
    self.locklist= [[NSMutableDictionary alloc] init];
    
    NSURL* url= [[NSURL alloc] initWithString:@"http://107.22.253.19/stanford-xml/index.xml"]; //set index
    self.parser= [[NSXMLParser alloc] initWithContentsOfURL:url];
    [self.parser setDelegate:self];
    [self.parser parse];
    
    NSLog(@"%i total words loaded from remote xml file.", [self.wordpacks count]);
}

- (NSMutableDictionary*) getWordPackForWord:(NSString *)word{
    return [self.wordpacks objectForKey:word];
}

NSInteger strSort(id str1, id str2, void* context){
    NSComparisonResult res=[((NSString*)str1) compare:((NSString*)str2)];
    return res;
}

- (NSArray*) getWordpacks{
    if (self.sortedPackArray!=nil){ //we cache the sorted array; if it's in the cache, return it
        return self.sortedPackArray;
    }
    
    if ([self.wordpacks count]==0){//if no internetz last time, attempt to reload
        [self initPacks];
    }
    
    if ([self.wordpacks count]==0){ //if still no wordpacks (ie. internet down), then show error message
        NSMutableArray* tmpArray = [[NSMutableArray alloc] init];
        [tmpArray addObject:@"No internet connection!"];
        return tmpArray;
    }

    
    NSMutableArray* allpacks= [[NSMutableArray alloc] init];
    
    for (NSString* key in self.wordpacks){
        [allpacks addObject:key];
    }
    
    NSArray* packArray=[[NSArray alloc] initWithArray:allpacks];
    packArray=[packArray sortedArrayUsingFunction:strSort context:NULL];
    NSLog(@"Array sorted.");
    
    self.sortedPackArray=packArray;
    return packArray;
}


- (BOOL) getLocked:(NSString*) wordpack{
    if ([[self.locklist valueForKey:wordpack] isEqualToString:@"1"]){
        return YES;
    }
    return NO;
}

- (void) unlockWordpack:(NSString*) wordpack{
    [self.locklist setValue:@"0" forKey:wordpack];
}



#pragma mark — NSXMLParser delegate

- (void)parserDidStartDocument:(NSXMLParser *)parser{
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
}

- (void)parser:(NSXMLParser *)parser 
didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName 
    attributes:(NSDictionary *)attributeDict {
    
    //NSLog(@"began element %@", elementName);
    
    if ([elementName isEqualToString:@"wordpack"]){
        self.currentwordpack= [[LanguageWordPack alloc] initWithDictManager:self.parent.dictionarymanager];
    } else if ([elementName isEqualToString:@"name"]){
        self.addword=YES;
    } else if ([elementName isEqualToString:@"purchase"]){
        self.setpurchase=YES;
    } else if ([elementName isEqualToString:@"file"]){
        self.setfile=YES;
    }
}


- (void)parser:(NSXMLParser *)parser 
 didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName{
    
    //NSLog(@"ended element");
    
    if ([elementName isEqualToString:@"wordpack"]){

        [self.wordpacks setObject:currentwordpack forKey:self.currentwordpack.title];
        [self.currentwordpack loadWords];
        
    } else if ([elementName isEqualToString:@"name"]){
        self.addword=NO;
    } else if ([elementName isEqualToString:@"purchase"]){
        self.setpurchase=NO;
    } else if ([elementName isEqualToString:@"file"]){
        self.setfile=NO;
    }
}

- (void)parser:(NSXMLParser *)parser 
foundCharacters:(NSString *)string{
        
    //NSLog(@"string: %@", string);
    //NSLog(@"Current wordpack: %@", self.currentwordpack.title);
    
    if (self.addword){
        self.currentwordpack.title=string;
    } else if (self.setpurchase){
        [self.locklist setValue:string forKey:self.currentwordpack.title];
        [self.currentwordpack setUnlocked:YES]; //TODO
    } else if (self.setfile){
        [self.currentwordpack setXmlSourceFile:string];
    }
}
@end
