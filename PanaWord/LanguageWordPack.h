//
//  LanguageWordPack.h
//  PanaWord
//
//  Created by Panafold on 10/10/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DictionaryManager.h"

@interface LanguageWordPack : NSObject <NSXMLParserDelegate>


@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSMutableArray *words;
@property (nonatomic) BOOL unlocked;

@property (nonatomic, retain) NSXMLParser* parser;


@property (strong, nonatomic) NSString* xmlSourceFile;

@property (nonatomic) BOOL setword;
@property (nonatomic) BOOL setdefinition;
@property (strong, nonatomic) DictionaryManager* dictionaryManager;
- (id)initWithDictManager:(DictionaryManager*)dict;
- (void) loadWords;

@end
