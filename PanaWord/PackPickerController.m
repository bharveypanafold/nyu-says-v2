//
//  PackPickerController.m
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "PackPickerController.h"
#import <QuartzCore/QuartzCore.h>

#import "BoxWorld.h"
#import "BoxBody.h"
#import "UIColor-Extras.h"

#import "LanguageSource.h"
#import "LanguageWordPack.h"

#import "NSData+HMAC.h"

@interface PackPickerController()

@property (strong,nonatomic) BoxWorld *world;
@property (strong,nonatomic) NSMutableArray *wallBodies;
@property (strong,nonatomic) CADisplayLink *displayLink;

@property (strong,nonatomic) BoxBody *mouseAnchorBody;
@property (strong,nonatomic) NSMutableSet *packButtonBodies;

- (void)startBoxWorld;
- (void)stopBoxWorld;
- (void)buildBoxWalls;
- (void)positionViews;

@end

@implementation PackPickerController

@synthesize chosenPack;
@synthesize displayLink;
@synthesize wallBodies,world;
@synthesize mouseAnchorBody;
@synthesize packButtonBodies;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.modalPresentationStyle = UIModalPresentationFormSheet;
        
        self.title = @"Word Pack Picker";
        
        self.world = [BoxWorld worldWithGravity:CGPointMake(0.0, -5.0)];
        
        self.packButtonBodies = [NSMutableSet setWithCapacity:1];
        
    }
    return self;
}

- (void)doneAction:(id)sender {
    UINavigationController *vc = (UINavigationController*)self.presentingViewController;
    [vc dismissViewControllerAnimated:YES
                           completion:^{
                               OPLog(@"dismissed");
                               [vc performSelector:@selector(wordPickerDone:) withObject:self.chosenPack];
                           }];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)pickPack:(UIButton*)button {
    
    //BoxBody *body = [button.layer valueForKey:@"body"];
    
    LanguageWordPack *pack = [button.layer valueForKey:@"pack"];
    
    self.chosenPack = pack;
    
    //[button.layer setValue:nil forKey:@"body"];
    
    [TestFlight passCheckpoint:@"WordPackChosen"];
    
    [self doneAction:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}

- (void)handleAttractorPanFrom:(UIPanGestureRecognizer*)recognizer {
    UIView *view = recognizer.view;
    BoxBody *theBody = nil;
    for (BoxBody *b in self.packButtonBodies) {
        if (view == b.weakContext) {
            theBody = b;
            break;
        }
    }
    
	if (recognizer.state == UIGestureRecognizerStateBegan) {
        // ...
        BoxMouseJoint *mJoint = [[BoxMouseJoint alloc] init];
        mJoint.bodyA = self.mouseAnchorBody;//[self.wallBodies lastObject];
        mJoint.bodyB = theBody;
        mJoint.targetPoint = [recognizer locationInView:self.view];
        mJoint.maxForce = 1000.0*theBody.mass;
        mJoint.dampingRatio = 0.9;
        
        [self.world addJoint:mJoint];
        
        [view.layer setValue:mJoint
                      forKey:@"mouseJoint"];
	}
	else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        mJoint.targetPoint = [recognizer locationInView:self.view];
	}
	else if (recognizer.state == UIGestureRecognizerStateEnded) {
        // ...
        BoxMouseJoint *mJoint = [view.layer valueForKey:@"mouseJoint"];
        
        [self.world removeJoint:mJoint];
        [view.layer setValue:nil forKey:@"mouseJoint"];
        
	}
}

- (CGFloat)randomFloatSeededWithString:(NSString*)string {
    NSData *wd = [[string dataUsingEncoding:NSUTF8StringEncoding] SHA1];
    
    long *d = (long*)[wd bytes]; // pointer to a long
    
    long seedling = *(d);
    
    d+= 1;
    seedling += *(d);
    
    srandom(seedling);
    
    return RANDOM_FLOAT();
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // add box bodies for views
    [self buildBoxWalls];
    
    CGFloat delay = 0.0;
    
    for (LanguageWordPack *pack in [LanguageSource sharedInstance].packs) {
        
        UIButton *packButton = [UIButton buttonWithType:UIButtonTypeCustom];
        packButton.alpha = 0.0;
        CGFloat diameter = 140.;
        packButton.frame = CGRectMake(0.0, 0.0, diameter, diameter);
        
        [packButton setTitle:pack.title
                    forState:UIControlStateNormal];
        
        [packButton addTarget:self
                       action:@selector(pickPack:)
             forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:packButton];
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(handleAttractorPanFrom:)];
        pan.minimumNumberOfTouches = 1;
        pan.maximumNumberOfTouches = 1;
        [packButton addGestureRecognizer:pan];
        
        CAShapeLayer *shape = [CAShapeLayer layer];
        shape.frame = CGRectMake(0., 0., packButton.bounds.size.width, packButton.bounds.size.height);
        shape.path = [UIBezierPath bezierPathWithOvalInRect:shape.bounds].CGPath;
        //shape.fillColor = [UIColor randomColor].CGColor;
        CGFloat hue = [self randomFloatSeededWithString:pack.title];
        shape.fillColor = [UIColor colorWithHue:hue
                                     saturation:0.8
                                     brightness:0.7
                                          alpha:1.].CGColor;
        
        [packButton.layer addSublayer:shape];
        
        
        BoxFixture *fixture = [BoxFixture fixtureWithShape:[CircleShape circleWithRadius:diameter/2.0 Position:CGPointZero]];
        fixture.friction = 0.5;
        fixture.density = 3.0;
        
        BoxBody *body = [BoxBody dynamicBody];
        body.linearDamping = 0.9;
        body.angularDamping = 2.0;
        [body addFixture:fixture];
        [self.world addBody:body];
        [self.packButtonBodies addObject:body];
        
        body.weakContext = packButton;
        
        [packButton.layer setValue:body
                            forKey:@"body"];
        [packButton.layer setValue:pack
                            forKey:@"pack"];
        
        RANDOM_SEED();
        CGFloat rx = 0.2 + RANDOM_FLOAT()*(0.8*self.view.bounds.size.width);
        CGFloat ry = 0.2 + RANDOM_FLOAT()*(0.8*self.view.bounds.size.height);
        
        body.position = CGPointMake(rx,ry);
        
        
        
        [UIView animateWithDuration:0.5
                              delay:delay
                            options:0
                         animations:^{
                             packButton.alpha = 1.;
                         } completion:^(BOOL finished) {
                             //
                         }];
        
        delay += 0.3;
    }
    
    [self positionViews];
    
    // start box world
    [self startBoxWorld];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop box world
    [self stopBoxWorld];
}

#pragma mark - Box

- (void)buildBoxWalls {
    self.wallBodies = [NSMutableArray arrayWithCapacity:4];
    
    CGFloat thickness = 200.;
    
    BoxBody *bb = [BoxBody staticBody];
    [bb addFixture:[BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:CGSizeMake(10., 10.)]]];
    bb.position = CGPointMake(-200., -200.); // offscreen
    [self.world addBody:bb];
    self.mouseAnchorBody = bb;
    
    // top
    BoxBody *wall = [BoxBody staticBody];
    BoxFixture *fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                                        CGSizeMake(1.1*self.view.bounds.size.width, thickness)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(self.view.bounds.size.width/2.0,
                                -thickness/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    // bottom
    wall = [BoxBody staticBody];
    fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                            CGSizeMake(1.1*self.view.bounds.size.width, thickness)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(self.view.bounds.size.width/2.0,
                                self.view.bounds.size.height+thickness/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    // left
    wall = [BoxBody staticBody];
    fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                            CGSizeMake(thickness,1.1*self.view.bounds.size.height)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(-thickness/2.0,
                                self.view.bounds.size.height/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    // right
    wall = [BoxBody staticBody];
    fixture = [BoxFixture fixtureWithShape:[PolygonShape rectangleWithSize:
                                            CGSizeMake(thickness,1.1*self.view.bounds.size.height)]];
    fixture.restitution = 0.3;
    fixture.friction = 0.5;
    [wall addFixture:fixture];
    wall.position = CGPointMake(self.view.bounds.size.width+thickness/2.0,
                                self.view.bounds.size.height/2.0);
    [self.world addBody:wall];
    [self.wallBodies addObject:wall];
    
    
}

- (void)positionViews {
    [self.world enumerateBodies:^(BoxBody *body) {
        // ...
        UIView *view = body.weakContext;
        
        //view.center = body.position;
        if (view) {
            CGPoint p = body.position;
            CGFloat a = body.angle;
            CGFloat xx = p.x - view.bounds.size.width/2.0;
            CGFloat yy = p.y - view.bounds.size.height/2.0;
            CGAffineTransform t = CGAffineTransformMakeTranslation(xx,
                                                                   yy);
            CGAffineTransform r = CGAffineTransformMakeRotation(a);
            
            view.transform = CGAffineTransformConcat(r, t);
            
            //OPLog(@"%f %f",xx,yy);
        }
    }];
}

- (void)boxTick {
    [self.world step];
    [self positionViews];
}

- (void)startBoxWorld {
    OPLog(@"start box world");
    //[self.world setAccelerometerEnabled:YES];
    [self.displayLink invalidate];
    self.displayLink = [CADisplayLink displayLinkWithTarget:self
                                                   selector:@selector(boxTick)];
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
}

- (void)stopBoxWorld {
    OPLog(@"stop box world");
    [self.displayLink invalidate];
    self.displayLink = nil;
}

@end
