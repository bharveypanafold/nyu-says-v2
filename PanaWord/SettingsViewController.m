//
//  SettingsViewController.m
//  CaliforniaEnglish
//
//  Created by Guillaume on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "PFViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Settings";
        self.contentSizeForViewInPopover = CGSizeMake(350., 400.);
    }
    return self;
}


- (void)dismiss {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
}

-(IBAction)toggleBG1:(id)sender{
    [self.parent setBackgroundToImage:@"1.png"];  
}

-(IBAction)toggleBG2:(id)sender{
    [self.parent setBackgroundToImage:@"2.png"];  
}

-(IBAction)toggleBG3:(id)sender{
    [self.parent setBackgroundToImage:@"3.png"];  
}

-(IBAction)toggleBG4:(id)sender{
    [self.parent setBackgroundToImage:@"4.png"];  
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
