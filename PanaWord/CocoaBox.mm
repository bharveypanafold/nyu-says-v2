//
//  CocoaBox.m
//  PanaFlow
//
//  Created by Panafold on 9/1/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "CocoaBox.h"
#import "Box2D.h"

// NSString *const PFTextViewWrapStyleKey = @"kTextViewWrapStyleKey";
NSString *const CocoaBoxBodyTypeKey = @"CocoaBoxBodyTypeKey";
NSString *const CocoaBoxBodyTypeStatic = @"CocoaBoxBodyTypeStatic";
NSString *const CocoaBoxBodyTypeDynamic = @"CocoaBoxBodyTypeDynamic";

NSString *const CocoaBoxBodyPositionKey = @"CocoaBoxBodyPositionKey";
NSString *const CocoaBoxBodyAngleKey = @"CocoaBoxBodyAngleKey";
NSString *const CocoaBoxBodyVelocityKey = @"CocoaBoxBodyVelocityKey";

NSString *const CocoaBoxBodySubShapesKey = @"CocoaBoxBodySubShapesKey";

NSString *const CocoaBoxShapePositionKey = @"CocoaBoxBodySubShapePositionKey";
NSString *const CocoaBoxShapeTypeKey = @"CocoaBoxBodySubShapeTypeKey";
NSString *const CocoaBoxShapeDensityKey = @"CocoaBoxBodySubShapeDensityKey";
NSString *const CocoaBoxShapeFrictionKey = @"CocoaBoxBodySubShapeFrictionKey";
NSString *const CocoaBoxShapeRestitutionKey = @"CocoaBoxShapeRestitutionKey";


NSString *const CocoaBoxShapeTypeCircle = @"CocoaBoxBodyShapeTypeCircle";
NSString *const CocoaBoxShapeCircleRadius = @"CocoaBoxBodyShapeCircleRadius";

NSString *const CocoaBoxShapeTypeBox = @"CocoaBoxBodyShapeTypeBox";
NSString *const CocoaBoxShapeBoxSize = @"CocoaBoxBodyShapeBoxSize";

@interface CocoaBox() <UIAccelerometerDelegate>

@end

@implementation CocoaBox

@synthesize pixelScale;

- (id)init {
    self = [super init];
    
    if (self) {
        
        b2Vec2 gravity = b2Vec2(0.0, .10);
        g[0] = 0.0;
        g[1] = 0.1;
        
        bool doSleep = true;
        world = new b2World(gravity, doSleep);
        
        self.pixelScale = 400.0;
        
        mouseJointMap = CFDictionaryCreateMutable(NULL, 0, NULL, NULL);
    }
    return self;
}

- (void)dealloc {
    [[UIAccelerometer sharedAccelerometer] setDelegate:nil];
    CFRelease(mouseJointMap);
    
    // ARC will not manage our C++ objects
    delete world;
    world = NULL;
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	CGFloat x = acceleration.x;
	CGFloat y = acceleration.y;
	CGFloat z = acceleration.z;
	
	g[0] = x;
	g[1] = y;
	g[2] = z;
}

- (void)_printVertexList:(b2Body*)body {
    b2Fixture *fixture = body->GetFixtureList();
    while (fixture != NULL) {
        b2PolygonShape *shape = (b2PolygonShape*)fixture->GetShape();
        for (int k = 0; k < shape->GetVertexCount(); k++) {
            b2Vec2 vv = body->GetWorldVector(shape->GetVertex(k));
            OPLog(@"     %i {%f,%f}",k,vv.x,vv.y);
        }
        fixture = fixture->GetNext();
    }
}

- (void)buildWallsForScreenSize:(CGSize)size {
    
    CGRect rect = CGRectMake(0.0, 0.0, size.width, size.height);
    rect.size.width /= self.pixelScale;
    rect.size.height /= self.pixelScale;
    // rect is now scaled to the b2 world.
    // !!!!!!!!!!!
    
    OPLog(@"Box world: %@ (screen: %@)",NSStringFromCGRect(rect),NSStringFromCGSize(size));
    
    b2BodyDef wallDef;
    b2Body *wall = NULL;
    
    b2PolygonShape wallBox;
    
    CGFloat W = rect.size.width, H = rect.size.height;
    CGFloat halfHeight = 0.2;
    
    OPLog(@"wall size in pixels: %f,%f",W*self.pixelScale,halfHeight*self.pixelScale);
    
    // top
    wallDef.position.Set(W/2.0,
                         H+halfHeight);
    wall = world->CreateBody(&wallDef);
    wallBox.SetAsBox(W/2.0, halfHeight);
    wall->CreateFixture(&wallBox,0.0);
    walls[0] = wall;
    
    OPLog(@" top wall origin: %f,%f",
          (wall->GetPosition()).x * self.pixelScale,
          (wall->GetPosition()).y * self.pixelScale  );
    
    // bottom
    wallDef.position.Set(W/2.0, 
                         -halfHeight);
    wall = world->CreateBody(&wallDef);
    wallBox.SetAsBox(W/2.0, halfHeight);
    wall->CreateFixture(&wallBox,0.0);
    walls[1] = wall;
    
    OPLog(@" bottom wall origin: %f,%f",
          (wall->GetPosition()).x * self.pixelScale,
          (wall->GetPosition()).y * self.pixelScale  );
    
    // left
    wallDef.position.Set(-halfHeight,
                         H/2.0);
    wall = world->CreateBody(&wallDef);
    wallBox.SetAsBox(halfHeight, H/2.0); // creates a box centered on the local origin
    wall->CreateFixture(&wallBox,0.0);
    walls[2] = wall;
    
    OPLog(@" left wall origin: %f,%f",
          (wall->GetPosition()).x * self.pixelScale,
          (wall->GetPosition()).y * self.pixelScale  );
    
    // right
    wallDef.position.Set(W+halfHeight,
                         H/2.0);
    wall = world->CreateBody(&wallDef);
    wallBox.SetAsBox(halfHeight, H/2.0);
    wall->CreateFixture(&wallBox,0.0);
    walls[3] = wall;
    
    OPLog(@" right wall origin: %f,%f",
          (wall->GetPosition()).x * self.pixelScale,
          (wall->GetPosition()).y * self.pixelScale  );
    
    
    [[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 60.)];
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];

}

- (void)step {

    world->SetGravity(b2Vec2(g[0], -g[1]));
    // fixed stepsize of 1/60 s
    // call this at 60hz for 'real time', or faster/slower to speed up or slow down 'time'
    world->Step(1.0/60.0, 8, 3);
}

- (void)addBodyWithDefinition:(NSDictionary*)def context:(id)context {
    b2BodyDef bodyDef;
    NSValue *position = [def valueForKey:CocoaBoxBodyPositionKey];
    NSValue *velocity = [def valueForKey:CocoaBoxBodyVelocityKey];
    NSNumber *angle = [def valueForKey:CocoaBoxBodyAngleKey];
    NSString *bodyType = [def valueForKey:CocoaBoxBodyTypeKey];
    NSArray *subShapes = [def valueForKey:CocoaBoxBodySubShapesKey];
    
    if ([bodyType isEqualToString:CocoaBoxBodyTypeDynamic]) {
        bodyDef.type = b2_dynamicBody;
    }
    if (position) {
        CGPoint p = [position CGPointValue];
        OPLog(@"p: %@ %f,%f",NSStringFromCGPoint(p),p.x/self.pixelScale,p.y/self.pixelScale);
        bodyDef.position.Set(p.x/self.pixelScale, p.y/self.pixelScale);
    }
    if (angle) {
        bodyDef.angle = [angle floatValue];
    }
    if (velocity) {
        CGPoint v = [velocity CGPointValue];
        bodyDef.linearVelocity.Set(v.x/self.pixelScale, v.y/self.pixelScale);
    }
    
    b2Body *body = world->CreateBody(&bodyDef);
    
    body->SetSleepingAllowed(false); // for some reason changes in gravity don't waken bodies. :(
    
    for (NSDictionary *subShape in subShapes) {
        
        NSString *shapeType = [subShape valueForKey:CocoaBoxShapeTypeKey];
        NSNumber *shapeDensity = [subShape valueForKey:CocoaBoxShapeDensityKey];
        NSNumber *shapeFriction = [subShape valueForKey:CocoaBoxShapeFrictionKey];
        NSValue *shapePosition = [subShape valueForKey:CocoaBoxShapePositionKey];
        NSNumber *shapeRestitution = [subShape valueForKey:CocoaBoxShapeRestitutionKey];
        
        CGFloat density = 1.0;
        if (shapeDensity) density = [shapeDensity floatValue];
        
        CGFloat friction = 0.3;
        if (shapeFriction) friction = [shapeFriction floatValue];
        
        CGPoint p = CGPointZero;
        if (shapePosition) p = [shapePosition CGPointValue];
        
        CGFloat rest = 0.0;
        if (shapeRestitution) rest = [shapeRestitution floatValue];
        
        b2FixtureDef fixDef;
        
        fixDef.density = density;
        fixDef.friction = friction;
        fixDef.restitution = rest;
        
        if ([shapeType isEqualToString:CocoaBoxShapeTypeBox]) {
            CGSize size = [[subShape valueForKey:CocoaBoxShapeBoxSize] CGSizeValue];
            b2PolygonShape shape;
            shape.SetAsBox((size.width/self.pixelScale)/2.0,
                           (size.height/self.pixelScale)/2.0);
            fixDef.shape = &shape;
            
        } else if ([shapeType isEqualToString:CocoaBoxShapeTypeCircle]) {
            CGFloat radius = [[subShape valueForKey:CocoaBoxShapeCircleRadius] floatValue];
            
            b2CircleShape shape;
            
            shape.m_radius = radius;
            fixDef.shape = &shape;
            
        } else {
            NSAssert(0, @"Unknown shape type. BAD ROBOT!");
        }
        
        body->CreateFixture(&fixDef);
    }
    
    body->SetUserData((__bridge void*)context);
}

- (void)enumerateBodiesWithBlock:(CocoaBoxEnumerateBodiesBlock)blk {
    
    b2Body *body = world->GetBodyList();
    
    while (NULL != body) {
        
        //if (b2_dynamicBody == body->GetType()) {
            id context = (__bridge id)body->GetUserData();
            
            b2Vec2 p = body->GetPosition();
            b2Vec2 v = body->GetLinearVelocity();
            CGFloat a = body->GetAngle();
            
            CGFloat s = self.pixelScale;
            blk(context,
                CGPointMake(p.x*s, p.y*s),
                CGPointMake(v.x*s, v.y*s),a);            
        //}
        // END
        body = body->GetNext();
    }
    
}

// Mouse Joints
class QueryCallback : public b2QueryCallback
{
public:
	QueryCallback(const b2Vec2& point)
	{
		m_point = point;
		m_fixture = NULL;
	}
    
	bool ReportFixture(b2Fixture* fixture)
	{
		b2Body* body = fixture->GetBody();
		if (body->GetType() == b2_dynamicBody)
		{
			bool inside = fixture->TestPoint(m_point);
			if (inside)
			{
				m_fixture = fixture;
                
				// We are done, terminate the query.
				return false;
			}
		}
        
		// Continue the query.
		return true;
	}
    
	b2Vec2 m_point;
	b2Fixture* m_fixture;
};

- (BOOL)addMouseJointAtPoint:(CGPoint)point withContext:(id)context {
    BOOL success = NO;
    // Make a small box.
	b2AABB aabb;
	b2Vec2 d;
    b2Vec2 p = b2Vec2(point.x/self.pixelScale, point.y/self.pixelScale);
	d.Set(0.001f, 0.001f);
	aabb.lowerBound = p - d;
	aabb.upperBound = p + d;
    
	// Query the world for overlapping shapes.
	QueryCallback callback(p);
	world->QueryAABB(&callback, aabb);
    
	if (callback.m_fixture)
	{
		b2Body* body = callback.m_fixture->GetBody();
		b2MouseJointDef md;
		md.bodyA = walls[0];
		md.bodyB = body;
		md.target = p;
		md.maxForce = 1000.0f * body->GetMass();
		b2MouseJoint *joint = (b2MouseJoint*)world->CreateJoint(&md);
		body->SetAwake(true);
        
        CFDictionarySetValue(mouseJointMap,
                             (__bridge void*)context,
                             joint);

        success = YES;
	}
    return success;
}

- (void)updateMouseJointToPoint:(CGPoint)point withContext:(id)context {
    
    b2Vec2 p = b2Vec2(point.x/self.pixelScale, point.y/self.pixelScale);
    
    b2MouseJoint *joint = (b2MouseJoint*)CFDictionaryGetValue(mouseJointMap,
                                                              (__bridge void*)context);
    
    if (NULL != joint) {
        joint->SetTarget(p);
    }
}

- (void)removeMouseJointWithContext:(id)context {
    
    b2MouseJoint *joint = (b2MouseJoint*)CFDictionaryGetValue(mouseJointMap,
                                                              (__bridge void*)context);
    if (NULL != joint) {
        CFDictionaryRemoveValue(mouseJointMap, (__bridge void*)context);        
        world->DestroyJoint(joint);
    }
}

@end
