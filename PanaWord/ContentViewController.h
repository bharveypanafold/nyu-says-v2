//
//  ContentViewController.h
//  PanaWord
//
//  Created by Panafold on 9/8/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageFiling.h"

@interface ContentViewController : UIViewController

@property (strong,nonatomic) LanguageFiling *filing;
@property (nonatomic, assign) int  indexSelected; 
-(void)setupWebView;
-(void)setupControlBar;

@end
