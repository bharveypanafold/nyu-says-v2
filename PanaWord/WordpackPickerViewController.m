//
//  WordpackPickerViewController.m
//  CaliforniaEnglish
//
//  Created by Guillaume on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//16102140000    110157
#import "WordpackPickerViewController.h"
#import "PFViewController.h"

@implementation WordpackPickerViewController

@synthesize listOfWordpacks;
@synthesize table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Wordpack Picker";
        // Change menu size
        self.contentSizeForViewInPopover = CGSizeMake(430., 430.);
    }
    return self;
}

- (void) buyPressed:(id)sender{
    NSInteger idx= ((UIButton*)sender).tag;
    NSString* wordpack= [listOfWordpacks objectAtIndex:idx];
    NSLog(@"Buy! %@", wordpack);
    //[self.parent.wordpackmanager unlockWordpack:wordpack];
    [sender removeFromSuperview];
}

- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section {
    return [listOfWordpacks count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //The code below checks whether to display the "purchase" button or not
    /*
     
     int cellHeight= cell.frame.size.height;
     int cellWidth= cell.frame.size.width;
     
     int buyButtonYOffset= (cellHeight-40)/2;
     int buyButtonXOffset=  (cellWidth-70*1.5);
     
     if ([self.parent.wordpackmanager getLocked:[listOfWordpacks objectAtIndex:indexPath.row]]){ // check whether wordpack is locked or not
     UIButton* purchaseButton= [UIButton buttonWithType:UIButtonTypeRoundedRect];
     [purchaseButton setTintColor:[UIColor greenColor]];
     [purchaseButton setTitle:@"Buy" forState:UIControlStateNormal];
     [purchaseButton addTarget:self action:@selector(buyPressed:) forControlEvents:UIControlEventTouchUpInside];
     [purchaseButton setFrame: CGRectMake(buyButtonXOffset, buyButtonYOffset, 70, 40)];
     [purchaseButton setTag:indexPath.row];
     [cell.contentView addSubview:purchaseButton];
     }
     */
    
    [[cell.contentView viewWithTag:CELL_NAME_TAG] removeFromSuperview];
    UILabel* wordPackName= [[UILabel alloc] initWithFrame:CGRectMake(10, 0, cell.frame.size.width-80, cell.frame.size.height)];
    wordPackName.font= [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
    [wordPackName setAdjustsFontSizeToFitWidth:YES];
    [wordPackName setBackgroundColor:[UIColor clearColor]];
    wordPackName.text= [NSString stringWithFormat:@"%@ %@", [listOfWordpacks objectAtIndex:indexPath.row],@""];
    wordPackName.tag= CELL_NAME_TAG;
    
    [cell.contentView addSubview:wordPackName];
    
    
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //    if ([self.parent.wordpackmanager getLocked:[listOfWordpacks objectAtIndex:indexPath.row]])
    //        return;
    
    NSString* wordpack= [self.listOfWordpacks objectAtIndex:indexPath.row];
    [self.parent loadWordPack:wordpack];
}

- (void) dismiss {
    [self dismissModalViewControllerAnimated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.listOfWordpacks= [self.parent getWordpackList];
    
    //[self.view setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    [self.table setBackgroundColor:[UIColor colorWithRed:208./255. green:211./255. blue:216./255. alpha:1.]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
