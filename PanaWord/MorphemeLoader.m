//
//  MorphemeLoader.m
//  EnglishDiscovery
//
//  Created by Guillaume on 5/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// No Morphemes in app
/*
#import "MorphemeLoader.h"

@implementation MorphemeLoader

- (id) init{
    self=[super init];
    
    if (self){
        db= [[MorphemeDatabase alloc] init];
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:FILENAME_AFFIXES ofType:@"xml"];
        NSData* data= [[NSData alloc] initWithContentsOfFile:filePath];
        parser= [[NSXMLParser alloc] initWithData:data];
        [parser setDelegate:self];
        [parser parse];
        
        filePath = [[NSBundle mainBundle] pathForResource:FILENAME_LATINGREEK_ROOTS ofType:@"xml"];
        data= [[NSData alloc] initWithContentsOfFile:filePath];
        parser= [[NSXMLParser alloc] initWithData:data];
        [parser setDelegate:self];
        [parser parse];
        
    }
    
    return self;
}

- (MorphemeDatabase*) getMorphemeDB{
    return db;
}

#pragma mark — NSXMLParser delegate

- (void)parserDidStartDocument:(NSXMLParser *)parser{
    NSLog(@"Parsing morpheme file");
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    NSLog(@"Loaded %d morphemes", [db.morphemes count]);
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSLog(@"%@", parseError);
}

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict {
    
    //NSLog(@"began element %@", elementName);
    
    if ([elementName isEqualToString:@"database"]){
    } else if ([elementName isEqualToString:@"prefix"]){
        addEntry=YES;
        currentEntry= [[LanguageWord alloc] init];
        currentEntry.type=@"prefix";
    } else if ([elementName isEqualToString:@"stem"]){
        addEntry=YES;
        currentEntry= [[LanguageWord alloc] init];
        currentEntry.type=@"stem";
    } else if ([elementName isEqualToString:@"suffix"]){
        addEntry=YES;
        currentEntry= [[LanguageWord alloc] init];
        currentEntry.type=@"suffix";
        
    } else if ([elementName isEqualToString:@"name"]){
        addName=YES;
    } else if ([elementName isEqualToString:@"definition"]){
        addDef=YES;
    }
}


- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"prefix"] || [elementName isEqualToString:@"stem"] || [elementName isEqualToString:@"suffix"]){
        addEntry=NO;
        [db addMorpheme:currentEntry];
        
    } else if ([elementName isEqualToString:@"name"]){
        addName=NO;
    } else if ([elementName isEqualToString:@"definition"]){
        addDef=NO;
    }
}

- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string{
    
    if (addDef){
        currentEntry.def=string;
    } else if (addName){
        currentEntry.name=string;
    }
}

@end
 
 */
