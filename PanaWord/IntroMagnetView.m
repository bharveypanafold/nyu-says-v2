//
//  IntroMagnetView.m
//  PowerOfWords
//
//  Created by Guillaume on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IntroMagnetView.h"

@implementation IntroMagnetView

- (id)initWithFrame:(CGRect)frame andSide:(int)side
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.type=@"welcome_magnet";
        
        NSString* magnetImage;
        
        if (side==LEFT_SIDE){
            //load with hand_LEFT.png
            magnetImage=@"hand_LEFT.png";
        } else {
            //load with hand_RIGHT.png
            magnetImage=@"hand_RIGHT.png";
        }
        
        [self.imgView removeFromSuperview];
        self.imgView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:magnetImage]];
        [self.rotatingView insertSubview:self.imgView atIndex:0];        

        //NSLog(@"%@", img);
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
