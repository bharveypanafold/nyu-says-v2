//
//  DictionaryManager.m
//  CaliforniaEnglish
//
//  Created by Guillaume on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DictionaryManager.h"
#import "WordpackLoader.h"
@implementation DictionaryManager

@synthesize dict;

- (id) init{
    
    self=[super init];
    
    if (self){
        self.dict=[[NSMutableDictionary alloc] initWithCapacity:100];
    }
    
    return self;
}

- (NSString*) getDefinitionForWord:(NSString*)word{
        NSLog(@"dictionary word is: %@", word);
    NSString* def= [self.dict objectForKey:word];

    NSLog(@"definition for word %@",[self.dict objectForKey:word]);
    if (def==NULL)
        def=@"No definition for this word.";
    return def;
}

- (void) setDefinition:(NSString*)def ForWord:(NSString*)word{

    if(def && word){
        NSLog(@"WORD: %@ definition set: %@", word, def);
        [self.dict setObject: def forKey:word];
          //  NSLog(@"WORD: %@ DEFINITIONNNN: %@", word, [dict objectForKey:word]) ;
    }
   
}


/*
 
 
 </wordpack>
 <wordpack name="Schools">
 
 <word>
 <fullword>CAS</fullword>
 
 
 <stem>CAS</stem>
 <definition>College of Arts and Sciences</definition>
 <rewrite>
 <elementA>CAS</elementA>
 <result>CAS</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Tisch</fullword>
 
 
 <stem>Tisch</stem>
 <definition>School of the Arts</definition>
 <rewrite>
 <elementA>Tisch</elementA>
 <result>Tisch</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Stern</fullword>
 
 
 <stem>Stern</stem>
 <definition>School of Business</definition>
 <rewrite>
 <elementA>Stern</elementA>
 <result>Stern</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Steinhardt</fullword>
 
 
 <stem>Steinhardt</stem>
 <definition>School of Culture, Education, and Human Develpment</definition>
 <rewrite>
 <elementA>Steinhardt</elementA>
 <result>Steinhardt</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Gallatin</fullword>
 
 
 <stem>Gallatin</stem>
 <definition>School of Individualized Study</definition>
 <rewrite>
 <elementA>Gallatin</elementA>
 <result>Gallatin</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>LSP</fullword>
 
 
 <stem>LSP</stem>
 <definition>Liberal Studies Program. A 2-year transitional program.</definition>
 <rewrite>
 <elementA>LSP</elementA>
 <result>LSP</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>SCPS</fullword>
 
 
 <stem>SCPS</stem>
 <definition>School of Continuing and Professional Studies</definition>
 <rewrite>
 <elementA>SCPS</elementA>
 <result>SCPS</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>SCPS</fullword>
 
 
 <stem>SCPS</stem>
 <definition>School of Continuing and Professional Studies</definition>
 <rewrite>
 <elementA>SCPS</elementA>
 <result>SCPS</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Silver</fullword>
 
 
 <stem>Silver</stem>
 <definition>School of Social Work (Not to be cofused with the Silver Center Building)</definition>
 <rewrite>
 <elementA>Silver</elementA>
 <result>Silver</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Poly</fullword>
 
 
 <stem>Poly</stem>
 <definition>NYU Poly School of Engineering (Located in Brooklyn)</definition>
 <rewrite>
 <elementA>Poly</elementA>
 <result>Poly</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>NYU Abu Dhabi</fullword>
 
 
 <stem>NYU Abu Dhabi</stem>
 <definition>NYU's Portal campus in Abu Dhabi. Students can attain their degree, and graduate, all while staying in Abu Dhabi.</definition>
 <rewrite>
 <elementA>NYU Abu Dhabi</elementA>
 <result>NYU Abu Dhabi</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>NYU Shanghai</fullword>
 
 
 <stem>NYU Shanghai</stem>
 <definition>NYU's Portal campus in Shanghai. Students can attain their degree, and graduate, all while staying in Shanghai.</definition>
 <rewrite>
 <elementA>NYU Shanghai</elementA>
 <result>NYU Shanghai</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>School of Nursing</fullword>
 
 
 <stem>School of Nursing</stem>
 <definition>Self Explanitory</definition>
 <rewrite>
 <elementA>School of Nursing</elementA>
 <result>School of Nursing</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>School of Dentistry</fullword>
 
 
 <stem>School of Dentistry</stem>
 <definition>Self Explanitory</definition>
 <rewrite>
 <elementA>School of Dentistry</elementA>
 <result>School of Dentistry</result>
 </rewrite>
 
 </word>
 
 
 </wordpack>
 
 <wordpack name="Dining Halls">
 
 <word>
 <fullword>Upstein</fullword>
 
 
 <stem>Upstein</stem>
 <definition>The food court on the first floor of Weinstein. Meal-based.</definition>
 <rewrite>
 <elementA>Upstein</elementA>
 <result>Upstein</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Downstein</fullword>
 
 
 <stem>Downstein</stem>
 <definition>The downstairs cafeteria in Weinstein. All you can eat.</definition>
 <rewrite>
 <elementA>Downstein</elementA>
 <result>Downstein</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Kosher Eatery</fullword>
 
 
 <stem>Kosher Eatery</stem>
 <definition>Kosher cafeteria located behind Upstein. Meat and Dairy days are alternated. Meal-based.</definition>
 <rewrite>
 <elementA>Kosher Eatery</elementA>
 <result>Kosher Eatery</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Hayden</fullword>
 
 
 <stem>Hayden</stem>
 <definition>Local grown, organic, vegitarian, and vegan varieties of food available. All you can eat.</definition>
 <rewrite>
 <elementA>Hayden</elementA>
 <result>Hayden</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Kimmel</fullword>
 
 
 <stem>Kimmel</stem>
 <definition>Food court located on 3rd floor of Kimmel. Meal based.</definition>
 <rewrite>
 <elementA>Kimmel</elementA>
 <result>Kimmel</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>3rd North</fullword>
 
 
 <stem>3rd North</stem>
 <definition>Cafeteria located on the ground floor of 3rd North. All you can eat.</definition>
 <rewrite>
 <elementA>3rd North</elementA>
 <result>3rd North</result>
 </rewrite>
 
 </word>
 
 <word>
 <fullword>Palladium</fullword>
 
 
 <stem>Palladium</stem>
 <definition>Cafeteria located on the 3rd floor of Palladium. Meal-based.</definition>
 <rewrite>
 <elementA>Palladium</elementA>
 <result>Palladium</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Rubin</fullword>
 
 
 <stem>Rubin</stem>
 <definition>Cafeteria located on the ground floor of Rubin. All you can eat.</definition>
 <rewrite>
 <elementA>Rubin</elementA>
 <result>Rubin</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Burger Studio</fullword>
 
 
 <stem>Burger Studio</stem>
 <definition>Grill with customizable orders located on the basement floor of University Hall.</definition>
 <rewrite>
 <elementA>Burger Studio</elementA>
 <result>Burger Studio</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Dunkin Donuts</fullword>
 
 
 <stem>Dunkin Donuts</stem>
 <definition>Regular Dunkin Donuts located in front of University Hall. Takes dining dollars and meal swipes.</definition>
 <rewrite>
 <elementA>Dunkin Donuts</elementA>
 <result>Dunkin Donuts</result>
 </rewrite>
 
 </word>
 </wordpack>
 
 <wordpack name="Freshman Housing">
 
 <word>
 <fullword>Weinstein</fullword>
 
 
 <stem>Weinstein</stem>
 <definition>Located on University Pl between 8th st and Waverly. Very close to "campus". Smaller rooms. Famous for cinderblock walls. Has three dining halls.</definition>
 <rewrite>
 <elementA>Weinstein</elementA>
 <result>Weinstein</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Rubin</fullword>
 
 
 <stem>Rubin</stem>
 <definition>Located on 10th st and 5th ave. Close to "campus". Average size rooms. Dining hall on ground floor.</definition>
 <rewrite>
 <elementA>Rubin</elementA>
 <result>Rubin</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Goddard</fullword>
 
 
 <stem>Goddard</stem>
 <definition>Located on 4th st and Washington Square Park East, above the Starbucks. Residential College builds community among residents through activities.</definition>
 <rewrite>
 <elementA>Goddard</elementA>
 <result>Goddard</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Hayden</fullword>
 
 
 <stem>Hayden</stem>
 <definition>Located on W 4th st and Washington Square Park West. Suite-style housing. Dining hall on ground level.</definition>
 <rewrite>
 <elementA>Hayden</elementA>
 <result>Hayden</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Brittany</fullword>
 
 
 <stem>Brittany</stem>
 <definition>Located on 10th st between University Pl and Broadway. Traditional suite-style housing.</definition>
 <rewrite>
 <elementA>Brittany</elementA>
 <result>Brittany</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>3rd North</fullword>
 
 
 <stem>3rd North</stem>
 <definition>Located on 3rd Ave and 11th st. Apartment-style housing (kitchen and dining area in each unit). Dining hall on ground level.</definition>
 <rewrite>
 <elementA>3rd North</elementA>
 <result>3rd North</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Founders</fullword>
 
 
 <stem>Founders</stem>
 <definition>Located on 12th st between 3rd and 4th ave. Suite-style housing with large rooms.</definition>
 <rewrite>
 <elementA>Founders</elementA>
 <result>Founders</result>
 </rewrite>
 
 </word>
 </wordpack>
 
 <wordpack name="Upperclassman Housing">
 
 <word>
 <fullword>Carlyle Court</fullword>
 
 
 <stem>Carlyle Court</stem>
 <definition>Located on Union Square West between 15th and 16th st. 10 minute walk from campus, but prime location in nyc. Very convineint location for subway". Apartment-style housing. No Dining Hall.</definition>
 <rewrite>
 <elementA>Carlyle Court</elementA>
 <result>Carlyle Court</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Coral Towers</fullword>
 
 
 <stem>Coral Towers</stem>
 <definition>Located on 3rd ave and 14th st. Apartment-style housing. Low-cost housing options available. No dining hall, but 1 block from Palladium and UHall.</definition>
 <rewrite>
 <elementA>Coral Towers</elementA>
 <result>Coral Towers</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Gramercy Green</fullword>
 
 
 <stem>Gramercy Green</stem>
 <definition>Also known as "Gram". Located on 3rd Ave between 23rd and 24th st. Far from campus, but NYU shuttle buses are available. Probably the nicest rooms in all of NYU. Apartment-style housing.</definition>
 <rewrite>
 <elementA>Gramercy Green</elementA>
 <result>Gramercy Green</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Greenwich Hotel</fullword>
 
 
 <stem>Greenwich Hotel</stem>
 <definition>>Also known as "G-Ho". Located in deep in the West Village on Greenwich st and Morton. 10 minute walk from campus. Apartment-style housing. No dining hall. </definition>
 <rewrite>
 <elementA>Greenwich Hotel</elementA>
 <result>Greenwich Hotel</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Alumni Hall</fullword>
 
 
 <stem>Alumni Hall</stem>
 <definition>Located on 3rd Ave and 9th st. 10 minute walk to campus. Right around the corner from St. Mark's Pl. Apartment-style, but smaller rooms. No dining hall, but 2 blocks from 3rd North.</definition>
 <rewrite>
 <elementA>Alumni Hall</elementA>
 <result>Alumni Hall</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>University Hall</fullword>
 
 
 <stem>Univesity Hall</stem>
 <definition>>Also known as "U-hall". Located on 14th st between 3rd and 4th Ave. Near Union Square.10 minute walk from campus. Apartment-style housing. Dining hall on basement level.</definition>
 <rewrite>
 <elementA>University Hall</elementA>
 <result>University Hall</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Second Street</fullword>
 
 
 <stem>Second Street</stem>
 <definition>Located on 2nd St between Bowery and 2nd Ave. 5 minute walk to campus. In the East Village. Studio and Apartment-style housing options.</definition>
 <rewrite>
 <elementA>Second Street</elementA>
 <result>Second Street</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Palladium</fullword>
 
 
 <stem>Palladium</stem>
 <definition>Located on 14th st between 3rd and 4th Ave. Near Union Square. 10 minute walk from campus. Apartment-style housing. Large dining hall, gym, and pool located on lower levels.</definition>
 <rewrite>
 <elementA>Palladium</elementA>
 <result>Palladium</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Broome</fullword>
 
 
 <stem>Broome</stem>
 <definition>Located on Broome St and Cleveland Pl. In between SoHo and Little Italy. 15 minute walk from campus, NYU shuttle buses also available. Apartment-style and Studio-style housing options. No dining hall.</definition>
 <rewrite>
 <elementA>Broome</elementA>
 <result>Broome</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Senior House</fullword>
 
 
 <stem>Senior House</stem>
 <definition>Located on 13th St between 5th and 6th Ave. 10 minute walk from campus. Near Union Square. Apartment-style housing. No dining hall.</definition>
 <rewrite>
 <elementA>Senior House</elementA>
 <result>Senior House</result>
 </rewrite>
 
 </word>
 <word>
 <fullword>Lafayette</fullword>
 
 
 <stem>Lafayette</stem>
 <definition>Located in Tribeca on Lafayette st betwen White and Franklin. Far from campus, but NYU shuttle buses are available. Near Chinatown and City Hall. Apartment-style housing. No dining hall.</definition>
 <rewrite>
 <elementA>Lafayette</elementA>
 <result>Lafayette</result>
 </rewrite>
 
 </word>
 </wordpack>
 
*/
 
 
 @end

