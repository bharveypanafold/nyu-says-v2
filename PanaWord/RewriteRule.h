//
//  RewriteRule.h
//  EnglishDiscovery
//
//  Created by Guillaume on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RewriteRule : NSObject
@property (strong,nonatomic) NSString* affix1;
@property (strong,nonatomic) NSString* affix2;
@property (strong,nonatomic) NSString* result;

-(id) initWithAffix1:(NSString*)a1 andAffix2:(NSString*)a2 forResult:(NSString*)r;

@end
