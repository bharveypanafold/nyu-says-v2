//
//  LanguageTopic.m
//  PanaWord
//
//  Created by Panafold on 11/28/11.
//  Copyright (c) 2013 Panafold. All rights reserved.
//

#import "LanguageTopic.h"

@implementation LanguageTopic

@synthesize subTopics,title,englishTitle;

- (id)init {
    
    self = [super init];
    if (self) {
        self.subTopics = [NSMutableArray arrayWithCapacity:1];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    
    self = [super init];
    if (self) {
        self.subTopics = [coder decodeObjectForKey:@"subTopics"];
        self.title = [coder decodeObjectForKey:@"title"];
        self.englishTitle = [coder decodeObjectForKey:@"englishTitle"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    
    [coder encodeObject:self.subTopics
                 forKey:@"subTopics"];
    
    [coder encodeObject:self.title
                 forKey:@"title"];
    
    [coder encodeObject:self.englishTitle
                 forKey:@"englishTitle"];
    
}

@end
