//
//  WordpackLoader.h
//  EnglishDiscovery
//
//  Created by Guillaume on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WordpackDatabase.h"
#import "WordDescription.h"
#import "LanguageWordPack.h"
#import "DictionaryManager.h"

#define WORDPACK_FILE @"wordpacks"

@interface WordpackLoader : NSObject <NSXMLParserDelegate>{
    NSXMLParser* parser;
    BOOL addEntry;
    BOOL addFullword;
    BOOL addDef;
    BOOL addAffix;
    BOOL addRewriteRule;
    BOOL addElementA;
    BOOL addElementB;
    BOOL addResult;
    BOOL addWordpack;
    BOOL setdefinition;
    WordpackDatabase* db;
    WordDescription* currentEntry;
    RewriteRule* rule;
    LanguageWordPack* currentWordpack;
}

@property (nonatomic, retain) DictionaryManager* dictionaryManager;
//@property (nonatomic) BOOL setdefinition;

- (NSString *)defReturn: (NSString *)term;

- (id) init;
- (WordpackDatabase*) getWordpackDB;

- (id)initWithDictManager:(DictionaryManager*)dict;
//- (Wordpack*) getWordPackNamed:(NSString*) wordpack;

@end
