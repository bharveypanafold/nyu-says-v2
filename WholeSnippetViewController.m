//
//  WholeSnippetViewController.m
//  NYUSays
//
//  Created by Vivian Allum on 4/3/14.
//
//

#import "WholeSnippetViewController.h"
#import "PFAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "ContentViewController.h"
#import "LanguageFiling.h"


@interface WholeSnippetViewController ()

@end

@implementation WholeSnippetViewController
@synthesize snippet;
@synthesize filing;
@synthesize indexSelected;
PFAppDelegate *appDelegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blueColor];
	// Do any additional setup after loading the view.
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(aMethod:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Back" forState:UIControlStateNormal];
    button.frame = CGRectMake(10,30,70,30);
    [self.view addSubview:button];
    
    appDelegate=(PFAppDelegate *)[[UIApplication sharedApplication] delegate];//in didLoad method
  
    // Do any additional setup after loading the view, typically from a nib.
    
    //UIFont * customFont = [UIFont fontWithName:Helvetica size:12]; //custom font
    NSString * text = snippet;
    
      
   // CGSize labelSize = [text constrainedToSize:CGSizeMake(380, 20) lineBreakMode:NSLineBreakByTruncatingTail];
    
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 15, 380, 400)];
    fromLabel.text = text;
   // fromLabel.font = customFont;
   //
    //fromLabel.
    fromLabel.numberOfLines = 1;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
    fromLabel.adjustsFontSizeToFitWidth = YES;
    fromLabel.adjustsLetterSpacingToFitWidth = YES;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor blackColor];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:fromLabel];
    
	// Do any additional setup after loading the view.

    
    
    //open whole page button
    
    UIButton *wholeSnippet = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [wholeSnippet addTarget:self
               action:@selector(webAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [wholeSnippet setTitle:@"More" forState:UIControlStateNormal];
    wholeSnippet.frame = CGRectMake(80,30,70,30);
    [self.view addSubview:wholeSnippet];
    
  /*  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow20x20"]
                                                                              style:UIBarButtonItemStyleBordered
                                                                             target:self
                                                                             action:@selector(webAction:)];
   */
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)aMethod:(UIButton*)button
{
    NSLog(@"Button  clicked.");
    
     CATransition *transition = [CATransition animation];
     transition.duration = 0.3;
     transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
     transition.type = kCATransitionPush;
     transition.subtype = kCATransitionFromLeft;
     [self.view.window.layer addAnimation:transition forKey:nil];
     [self dismissModalViewControllerAnimated:NO];
}

//add in the open whole page button

- (void)webAction:(id)sender {
    ContentViewController *vc = [[ContentViewController alloc] initWithNibName:nil bundle:nil];
    NSNumber *num = [NSNumber numberWithInt:indexSelected];
    [vc performSelectorInBackground:@selector(passIndex:) withObject:num];

   // vc.filing = [appDelegate.arrayFilings objectAtIndex:indexSelected];
    LanguageFiling* filing1 = [appDelegate.arrayFilings[indexSelected] mutableCopy];
    vc.filing= filing1;
    vc.indexSelected=indexSelected;
   // NSLog(@"this is hte website %@", [filing.content valueForKey:@"page"]);
    NSLog(@"this is hte website %@", [appDelegate.arrayFilings[indexSelected] mutableCopy] );
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentModalViewController:nav
                            animated:YES];
    
}
- (void)passIndex:(NSNumber *)someNumber{
  
    indexSelected=[someNumber integerValue];
    NSLog(@" int int int %i", indexSelected);
}

//take out open whole page button

@end
